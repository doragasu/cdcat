<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name></name>
    <message>
        <location filename="../dbase.cpp" line="185"/>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddLnk</name>
    <message>
        <location filename="../adddialog.cpp" line="763"/>
        <source>Add a &quot;Catalog Link&quot; to the database</source>
        <translation type="unfinished">Pridať &quot;Katalógovú linku&quot; k databáze</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="764"/>
        <source>Location of the .hcf file:</source>
        <translation type="unfinished">Cesta k súboru .hcf:</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="766"/>
        <source>Cancel</source>
        <translation type="unfinished">Zrušiť</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="767"/>
        <source>Ok</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="784"/>
        <source>CdCat databases (*.hcf )</source>
        <translation type="unfinished">CdCat databázy (*.hcf)</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="784"/>
        <source>Open a file...</source>
        <translation type="unfinished">Otvoriť súbor...</translation>
    </message>
</context>
<context>
    <name>ArchiveFile</name>
    <message>
        <location filename="../dbase.h" line="283"/>
        <source>unknown</source>
        <translation type="unfinished">neznámy</translation>
    </message>
    <message>
        <location filename="../dbase.h" line="283"/>
        <source></source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CatalogTypeEditDialog</name>
    <message>
        <location filename="../guibase.cpp" line="3138"/>
        <source>Change media type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3139"/>
        <source>Change type of media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3141"/>
        <source>CD</source>
        <translation type="unfinished">CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3142"/>
        <source>DVD</source>
        <translation type="unfinished">DVD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3143"/>
        <source>HardDisc</source>
        <translation type="unfinished">HardDisk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3144"/>
        <source>Floppy</source>
        <translation type="unfinished">Disketa</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3145"/>
        <source>NetworkPlace</source>
        <translation type="unfinished">Sieťový disk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3146"/>
        <source>FlashDrive</source>
        <translation type="unfinished">FlashDisk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3147"/>
        <source>OtherDevice</source>
        <translation type="unfinished">Ostatné</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3150"/>
        <source>Cancel</source>
        <translation type="unfinished">Zrušiť</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3151"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
</context>
<context>
    <name>CdCatConfig</name>
    <message>
        <location filename="../config.cpp" line="213"/>
        <source>Error during autoload...</source>
        <translation>Chyba pri automatickom načítavaní...</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="215"/>
        <source>I can&apos;t open the autoload catalog.
Check the file, or change the autoload option in the config dialog!
</source>
        <translation>Nemožno otvoriť katalóg pre automatické načítanie.
Skontrolujte súbor alebo zmeňte nastavenie automatického načítania katalógu!
</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error during write config...</source>
        <translation type="obsolete">Chyba pri ukladaní konfigurácie...</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="215"/>
        <source>I can&apos;t open the autoload catalog according the first command line parameter.
Check the file!
</source>
        <translation>Nemožno otvoriť katalóg zadaný ako prvý parameter príkazového riadku.
Skontrolujte prosím súbor!
</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>I can&apos;t get the $HOME environment variable.
It is necessary couse I&apos;d like to save the config file into your home directory.</source>
        <translation type="obsolete">Z premennej prostredia $HOME nie je možné zistiť cestu k Vášmu domovskému adresáru.
Je potrebné ju poznať pre uloženie konfigurácie.</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1347"/>
        <source>I can&apos;t create or rewrite the ./cdcat.cfg file </source>
        <translation>Nemožno vytvoriť alebo prepísať ./cdcat.cfg</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1349"/>
        <source>I can&apos;t create or rewrite the $(HOME)/.cdcatconfig file </source>
        <translation>Nemožno vytvoriť alebo prepísať $(HOME)/.cdcatconfig</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1349"/>
        <source>Error while saving config file...</source>
        <translation>Chyba pri ukladaní konfigurácie...</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="972"/>
        <source>I can&apos;t get the $HOME environment variable.
It is necessary because I&apos;d like to save the config file into your home directory.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CdCatMainWidget</name>
    <message>
        <location filename="" line="0"/>
        <source>No selected item</source>
        <translation type="obsolete">Žiadna vybraná položka</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="646"/>
        <source>Directory Tree</source>
        <translation>Adresárový strom</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="649"/>
        <source>Name</source>
        <translation>Názov</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="650"/>
        <source>Size</source>
        <translation>Veľkosť</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="171"/>
        <source>New...</source>
        <translation type="obsolete">Nový...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="172"/>
        <source>Open...</source>
        <translation type="obsolete">Otvoriť...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="173"/>
        <source>Save</source>
        <translation type="obsolete">Uložiť</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="174"/>
        <source>Save As...</source>
        <translation type="obsolete">Uložiť ako...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="177"/>
        <source>Close</source>
        <translation type="obsolete">Zatvoriť</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="187"/>
        <source>Quit</source>
        <translation type="obsolete">Koniec</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="677"/>
        <source>Add media...</source>
        <translation>Pridať médium...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="681"/>
        <source>Rescan media...</source>
        <translation>Aktualizovať médium...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="683"/>
        <source>Insert Catalog...</source>
        <translation>Vložiť katalóg...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="687"/>
        <source>Rename node...</source>
        <translation>Premenovať položku...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="693"/>
        <source>Delete node</source>
        <translation>Zmazať položku</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="578"/>
        <source>Sort media by number</source>
        <translation type="obsolete">Zoradiť média podľa čísla</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="307"/>
        <source>Sort media by name</source>
        <translation>Zoradiť média podľa názvu</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="202"/>
        <source>Sort media by type</source>
        <translation type="obsolete">Zoradiť média podľa typu</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="582"/>
        <source>Sort media by time</source>
        <translation type="obsolete">Zoradiť média podľa času</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="718"/>
        <source>Seek in database...</source>
        <translation>Hľadať...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="724"/>
        <source>Node size</source>
        <translation>Veľkosť položky</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="726"/>
        <source>Configuration...</source>
        <translation>Konfigurácia...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="735"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="514"/>
        <source>About</source>
        <translation type="obsolete">O programe</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="350"/>
        <source>Catalog</source>
        <translation type="obsolete">Katalóg</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="638"/>
        <source>Edit</source>
        <translation>Editovať</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="642"/>
        <source>Others</source>
        <translation>Ostatné</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="747"/>
        <source>Hyper&apos;s CD Catalogizer</source>
        <translation>Hyperov CD katalogizér</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="493"/>
        <source>Open a catalog from a file.</source>
        <translation type="obsolete">Otvoriť katalóg zo súboru.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="499"/>
        <source>Close the actual catalog.</source>
        <translation type="obsolete">Zatvoriť aktuálny katalóg.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="501"/>
        <source>Add a new media to the catalog.</source>
        <translation type="obsolete">Pridať do katalógu nové médium.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Rescan the selected media from the disk.</source>
        <translation type="obsolete">Aktualizovať zvolené médium.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete the selected media from the catalog.</source>
        <translation type="obsolete">Vymazať vybrané médium z katalógu.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="507"/>
        <source>Configuration of the program.</source>
        <translation type="obsolete">Konfigurácia programu.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="511"/>
        <source>Create a new, empty catalog and close the previous.</source>
        <translation type="obsolete">Vytvoriť nový, prázdny katalóg a zatvoriť predchádzajúci.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="513"/>
        <source>Search an element in the database(catalog).You can search filenames, directory names, name parts or mp3 tags etc...</source>
        <translation type="obsolete">Vyhľadať položku v katalógu. Môžete hľadať podľa názvu/časti názvu adresára, súboru, ID3 Tagu atď...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="369"/>
        <source>Search an element.</source>
        <translation type="obsolete">Vyhľadať položku.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="672"/>
        <source>Recent files...</source>
        <translation>Nedávne súbory...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Export database</source>
        <translation type="obsolete">Exportovať databázu</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="641"/>
        <source>Import/Export</source>
        <translation>Import/Export</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="689"/>
        <source>Re-Number media node...</source>
        <translation>Zmeň číslo média...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Import database (csv/gtktalog)</source>
        <translation type="obsolete">Importovať databázu (csv/gtkatalog)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="723"/>
        <source>Borrowing info...</source>
        <translation>Informácie o zapožičaní...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="495"/>
        <source>Save all modifications to the disc.</source>
        <translation type="obsolete">Uložiť všetky zmeny na disk.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="497"/>
        <source>Save the catalog to a new file.</source>
        <translation type="obsolete">Uložiť katalóg do nového súboru.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add a link to a CdCAt Catalog...</source>
        <translation type="obsolete">Pridaj linku ku katalógu CdCat...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="728"/>
        <source>Set Colors...</source>
        <translation>Zvoliť farby...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="85"/>
        <source>No item selected</source>
        <translation type="obsolete">Žiadna vybraná položka</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="503"/>
        <source>Rescan the selected media.</source>
        <translation type="obsolete">Aktualizovať zvolené médium.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="721"/>
        <source>Seek in the panel</source>
        <translation>Hľadaj v paneli</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="738"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="679"/>
        <source>Add a link to a CdCat Catalog...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="799"/>
        <source>Scanning:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="730"/>
        <source>Import database (CSV/XML)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="733"/>
        <source>Export database (CSV/HTML/XML)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="640"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="655"/>
        <source>&amp;New...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="656"/>
        <source>Create a new catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="657"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="658"/>
        <source>Open a existing catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="659"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="660"/>
        <source>Save catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="661"/>
        <source>&amp;Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="662"/>
        <source>save catalog with new name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="674"/>
        <source>Close catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="821"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="676"/>
        <source>Close program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="678"/>
        <source>Add new media to catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="680"/>
        <source>Add a link to a existing cdcat catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="682"/>
        <source>Rescan existing media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="684"/>
        <source>Insert catalog into database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="688"/>
        <source>Rename node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="690"/>
        <source>Renumber node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="719"/>
        <source>Seek in database for files and folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="725"/>
        <source>Calculate node size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="727"/>
        <source>Edit cdcat configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="729"/>
        <source>Set the colors for display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="731"/>
        <source>Import database (CSV/XML) from various catalog programs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="737"/>
        <source>About Cdcat</source>
        <translation type="unfinished">O programe</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="739"/>
        <source>About the Qt toolkit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="812"/>
        <source>Mi&amp;nimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="815"/>
        <source>Ma&amp;ximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="818"/>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="920"/>
        <source>Cdcat - idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="81"/>
        <source>Main toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="109"/>
        <source>Comment dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="710"/>
        <source>view tool bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="711"/>
        <source>View tool bar in main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="712"/>
        <source>view status bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="713"/>
        <source>View status bar in main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="714"/>
        <source>view comment dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="715"/>
        <source>show comment dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="639"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="717"/>
        <source>show systray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="637"/>
        <source>File</source>
        <translation type="unfinished">Súbor</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="651"/>
        <source>Type</source>
        <translation type="unfinished">Typ</translation>
    </message>
    <message>
        <location filename="../mainwidget.h" line="94"/>
        <source>processing file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="695"/>
        <source>Sort media by number (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="697"/>
        <source>Sort media by number (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="703"/>
        <source>Sort media by time (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="705"/>
        <source>Sort media by time (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="699"/>
        <source>Sort media by name (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="701"/>
        <source>Sort media by name (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="707"/>
        <source>Sort media by type (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="709"/>
        <source>Sort media by type (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="685"/>
        <source>Insert Cdcat XML export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="686"/>
        <source>Insert Cdcat XML export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="251"/>
        <source>Insert Cdcat export XML...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="254"/>
        <source>Insert Cdcat exported XML into database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="664"/>
        <source>Change password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="665"/>
        <source>Changes password for catalog encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="666"/>
        <source>Disable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="667"/>
        <source>Disables catalog encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="668"/>
        <source>Enable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="669"/>
        <source>Enables catalog encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="395"/>
        <source>Borrowing info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="444"/>
        <source>Key bindings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSchemePreview</name>
    <message>
        <location filename="../colorsettings.cpp" line="71"/>
        <source>Title:</source>
        <translation>Názov:</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="73"/>
        <source>Road To Hell</source>
        <translation>Cesta do pekla</translation>
    </message>
</context>
<context>
    <name>ColorSettings</name>
    <message>
        <location filename="../colorsettings.cpp" line="221"/>
        <source>ColorSettings</source>
        <translation>Nastavenie farieb</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="223"/>
        <source>Comment area background and the file higlighting line</source>
        <translation>Pozadie komentára a zvýrazňovanie súborov</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="224"/>
        <source>Frame of comment area</source>
        <translation>Rám okolo komentára</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="225"/>
        <source>Comment window static text (Program text)</source>
        <translation>Text komentára (nadpisy)</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="226"/>
        <source>Comment window variable text (Data)</source>
        <translation>Text komentára (premenné)</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="227"/>
        <source>color</source>
        <translation>farba</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="228"/>
        <source>Red:</source>
        <translation>Červená:</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="229"/>
        <source>Green:</source>
        <translation>Zelená:</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="230"/>
        <source>Blue:</source>
        <translation>Modrá:</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="231"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="232"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
</context>
<context>
    <name>CommentWidget</name>
    <message>
        <location filename="../commwidget.cpp" line="88"/>
        <source>Edit and refresh the actual comment page.</source>
        <translation>Editovať a obnoviť aktuálny komentár.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="182"/>
        <source>Upper container! (..)</source>
        <translation>Vyšší kontajner (..)</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="239"/>
        <source> CdCat Catalog root</source>
        <translation>Koreňový adresár CdCat katalógu</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="245"/>
        <source> Directory</source>
        <translation>Adresár</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="248"/>
        <source> File</source>
        <translation>Súbor</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="251"/>
        <source>Media</source>
        <translation>Médium</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="265"/>
        <source>Size:</source>
        <translation>Veľkosť:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Last moification:</source>
        <translation type="obsolete">Posledná zmena:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="328"/>
        <source>Owner:</source>
        <translation>Vlastník:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="336"/>
        <source>Unknown</source>
        <translation>Neznámy</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Media (number/name):</source>
        <translation type="obsolete">Médium (číslo/názov):</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="386"/>
        <source>Mp3-Tag:</source>
        <translation>MP3-Tag:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="313"/>
        <source>(Art/Tit/Alb/Year/Comm)</source>
        <translation type="obsolete">(Autor/Názov/Album/Rok/Kom)</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="495"/>
        <source>Comment:</source>
        <translation>Komentár:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="83"/>
        <source>Shows the content of the file.</source>
        <translation>Zobraziť obsah súboru.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="191"/>
        <source>Name:</source>
        <translation>Názov:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="234"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="844"/>
        <source>There is no selected element.</source>
        <translation>Nie je vybraná žiadna položka.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="242"/>
        <source> Link to a CdCat catalog</source>
        <translation>Linka ku katalógu CdCat</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="283"/>
        <source>Location:</source>
        <translation>Umiestnenie:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="297"/>
        <source>Last modification:</source>
        <translation>Posledná zmena:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="204"/>
        <source>At media (number/name):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="98"/>
        <source>Edit and refresh the actual category page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="560"/>
        <source>Category:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="615"/>
        <source>Archive contents:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="393"/>
        <source>Artist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="399"/>
        <source>Title:</source>
        <translation type="unfinished">Názov:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="399"/>
        <source>track:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="405"/>
        <source>Album:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="411"/>
        <source>Year:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="730"/>
        <source>File info:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="636"/>
        <source>Rights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="641"/>
        <source>Owner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="646"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="651"/>
        <source>Size</source>
        <translation type="unfinished">Veľkosť</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="656"/>
        <source>Changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="666"/>
        <source>Comment</source>
        <translation type="unfinished">Komentár</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="661"/>
        <source>Path</source>
        <translation type="unfinished">Cesta</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="431"/>
        <source>Exif data:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="457"/>
        <source>Thumbnail:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="825"/>
        <source>Stored size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="845"/>
        <source>There is no selected element:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="353"/>
        <source>Number</source>
        <translation type="unfinished">Číslo</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="356"/>
        <source>Name</source>
        <translation type="unfinished">Názov</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="359"/>
        <source>Type</source>
        <translation type="unfinished">Typ</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="362"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="367"/>
        <source>sorted by:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../config.cpp" line="1609"/>
        <source>Configure  CdCat...</source>
        <translation>Konfigurovať CdCat...</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1613"/>
        <source>Autoload DataBase on startup</source>
        <translation>Automaticky načítať databázu pri štarte</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1615"/>
        <source>Save the XML-db.file nicer format(needs more space)</source>
        <translation>Uložiť vo formáte XML-db (krajší formát, potrebuje viac miesta)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1616"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1617"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1621"/>
        <source>Application font size.(must restart cdcat!) </source>
        <translation>Veľkosť fontu aplikácie. (Prejaví sa až po reštarte cdcat-u)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>CdCat Databases (*.hcf)</source>
        <translation type="obsolete">CdCat databázy (*.hcf)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1612"/>
        <source>Path to cdrom device</source>
        <translation type="obsolete">Cesta k CD-ROM</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1627"/>
        <source>Scanning: mount cdrom at start / eject when finish</source>
        <translation>Skenovanie: pripojiť CD-ROM pri zahájení / vysunúť CD po dokončení</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1619"/>
        <source>Use own font size</source>
        <translation>Použiť vlastnú veľkosť fontu</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1622"/>
        <source>Number of history entries</source>
        <translation>Počet položiek histórie</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1646"/>
        <source>Choose path to cdrom!</source>
        <translation>Vyberte cestu k CD-ROM!</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1631"/>
        <source>The language of CdCat interface</source>
        <translation>Jazyk programu CdCat</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select readable items</source>
        <translation type="obsolete">Výber ďalších položiek pre čítanie</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1640"/>
        <source>Choose a file to load automatically!</source>
        <translation>Zvoľte súbor pre automatické načítanie!</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Automatically save the database after every scan (for dafety sake)</source>
        <translation type="obsolete">Automaticky uložiť databázu po každom skenovaní</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1618"/>
        <source>Select additional items to read</source>
        <translation>Výber ďalších položiek pre čítanie</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1614"/>
        <source>Automatically save the database after every scan (for safety sake)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1634"/>
        <source>Display debug info on console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1640"/>
        <source>CdCat databases (*.hcf )</source>
        <translation type="unfinished">CdCat databázy (*.hcf)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1635"/>
        <source>Save catalogs always as UTF8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1636"/>
        <source>Show progressed file at scanning in status label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1620"/>
        <source>font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1494"/>
        <source>show systray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1610"/>
        <source>Show systray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1611"/>
        <source>display current scanned file in tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1612"/>
        <source>display current scanned file in tray (mediainfo / archive scan)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1623"/>
        <source>Path to cdrom device / mount dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1624"/>
        <source>This is the path to the cdrom/dvd device or the path where its mounted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataBase</name>
    <message>
        <location filename="../dbase.cpp" line="719"/>
        <source>I can&apos;t rewrite the file: %1</source>
        <translation>Nemožno prepísať súbor: %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="744"/>
        <source>I can&apos;t create the file: %1</source>
        <translation>Nemožno vytvoriť súbor: %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>There is not opened database!</source>
        <translation type="obsolete">Nie je otvorená žiadna databáza!</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2091"/>
        <source>I can&apos;t open the file: %1</source>
        <translation>Nemožno otvoriť súbor: %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="990"/>
        <source>Cannot read directory: %1</source>
        <translation>Nemožno prečítať adresár: %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2127"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ignore directory</source>
        <translation type="obsolete">Ignorovať adresár</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="994"/>
        <source>Cancel scanning</source>
        <translation>Zrušiť prehľadávanie</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Symbolic link#Points to:</source>
        <translation type="obsolete">Symbolická linka#Ukazuje na:</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2127"/>
        <source>Error while parsing file: %1</source>
        <translation>Chyba pri čítaní informácií zo súboru: %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="769"/>
        <source>No database opened!</source>
        <translation>Nie je otvorená žiadna databáza!</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="992"/>
        <source>Cannot read file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="994"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1091"/>
        <source>Symbolic link to file:#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1143"/>
        <source>Symbolic link to directory:#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1172"/>
        <source>DEAD Symbolic link to:#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1182"/>
        <source>System file (e.g. FIFO, socket or device file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2115"/>
        <source>Not enough memory to open the file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="959"/>
        <source>Reading file, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1087"/>
        <source>Parsing file, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1080"/>
        <source>Converting to unicode, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="422"/>
        <source>Please Wait...</source>
        <translation type="obsolete">Prosím čakajte...</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="588"/>
        <source>Cancel</source>
        <translation type="obsolete">Zrušiť</translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1094"/>
        <source>You have cancelled catalog reading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1876"/>
        <source>device </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1892"/>
        <source> link to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1208"/>
        <source>reading mp3 info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1262"/>
        <source>reading media info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1279"/>
        <source>reading avi info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1353"/>
        <source>reading file content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1407"/>
        <source>reading exif data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1436"/>
        <source>reading thumbnail data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1948"/>
        <source>scanning archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2044"/>
        <source>scanning archive, file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="571"/>
        <source>unknown</source>
        <translation type="unfinished">neznámy</translation>
    </message>
</context>
<context>
    <name>DirectoryView</name>
    <message>
        <location filename="../dirview.cpp" line="185"/>
        <source>Name</source>
        <translation>Názov</translation>
    </message>
</context>
<context>
    <name>GuiSlave</name>
    <message>
        <location filename="" line="0"/>
        <source>No opened database.</source>
        <translation type="obsolete">Nie je otvorená žiadna databáza.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No selected item.</source>
        <translation type="obsolete">Žiadna vybraná položka.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="622"/>
        <source>Directory</source>
        <translation>Adresár</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="679"/>
        <source>Catalog</source>
        <translation>Katalóg</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2274"/>
        <source>Unknown(DB)</source>
        <translation>Neznáma (DB)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2278"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2282"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2286"/>
        <source>HardDisc</source>
        <translation>HardDisk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2290"/>
        <source>Floppy</source>
        <translation>Disketa</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2294"/>
        <source>NetworkDrv</source>
        <translation>Sieťový disk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2298"/>
        <source>FlashDrv</source>
        <translation>FlashDisk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2302"/>
        <source>OtherDevice</source>
        <translation>Ostatné</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="710"/>
        <source>File</source>
        <translation>Súbor</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="926"/>
        <source>Hyper&apos;s CD Catalogizer (modified)</source>
        <translation>Hyperov CD katalogizér (upravený)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="932"/>
        <source>Hyper&apos;s CD Catalogizer</source>
        <translation>Hyperov CD katalogizér</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2198"/>
        <source>CdCat databases (*.hcf )</source>
        <translation>CdCat databázy (*.hcf)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1176"/>
        <source>Open a file...</source>
        <translation>Otvoriť súbor...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2847"/>
        <source>Error while opening...</source>
        <translation>Chyba pri otváraní...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1345"/>
        <source>Save to file...</source>
        <translation>Uložiť do súboru...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1370"/>
        <source>Error while saving...</source>
        <translation>Chyba pri ukladaní...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1418"/>
        <source>Closing previous database...</source>
        <translation>Zaváram predchádzajúcu databázu...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1418"/>
        <source>Do you want to save the changes?</source>
        <translation>Chcete uložiť zmeny?</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2914"/>
        <source>Yes</source>
        <translation>Áno</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2914"/>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2930"/>
        <source>Error:</source>
        <translation>Chyba:</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1434"/>
        <source>Do you really want to delete this?: 
%1</source>
        <translation type="obsolete">Naozaj zmazať nasledovnú položku?: 
%1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2912"/>
        <source>Confirmation</source>
        <translation>Potvrdenie</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1811"/>
        <source>Warning...</source>
        <translation>Varovanie...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1957"/>
        <source>You can refresh ONLY a MEDIA node!</source>
        <translation>Aktualizovať je možné iba položku MÉDIA!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2198"/>
        <source>Insert a database file...</source>
        <translation>Vložiť databázový súbor...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The Resoult:</source>
        <translation type="obsolete">Výsledky:</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2379"/>
        <source>The size of &quot;%1&quot; : 
 %2 
 %3 file /%4 directory</source>
        <translation>Veľkosť položky &quot;%1&quot; :
%2
%3 súbor(ov) /%4 adresár(ov)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1541"/>
        <source>Cannot mount CD</source>
        <translation>Nemožno pripojiť CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1887"/>
        <source>Cannot eject CD!</source>
        <translation>Nemožno vysunúť CD!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1541"/>
        <source>I can&apos;t find the &quot;mount&quot; program</source>
        <translation>Nemožno nájsť program &quot;mount&quot;</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1905"/>
        <source>Cannot mount CD!</source>
        <translation>Nemožno pripojiť CD!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1853"/>
        <source>Cannot eject CD</source>
        <translation>Nemožno vysunúť CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1853"/>
        <source>I can&apos;t find the &quot;eject&quot; program</source>
        <translation>Nemožno nájsť program &quot;eject&quot;</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1042"/>
        <source>Rename node...</source>
        <translation>Premenovať položku...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1043"/>
        <source>Delete node</source>
        <translation>Zmazať položku</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1052"/>
        <source>Rescan media...</source>
        <translation>Aktualizovať médium...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2542"/>
        <source>Add media...</source>
        <translation>Pridať médium...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1077"/>
        <source>Insert Catalog...</source>
        <translation>Vložiť katalóg...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1961"/>
        <source>Rescan %1</source>
        <translation>Aktualizovať %1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1053"/>
        <source>Re-Number media...</source>
        <translation>Zmeň číslo média...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2322"/>
        <source>It is NOT a media node!</source>
        <translation>Toto nie je uzol média!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1037"/>
        <source>Node size</source>
        <translation>Veľkosť položky</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1034"/>
        <source>View/Edit Comment...</source>
        <translation>Zobraziť/Editovať komentár...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1081"/>
        <source>Close all branch</source>
        <translation>Zatvoriť všetky vetvy</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="605"/>
        <source>Show/Remove Content...</source>
        <translation type="obsolete">Zobraziť/Odstrániť obsah...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2930"/>
        <source>There is no selected item in the middle list box!</source>
        <translation>V strednom paneli nie je vybraná žiadna položka!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1811"/>
        <source>An error occured while scanning,
the DataBase may be incomplete</source>
        <translation>Nastala chyba pri skenovaní,
databáza pravdepodobne nebude kompletná</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1953"/>
        <source>There is no selected Media in the middle list box!</source>
        <translation>V strednom paneli nie je vybrané žiadne médium!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2027"/>
        <source>An error occured while scanning, the rescan operation was cancelled: 
%1</source>
        <translation>Nastala chyba pri skenovaní, aktualizácia bola zrušená:
%1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1048"/>
        <source>Borrow this media to...</source>
        <translation>Zapožičať médium...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2913"/>
        <source>Do you want to clear the borrowing mark from media &quot;%1&quot;?
(Say yes if you got it back.)</source>
        <translation>Naozaj zmazať informácie o zapožičaní média:
&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1050"/>
        <source>I got it back! (clear borrowing mark)</source>
        <translation>Mám ho späť! (zmazať informácie o zapožičaní)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The database file has newer version than this version of cdcat knows:
I understand maximum %1 datafile version but readed %2

Strongly recommended to upgrade your cdcat!!!
Homepage: %3</source>
        <translation type="obsolete">Súbor databázy má novšiu verziu formátu, a táto verzia programu CdCat ju nevie spracovať :
Rozumie maximálne verzii %1, tento súbor je vo verzii %2.

Aktualizujte si program CdCat!!!
Domovská stránka: %3</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="595"/>
        <source>Number</source>
        <translation>Číslo</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="598"/>
        <source>Size</source>
        <translation>Veľkosť</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="713"/>
        <source>Catalog Link</source>
        <translation>Linka ku katalógu</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="991"/>
        <source>Follow the link (Open it) !</source>
        <translation>Otvor súbor na ktorý ukazuje linka!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="777"/>
        <source>Add a link to a CdCAt Catalog...</source>
        <translation type="obsolete">Pridaj linku ku katalógu CdCat...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2807"/>
        <source>The link is empty.</source>
        <translation>Linka neobsahuje žiadnu informáciu.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="343"/>
        <source>The database file has newer version than this version of cdcat can work with:
I understand maximum %1 datafile version but readed %2

Strongly recommended to upgrade your cdcat!!!
Homepage: %3</source>
        <translation>Súbor databázy má novšiu verziu formátu, a táto verzia programu CdCat ju nevie spracovať :
Rozumie maximálne verzii %1, tento súbor je vo verzii %2.

Aktualizujte si program CdCat!!!
Domovská stránka: %3</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="354"/>
        <source>No database opened.</source>
        <translation>Nie je otvorená žiadna databáza.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="356"/>
        <source>No item selected.</source>
        <translation>Nie je vybraná žiadna položka.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The resoult:</source>
        <translation type="obsolete">Výsledok:</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1962"/>
        <source>Select directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2384"/>
        <source>The result:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1985"/>
        <source>Scanning directory tree, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1035"/>
        <source>View/Edit Category...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1057"/>
        <source>Change media type...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1718"/>
        <source>Enter media name...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1719"/>
        <source>Media name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1720"/>
        <source>Media</source>
        <translation type="unfinished">Médium</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1009"/>
        <source>search for duplicates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="983"/>
        <source>Show content...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="986"/>
        <source>Show/Remove content...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1014"/>
        <source>Add a link to a CdCat Catalog...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1076"/>
        <source>Add a link to a Cdcat catalog...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1770"/>
        <source>You have cancelled catalog scanning,
the DataBase may be incomplete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1988"/>
        <source>Scan started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1988"/>
        <source>Scanning %1 into %2 has been started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2003"/>
        <source>Scanning %1 into %2 has been finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2007"/>
        <source>Scan finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2007"/>
        <source>Scanning %1 into %2 has been finished (NOT complete)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2095"/>
        <source>Cdcat - idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2514"/>
        <source>Help</source>
        <translation type="unfinished">Pomoc</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2516"/>
        <source>Whats this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2519"/>
        <source>The cdcat is graphical (QT based) multiplatform (Linux/Windows) catalog program which scans the directories/drives you want and memorize the filesystem including the tags of mp3&apos;s and other data and store it in a small file. The database is stored in a gzipped XML format, so you can hack it, or use it if necessary :-).)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2521"/>
        <source>The program can store the content of some specified files up to a limit size if you want. (for example: *.nfo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2524"/>
        <source>Usage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2527"/>
        <source>Before the scanning select the necessary readable components in the config dialog, which can be mp3 tags content of some files or etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2531"/>
        <source>Create a new catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2534"/>
        <source>Run the %1 command in the catalog menu. You have to type the name of the new catalog. You can specify the default username of the media(which you scan later), and add a comment to the catalog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2534"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2538"/>
        <source>Add media</source>
        <translation type="unfinished">Pridať médium</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2542"/>
        <source>Run the %1 command in the edit menu. In that dialog you have to specify the directory/or drive the media you want to add. It&apos;s recommended to specify the name and the number of the media which has to be unique. (The program always generate one identical name and number). You can label the media to a owner, if necessary.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2547"/>
        <source>save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2551"/>
        <source>Open an existing catalog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2553"/>
        <source>open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2557"/>
        <source>Author:</source>
        <translation type="unfinished">Autor:</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2560"/>
        <source>The program was written by Peter Deak (hungary)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2563"/>
        <source>The current maintainer is %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2567"/>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2571"/>
        <source>General Public License (GPL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2575"/>
        <source>Homepage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2579"/>
        <source>You can read about the program and get new versions, sources etc, in the hompage of cdcat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2553"/>
        <source>Run the %1 command in the file menu, and choice the file of the catalog. (*.hcf). After the opening you will be able browse the catalog or search in it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2547"/>
        <source>If you scanned your media, you will be able to browse in it with the browser window (like mc) , or search in it. You can save the catalog with %1 command in the file menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1352"/>
        <source>Saving catalog, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2164"/>
        <source>Insert a cdcat exported xml file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2164"/>
        <source>CdCat xml export (*.xml )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1065"/>
        <source>Change password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2955"/>
        <source>Password has been successfully changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3007"/>
        <source>Password changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2994"/>
        <source>Password length is too short, must be minimum 4 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2994"/>
        <source>Password too short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2981"/>
        <source>Password length is too big, must be maximal %1 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2981"/>
        <source>Password too big</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1066"/>
        <source>Disable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1070"/>
        <source>Enable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2986"/>
        <source>Encryption has been successfully enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3007"/>
        <source>Encryption has been successfully disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2998"/>
        <source>Passwords not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2998"/>
        <source>Passwords does not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1453"/>
        <source>Do you really want to delete
&quot;%1&quot;?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HDirectoryView</name>
    <message>
        <location filename="../hdirview.cpp" line="322"/>
        <source>Directory Tree</source>
        <translation>Adresárový strom</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="323"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="362"/>
        <source>Copy</source>
        <translation type="obsolete">Kopírovať</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="365"/>
        <source>Move</source>
        <translation type="obsolete">Presunúť</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="369"/>
        <source>Link</source>
        <translation type="obsolete">Linkovať</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="373"/>
        <source>Unknown</source>
        <translation type="obsolete">Neznámy</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="387"/>
        <source>Drop target</source>
        <translation type="obsolete">Upustiť cieľ</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="431"/>
        <source>Not implemented</source>
        <translation type="obsolete">Nie je implementované</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="430"/>
        <source>Drag source</source>
        <translation type="obsolete">Pretiahnuť zdroj</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="431"/>
        <source>Delete </source>
        <translation type="obsolete">Zmazať</translation>
    </message>
</context>
<context>
    <name>HQListView</name>
    <message>
        <location filename="../guibase.cpp" line="198"/>
        <source>Name</source>
        <translation>Názov</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="199"/>
        <source>Size</source>
        <translation>Veľkosť</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="200"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
</context>
<context>
    <name>ImportDialog</name>
    <message>
        <location filename="../importdialog.cpp" line="80"/>
        <source>Create new Database</source>
        <translation>Vytvoriť novú databázu</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Seperator:</source>
        <translation type="obsolete">Oddeľovač:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>This is the seperator in dataline
&lt;path&gt;SEPERATOR&lt;size&gt;SEPERATOR&lt;date&gt;&lt;space&gt;&lt;time&gt;</source>
        <translation type="obsolete">&lt;media&gt;/&lt;path&gt;/.../&lt;file&gt;SEP&lt;size&gt;SEP&lt;day/month/year&gt;SPACE&lt;hour&gt;:&lt;minute&gt;:&lt;second&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Seperator in data line</source>
        <translation type="obsolete">Oddeľovač v riadku</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="186"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="187"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="230"/>
        <source>Open file dialog</source>
        <translation type="obsolete">Dialóg &quot;Otvoriť súbor&quot;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="234"/>
        <source>Corrects bad output style (gtktalog)</source>
        <translation type="obsolete">Opraviť zlý výstup (GtKatalog)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="243"/>
        <source>Error:</source>
        <translation>Chyba:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="304"/>
        <source>csv files(*.csv)</source>
        <translation>CSV súbory(*.csv)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>open file dialog</source>
        <translation type="obsolete">dialóg &quot;Otvoriť súbor&quot;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="327"/>
        <source>Choose a file for import</source>
        <translation>Vyberte súbor pre import</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="190"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;XML (gtktalog)</source>
        <translation type="obsolete">&amp;XML (gtktalog)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="230"/>
        <source>&lt;strong&gt;Please read the README_IMPORT before you import!&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Pred importovaním databázy si prečítajte súbor README_IMPORT!&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="310"/>
        <source>xml files(*.xml)</source>
        <translation>XML súbory(*.xml)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Correct bad style of gtktalog export</source>
        <translation type="obsolete">Opraviť zlý výstup exportu gtkatalog</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select the type of import</source>
        <translation type="obsolete">Vyberte typ importu</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="180"/>
        <source>Import CSV file</source>
        <translation>Importovať CSV súbor</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="181"/>
        <source>File:</source>
        <translation>Súbor:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File dialog for selecting file to import.</source>
        <translation type="obsolete">Otvorí dialóg pre vyhľadanie súboru pre import.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Corrects bad output style of gtktalog.
&lt;media&gt;SEPERATOR/&lt;dir&gt;/SEPERATOR/&lt;dir&gt;
 will be to
&lt;media&gt;/&lt;dir&gt;/&lt;dir&gt;</source>
        <translation type="obsolete">Opraviť zlý výstup exportu gtkatalog</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Text (CSV)</source>
        <translation type="obsolete">&amp;Text (CSV)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select this for importing text file (CSV)</source>
        <translation type="obsolete">Import z textových súborov (formát CSV)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select this for importing a XML report generated with gtktalog</source>
        <translation type="obsolete">Import XML súborov generovaných gtkatalógom</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>You must fill the &quot;Filename&quot; field!</source>
        <translation type="obsolete">Nebolo zadané meno súboru !</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>You must fill the &quot;Seperator&quot; field!</source>
        <translation type="obsolete">Nebol zadaný separátor !</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="87"/>
        <source>Correct bad style from gtktalog export</source>
        <translation>Opraviť zlý výstup exportu gtkatalog</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="184"/>
        <source>Separator:</source>
        <translation>Oddeľovač:</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="185"/>
        <source>This is the separator in dataline
&lt;path&gt;SEPARATOR&lt;size&gt;SEPARATOR&lt;date&gt;&lt;space&gt;&lt;time&gt;</source>
        <translation>Toto je separátor v riadku
&lt;cesta&gt;SEPARÁTOR&lt;veľkosť&gt;SEPARÁTOR&lt;dátum&gt;&lt;medzera&gt;&lt;čas&gt;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="173"/>
        <source>Separator in data line</source>
        <translation type="obsolete">Separátor v riadku</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="188"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Text (csv)</source>
        <translation type="obsolete">&amp;Text (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="200"/>
        <source>Gtktalog &amp;XML</source>
        <translation>Gtktalog &amp;XML</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="201"/>
        <source>&amp;WhereIsIt XML (classic)</source>
        <translation>&amp;WhereIsIt XML (classic)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="203"/>
        <source>Select the type of import here</source>
        <translation>Zadajte typ importu</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select this for importing a text import (csv)</source>
        <translation type="obsolete">Vyberte toto pre importovanie textu (csv)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select this for importing a xml report generated frm gtktalog</source>
        <translation type="obsolete">Import XML súborov generovaných gtkatalógom</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="227"/>
        <source>Open the file dialog for selecting file to import.</source>
        <translation>Vyber súbor pre import.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="229"/>
        <source>Corrects bad output style from gtktalog.
&lt;media&gt;SEPARATOR/&lt;dir&gt;/SEPARATOR/&lt;dir&gt;
 will be to
&lt;media&gt;/&lt;dir&gt;/&lt;dir&gt;</source>
        <translation>Opraviť zlý výstup exportu gtkatalog.
&lt;médium&gt;SEPARÁTOR/&lt;adresár&gt;/SEPARÁTOR/&lt;adresár&gt;
na
&lt;médium&gt;/&lt;adresár&gt;/&lt;adresár&gt;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="238"/>
        <source>You must be fill the &quot;Filename&quot; field!</source>
        <translation>Nebolo zadané meno súboru !</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="243"/>
        <source>You must be fill the &quot;Separator&quot; field!</source>
        <translation>Nebolo zadaný separátor!</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="315"/>
        <source>all files(*.*)</source>
        <translation>všetky súbory (*.*)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="191"/>
        <source>&amp;Gtktalog (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="205"/>
        <source>Select this for importing a text import (csv) generated from Gtktalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="192"/>
        <source>&amp;Kat-CeDe (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="207"/>
        <source>Select this for importing a text import (csv) generated from Kat-CeDe.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="223"/>
        <source>Select this for importing a xml report generated from gtktalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="225"/>
        <source>Select this for importing a xml report generated from WhereIsIt?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="193"/>
        <source>&amp;Disclib (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="209"/>
        <source>Select this for importing a text import (csv) generated from Disclib.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="194"/>
        <source>&amp;VisualCD (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="211"/>
        <source>Select this for importing a text import (csv) generated from VisualCD.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="195"/>
        <source>&amp;VVV (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="213"/>
        <source>Select this for importing a text import (csv) generated from VVV.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="196"/>
        <source>&amp;Advanced File Organizer (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="215"/>
        <source>Select this for importing a text import (csv) generated from Advanced File Organizer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="197"/>
        <source>&amp;File Archivist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="217"/>
        <source>Select this for importing a File Archivist catalog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="313"/>
        <source>File Archivist files(*.arch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="198"/>
        <source>&amp;Advanced Disk Catalog (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="219"/>
        <source>Select this for importing a text import (csv) generated from Advanced Disk Catalog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="199"/>
        <source>W&amp;hereIsIt (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="221"/>
        <source>Select this for importing a text import (csv) generated from WhereIsIt.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../info.cpp" line="143"/>
        <source>German translation &amp; additional programming</source>
        <translation>Nemecký preklad &amp; programovanie</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Spanish translation</source>
        <translation>Španielský preklad</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="163"/>
        <source>Czech translation</source>
        <translation>Český preklad</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="456"/>
        <source>About cdcat</source>
        <translation>O programe</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="457"/>
        <source>close</source>
        <translation>zatvoriť</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="460"/>
        <source>About Cdcat</source>
        <translation>O programe</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="463"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="465"/>
        <source>Thanks</source>
        <translation>Poďakovanie</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="83"/>
        <source>-= CdCatalog by Hyper =-</source>
        <translation>-= CdKatalogizér od Hypera =-</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="87"/>
        <source>Version:</source>
        <translation>Verzia:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="132"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="134"/>
        <source>Copyright (C) 2003 Peter Deak (GPL)</source>
        <translation>Copyright (C) 2003 Peter Deak (GPL)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Polish translation</source>
        <translation type="obsolete">Poľský preklad</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="151"/>
        <source>AVI reader plugin &amp; Polish translation</source>
        <translation>AVI plugin &amp; Poľský preklad</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="136"/>
        <source>Copyright (C) 2010 Christoph Thielecke (GPL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="145"/>
        <source>Ported to Qt4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="89"/>
        <source> (with debug)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <source>Development version build at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="170"/>
        <source>For more details about the GPL license and to read in other languages, visit %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="170"/>
        <source>GPL page on GNU website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="107"/>
        <source>Features:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="111"/>
        <source>archive read support using lib7zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="115"/>
        <source>mediainfo (compiled in)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="119"/>
        <source>mediainfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="123"/>
        <source>exif data read support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="127"/>
        <source>encrypted catalog support</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyBindingDialog</name>
    <message>
        <location filename="../info.cpp" line="479"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="481"/>
        <source>Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="535"/>
        <source>Key bindings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="536"/>
        <source>close</source>
        <translation type="unfinished">zatvoriť</translation>
    </message>
</context>
<context>
    <name>PWw</name>
    <message>
        <location filename="../adddialog.cpp" line="488"/>
        <source>Please Wait...</source>
        <translation type="unfinished">Prosím čakajte...</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="514"/>
        <source>Cancel</source>
        <translation type="obsolete">Zrušiť</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="588"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Zrušiť</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../hdirview.cpp" line="254"/>
        <source>Directory</source>
        <translation>Adresár</translation>
    </message>
    <message>
        <location filename="../dirview.cpp" line="148"/>
        <source>Unreadable Directory</source>
        <translation>Nečitateľný adresár</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="232"/>
        <source>Catalog</source>
        <translation>Katalóg</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="236"/>
        <source>Unknown(DB)</source>
        <translation>Neznáma (DB)</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="238"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="240"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="242"/>
        <source>HardDisc</source>
        <translation>HardDisk</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="244"/>
        <source>Floppy</source>
        <translation>Disketa</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="246"/>
        <source>NetworkDrv</source>
        <translation>Sieťový disk</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="248"/>
        <source>FlashDrv</source>
        <translation>FlashDisk</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="250"/>
        <source>OtherDevice</source>
        <translation>Ostatné</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="640"/>
        <source>Video:
</source>
        <translation>Video:
</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="642"/>
        <source>Total Time</source>
        <translation>Celkový čas</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="643"/>
        <source>Framerate</source>
        <translation>Snímková frekvencia</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="644"/>
        <source>Resolution</source>
        <translation>Rozlíšenie</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Audio:</source>
        <translation type="obsolete">Zvuk:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>More Data:</source>
        <translation type="obsolete">Ďalšie detaily:</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="656"/>
        <source>Artist</source>
        <translation>Interpret</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="658"/>
        <source>Comments</source>
        <translation>Komentár</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="660"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="662"/>
        <source>Creation Date</source>
        <translation>Dátum vytvorenia</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="664"/>
        <source>Engineer</source>
        <translation>Inžinier</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="668"/>
        <source>Name</source>
        <translation>Názov</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="670"/>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="672"/>
        <source>Software</source>
        <translation>Softvér</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="674"/>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="676"/>
        <source>Subject</source>
        <translation>Subjekt</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="678"/>
        <source>Technician</source>
        <translation>Technik</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="763"/>
        <source>Unknown/not implemented/broken header
</source>
        <translation>Neznáma/neimplementovaná/poškodená hlavička
</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="648"/>
        <source>Channels</source>
        <translation>Kanály</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Samples/s</source>
        <translation type="obsolete">Sample</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="650"/>
        <source>Bitrate</source>
        <translation>Dátový tok</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="666"/>
        <source>Genre</source>
        <translation>Žáner</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="492"/>
        <source>Not a RIFF/AVI file OR header broken!</source>
        <translation>Toto nie je RIFF/AVI súbor alebo má poškodenú hlavičku!</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="649"/>
        <source>Sample/s</source>
        <translation>Vzorky</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2411"/>
        <source>importuser</source>
        <translation type="unfinished">vlastník importu</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="646"/>
        <source>Audio:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="653"/>
        <source>More Data:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1004"/>
        <source>Enter password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1004"/>
        <source>Enter password for catalog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="230"/>
        <source>Catalog (encrypted)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1008"/>
        <source>password empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1014"/>
        <source>cant set password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1034"/>
        <source>decrypt failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1047"/>
        <source>cant load catalog: encrypted catalog support not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2976"/>
        <source>Enter password for catalog (again):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelReadable</name>
    <message>
        <location filename="../selreadable.cpp" line="836"/>
        <source>Select readable items</source>
        <translation>Výber ďalších položiek pre čítanie</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="848"/>
        <source>Read mp3 tags</source>
        <translation>Čítať MP3 tagy</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="864"/>
        <source>Store content of some files</source>
        <translation>Ukladať obsah niektorých súborov</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="885"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="886"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="875"/>
        <source>content size limit in kByte</source>
        <translation>maximálna veľkosť pre ukladanie obsahu v kB</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="870"/>
        <source>; separated list of readable file patterns</source>
        <translation>&quot;;&quot; oddelený zoznam vzorov načítaných súborov</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="859"/>
        <source>Default tag</source>
        <translation>Predvolený tag</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="120"/>
        <source>version</source>
        <translation>verzia</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="861"/>
        <source>Read mp3 technical info as comment (bitrate,freq,length...)</source>
        <translation>Ukladať parametre MP3 súborov (bitrate, frekvencia, dĺžka,komentár...)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Read avi technical info as comment (codecs,lenhth,...)</source>
        <translation type="obsolete">Ukladať informácie o avi súboroch (kodeky, dĺžka, komentáre...)</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="863"/>
        <source>Read avi technical info as comment (codecs,length,...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="841"/>
        <source>Supported extensions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="837"/>
        <source>Archive file display options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="838"/>
        <source>Scan for archive file list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="868"/>
        <source>Read some technical info using mediainfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="850"/>
        <source>Read thumbnails from pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="856"/>
        <source>Read EXIF data from pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="854"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="872"/>
        <source>; separated list of image file extensions, e.g. png;jpg;gif</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="622"/>
        <source>Path to external content viewer (found)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="629"/>
        <source>Path to external content viewer (not found)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="606"/>
        <source>Choose path to external context viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="866"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="867"/>
        <source>Select external viewer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="175"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="413"/>
        <source>lib7zip found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="417"/>
        <source>lib7zip not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="420"/>
        <source>Archive support:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="449"/>
        <source>mediainfo not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="451"/>
        <source>mediainfo found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="456"/>
        <source>mediainfo status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="842"/>
        <source>Permission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="843"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="844"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="845"/>
        <source>Size</source>
        <translation type="unfinished">Veľkosť</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="846"/>
        <source>Date</source>
        <translation type="unfinished">Veľkosť</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="847"/>
        <source>Comment</source>
        <translation type="unfinished">Komentár</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="849"/>
        <source>Read thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="851"/>
        <source>Size:</source>
        <translation type="unfinished">Veľkosť:</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="852"/>
        <source>Thumbnail size (width) in pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="853"/>
        <source>Thumbnail size (height) in pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="865"/>
        <source>Use external file content viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="869"/>
        <source>file patterns:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="871"/>
        <source>File extensions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="874"/>
        <source>max size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="860"/>
        <source>Read mp3 info as comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="862"/>
        <source>Read avi info as comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="667"/>
        <source>Add exclude rule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="667"/>
        <source>Enter regular expression for exclude:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="651"/>
        <source>edit rule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="655"/>
        <source>delete rule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="876"/>
        <source>exclude files/directories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="883"/>
        <source>add exclude rule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="884"/>
        <source>list of patterns (regular expression) for files/directories to skip on reading from filesystem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="695"/>
        <source>regular expression is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="699"/>
        <source>regular expression is valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="840"/>
        <source>show archive file at scanning in status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="839"/>
        <source>show archive file in status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="804"/>
        <source>About regular expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="806"/>
        <source>close</source>
        <translation type="unfinished">zatvoriť</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="879"/>
        <source>About regex:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="880"/>
        <source>Information about regular expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="881"/>
        <source>About regular expressions....</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="882"/>
        <source>Show introduction into regular expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="744"/>
        <source>Metacharacter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="745"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="753"/>
        <source>A bracket expression. Matches a single character that is contained within the brackets. For example, &lt;code&gt;[abc]&lt;/code&gt; matches &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;. &lt;code&gt;[a-z]&lt;/code&gt; specifies a range which matches any lowercase letter from &quot;&lt;i&gt;a&lt;/i&gt;&quot; to &quot;&lt;i&gt;z&lt;/i&gt;&quot;. These forms can be mixed: &lt;code&gt;[abcx-z]&lt;/code&gt; matches &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, &quot;&lt;i&gt;c&lt;/i&gt;&quot;, &quot;&lt;i&gt;x&lt;/i&gt;&quot;, &quot;&lt;i&gt;y&lt;/i&gt;&quot;, or &quot;&lt;i&gt;z&lt;/i&gt;&quot;, as does &lt;code&gt;[a-cx-z]&lt;/code&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="754"/>
        <source>The &lt;code&gt;-&lt;/code&gt; character is treated as a literal character if it is the last or the first (after the &lt;code&gt;^&lt;/code&gt;) character within the brackets: &lt;code&gt;[abc-]&lt;/code&gt;, &lt;code&gt;[-abc]&lt;/code&gt;. Note that backslash escapes are not allowed. The &lt;code&gt;]&lt;/code&gt; character can be included in a bracket expression if it is the first (after the &lt;code&gt;^&lt;/code&gt;) character: &lt;code&gt;[]abc]&lt;/code&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="759"/>
        <source>Matches a single character that is not contained within the brackets. For example, &lt;code&gt;[^abc]&lt;/code&gt; matches any character other than &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;. &lt;code&gt;[^a-z]&lt;/code&gt; matches any single character that is not a lowercase letter from &quot;&lt;i&gt;a&lt;/i&gt;&quot; to &quot;&lt;i&gt;z&lt;/i&gt;&quot;. Likewise, literal characters and ranges can be mixed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="763"/>
        <source>Matches the starting position within the string. In line-based tools, it matches the starting position of any line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="776"/>
        <source>Matches what the &lt;i&gt;n&lt;/i&gt;th marked subexpression matched, where &lt;i&gt;n&lt;/i&gt; is a digit from 1 to 9. This construct is theoretically &lt;b&gt;irregular&lt;/b&gt; and was not adopted in the POSIX ERE syntax. Some tools allow referencing more than nine capturing groups.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="780"/>
        <source>Matches the preceding element zero or more times. For example, &lt;code&gt;ab*c&lt;/code&gt; matches &quot;&lt;i&gt;ac&lt;/i&gt;&quot;, &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, &quot;&lt;i&gt;abbbc&lt;/i&gt;&quot;, etc. &lt;code&gt;[xyz]*&lt;/code&gt; matches &quot;&quot;, &quot;&lt;i&gt;x&lt;/i&gt;&quot;, &quot;&lt;i&gt;y&lt;/i&gt;&quot;, &quot;&lt;i&gt;z&lt;/i&gt;&quot;, &quot;&lt;i&gt;zx&lt;/i&gt;&quot;, &quot;&lt;i&gt;zyx&lt;/i&gt;&quot;, &quot;&lt;i&gt;xyzzy&lt;/i&gt;&quot;, and so on. &lt;code&gt;\(ab\)*&lt;/code&gt; matches &quot;&quot;, &quot;&lt;i&gt;ab&lt;/i&gt;&quot;, &quot;&lt;i&gt;abab&lt;/i&gt;&quot;, &quot;&lt;i&gt;ababab&lt;/i&gt;&quot;, and so on.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="788"/>
        <source>Examples:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="790"/>
        <source>matches any three-character string ending with &quot;at&quot;, including &quot;&lt;i&gt;hat&lt;/i&gt;&quot;, &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, and &quot;&lt;i&gt;bat&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="791"/>
        <source>matches &quot;&lt;i&gt;hat&lt;/i&gt;&quot; and &quot;&lt;i&gt;cat&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="792"/>
        <source>matches all strings matched by &lt;code&gt;.at&lt;/code&gt; except &quot;&lt;i&gt;bat&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="793"/>
        <source>matches &quot;&lt;i&gt;hat&lt;/i&gt;&quot; and &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, but only at the beginning of the string or line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="794"/>
        <source>matches &quot;&lt;i&gt;hat&lt;/i&gt;&quot; and &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, but only at the end of the string or line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="798"/>
        <source>Source:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="795"/>
        <source>matches any single character surrounded by &quot;[&quot; and &quot;]&quot; since the brackets are escaped, for example: &quot;&lt;i&gt;[a]&lt;/i&gt;&quot; and &quot;&lt;i&gt;[b]&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="707"/>
        <source>POSIX Basic Regular Expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="877"/>
        <source>Use wildcard instead regex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="878"/>
        <source>Use wildcard expression instead regular expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="406"/>
        <source>lib7zip not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="411"/>
        <source>unknown</source>
        <translation type="unfinished">neznámy</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="457"/>
        <source>support not compiled in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="455"/>
        <source>mediainfo not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="873"/>
        <source>Supported image extensions found: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="749"/>
        <source>Matches any single character (many applications exclude &lt;a href=&quot;http://en.wikipedia.org/wiki/Newline&quot; title=&quot;Newline&quot;&gt;newlines&lt;/a&gt;, and exactly which characters are considered newlines is flavor-, character-encoding-, and platform-specific, but it is safe to assume that the line feed character is included). Within POSIX bracket expressions, the dot character matches a literal dot. For example, &lt;code&gt;a.c&lt;/code&gt; matches &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, etc., but &lt;code&gt;[a.c]&lt;/code&gt; matches only &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;.&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="798"/>
        <source>&lt;a href=&quot;http://en.wikipedia.org/wiki/Regex&quot;&gt;regular expressions&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="800"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowContent</name>
    <message>
        <location filename="../showcontent.cpp" line="116"/>
        <source>Content of %1</source>
        <translation>Obsah položky %1</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="206"/>
        <source>Close</source>
        <translation>Zatvoriť</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="252"/>
        <source>Confirmation</source>
        <translation>Potvrdenie</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Are you sure want to delete this file&apos;s content from the database?</source>
        <translation type="obsolete">Chcete naozaj zmazať obsah tohto súboru z databázy?</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="255"/>
        <source>Yes</source>
        <translation>Áno</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="255"/>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="71"/>
        <source>Remove the file content from the database. (Warning: Unrecoverable!)</source>
        <translation>Odstrániť obsah súboru z databázy. (Pozor: Nevratné!)</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="61"/>
        <source>%1 kByte</source>
        <translation>%1 kB</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="75"/>
        <source>Save this content to a new file</source>
        <translation>Uložiť obsah do nového súboru</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save content...</source>
        <translation type="obsolete">Uložiť obsah...</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="229"/>
        <source>Select a filename below</source>
        <translation>Vyberte názov súboru</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="233"/>
        <source>I can&apos;t rewrite the file: %1</source>
        <translation>Nemožno prepísať súbor: %1</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="234"/>
        <source>Error while saving...</source>
        <translation>Chyba pri ukladaní...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="254"/>
        <source>Sure to delete this file&apos;s content from the database?</source>
        <translation>Chcete naozaj zmazať obsah tohto súboru z databázy?</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="229"/>
        <source>CdCat databases (*.hcf )</source>
        <translation type="unfinished">CdCat databázy (*.hcf)</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="92"/>
        <source>Category of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="202"/>
        <source>Set category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="204"/>
        <source>Set content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="205"/>
        <source>select font for display</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>addDialog</name>
    <message>
        <location filename="../adddialog.cpp" line="77"/>
        <source>Directory Browser</source>
        <translation>Prechádzať adresáre</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="323"/>
        <source>New Disk %1</source>
        <translation>Nový Disk %1</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="245"/>
        <source>Add Media to the Database</source>
        <translation>Pridať médium do databázy</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="246"/>
        <source>Choose a directory to scan:</source>
        <translation>Vyberte adresár na skenovanie:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Media Name:</source>
        <translation type="obsolete">Názov média:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Serial number of Media:</source>
        <translation type="obsolete">Sériové číslo média:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Owner:</source>
        <translation type="obsolete">Vlastník:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Type:</source>
        <translation type="obsolete">Typ:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Comment:</source>
        <translation type="obsolete">Komentár:</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="656"/>
        <source>Cancel</source>
        <translation type="obsolete">Zrušiť</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK / Scan</source>
        <translation type="obsolete">OK / Skenuj</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="261"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="262"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="263"/>
        <source>HardDisc</source>
        <translation>HardDisk</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="264"/>
        <source>Floppy</source>
        <translation>Disketa</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="265"/>
        <source>NetworkPlace</source>
        <translation>Sieťový disk</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="266"/>
        <source>FlashDrive</source>
        <translation>FlashDisk</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="267"/>
        <source>OtherDevice</source>
        <translation>Ostatné</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="398"/>
        <source>Error:</source>
        <translation>Chyba:</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="367"/>
        <source>You must be fill the &quot;Name&quot; field!</source>
        <translation>Nebolo zadané meno súboru !</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="657"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="343"/>
        <source>The Media Name must be unique! Please change it!</source>
        <translation type="obsolete">Názov média musí byť jednoznačný! Zmeňte ho prosím!</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select readable items</source>
        <translation type="obsolete">Výber ďalších položiek pre čítanie</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="393"/>
        <source>The Value of Serial Number must be unique! Please change it!</source>
        <translation>Sériové číslo musí byť jednoznačné! Zmeňte ho, prosím!</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="398"/>
        <source>You haven&apos;t selected a directory! Please select one!</source>
        <translation>Nie je vybraný žiadny adresár!</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="372"/>
        <source>The media name can&apos;t begin with the &quot;@&quot; character!</source>
        <translation>Názov média nemôže začínať znakom &quot;@&quot; !</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please Wait...</source>
        <translation type="obsolete">Prosím čakajte...</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="653"/>
        <source>Add a &quot;Catalog Link&quot; to the database</source>
        <translation type="obsolete">Pridať &quot;Katalógovú linku&quot; k databáze</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="654"/>
        <source>Location of the .hcf file:</source>
        <translation type="obsolete">Cesta k súboru .hcf:</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="674"/>
        <source>Open a file...</source>
        <translation type="obsolete">Otvoriť súbor...</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="674"/>
        <source>CdCat databases (*.hcf )</source>
        <translation type="obsolete">CdCat databázy (*.hcf)</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="247"/>
        <source>Media &amp;Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="248"/>
        <source>S&amp;erial number of Media:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="249"/>
        <source>&amp;Owner:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="250"/>
        <source>C&amp;ategory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="251"/>
        <source>&amp;Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="252"/>
        <source>Co&amp;mment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="253"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Zrušiť</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="254"/>
        <source>&amp;Scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="255"/>
        <source>Select &amp;readable items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="257"/>
        <source>detect CDROM/DVD med&amp;ia name after mount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="383"/>
        <source>Enter media name...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="383"/>
        <source>The Media Name must be unique! Enter new media name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="260"/>
        <source>unknown</source>
        <translation type="unfinished">neznámy</translation>
    </message>
</context>
<context>
    <name>borrowDialog</name>
    <message>
        <location filename="../borrow.cpp" line="116"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="117"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="75"/>
        <source>I borrow the &quot;%1&quot; named media to:</source>
        <translation>Požičiavam médium &quot;%1&quot;:</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="87"/>
        <source>unknown</source>
        <translation>neznámy</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="115"/>
        <source>I borrow the &quot;&quot; named media to:</source>
        <translation>Požičiavam médium &quot;&quot;:</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="114"/>
        <source>Borrowing...</source>
        <translation>Požičiavanie...</translation>
    </message>
</context>
<context>
    <name>borrowingDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>Borrwing info...</source>
        <translation type="obsolete">Informácie o zapožičaní...</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="211"/>
        <source>Media borrowing info:</source>
        <translation>Informácie o zapožičaní média:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Only show borrowed items</source>
        <translation type="obsolete">Zobraz len požičané položky</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Clear all borrowing</source>
        <translation type="obsolete">Vymaž info o požičaní</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="248"/>
        <source>Media</source>
        <translation>Médium</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="249"/>
        <source>Borrowed</source>
        <translation>Požičané</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="250"/>
        <source>where is it now?</source>
        <translation>kde je teraz ?</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="220"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="256"/>
        <source>0</source>
        <translation type="obsolete">0</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="456"/>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="463"/>
        <source>Yes</source>
        <translation>Áno</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="325"/>
        <source>unknown</source>
        <translation>neznámy</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="329"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="329"/>
        <source>Set &quot;Yes&quot; or &quot;No&quot; !</source>
        <translation>Zvoľte &quot;Áno&quot; alebo &quot;Nie&quot; !</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="391"/>
        <source>Warning</source>
        <translation>Varovanie</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="352"/>
        <source>Do you save the changes?</source>
        <translation>Chcete uložiť zmeny?</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="391"/>
        <source>Are you sure want to clear all borrow?</source>
        <translation>Naozaj zmazať všetky informácie o zapožičaní ?</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="422"/>
        <source>I got it back!</source>
        <translation>Mám to späť!</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="427"/>
        <source>&lt;&lt; </source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="210"/>
        <source>Borrowing info...</source>
        <translation>Informácie o zapožičaní...</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="212"/>
        <source>Show only borrowed items</source>
        <translation>Zobraz len požičané položky</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="213"/>
        <source>Clear all borrowing info</source>
        <translation>Zmazať všetky informácie o požičaní</translation>
    </message>
</context>
<context>
    <name>commentEdit</name>
    <message>
        <location filename="" line="0"/>
        <source>Edit Comment</source>
        <translation type="obsolete">Editovať komentár</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1059"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1060"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1056"/>
        <source>Edit comment of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1058"/>
        <source>Edit category of</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>exportCdcatDB</name>
    <message>
        <location filename="../exportcdcatdb.cpp" line="310"/>
        <source>Availiable media</source>
        <translation>Dostupné médiá</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="317"/>
        <source>Media to export</source>
        <translation>Médiá na export</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="318"/>
        <source>All media</source>
        <translation>Všetky médiá</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="326"/>
        <source>separator:</source>
        <translation>oddeľovač:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="327"/>
        <source>File to export:</source>
        <translation>Súbor pre export:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="328"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="329"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="330"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Zrušiť</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="398"/>
        <source>Filename missing</source>
        <translation>Chýba meno súboru</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="398"/>
        <source>Please enter a filename!</source>
        <translation>Prosím zadajte názov súboru!</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="401"/>
        <source>Separator missing</source>
        <translation>Chýba separátor</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="401"/>
        <source>Please enter a separator!</source>
        <translation>Prosím zadajte separátor!</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error during write config...</source>
        <translation type="obsolete">Chyba pri ukladaní konfigurácie...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="713"/>
        <source>All files (*.*)</source>
        <translation>Všetky súbory (*.*)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>file chooser</source>
        <translation type="obsolete">výber súboru</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="713"/>
        <source>Choose a file!</source>
        <translation>Vyberte súbor!</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="315"/>
        <source>Add media</source>
        <translation>Pridať médium</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="313"/>
        <source>Remove media</source>
        <translation>Vymazať médium</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="321"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="322"/>
        <source>export as HTML</source>
        <translation>exportovať ako HTML</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="323"/>
        <source>export as CSV</source>
        <translation>exportovať ako CSV</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="482"/>
        <source>Media</source>
        <translation>Médium</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="467"/>
        <source>File</source>
        <translation>Súbor</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="509"/>
        <source>Size</source>
        <translation>Veľkosť</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="514"/>
        <source>Date</source>
        <translation>Veľkosť</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="610"/>
        <source>I can&apos;t create or rewrite the file</source>
        <translation>Nemožno vytvoriť alebo prepísať súbor</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="441"/>
        <source>Export from cdcat:</source>
        <translation>Export z cdcatu:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="60"/>
        <source>Export database...</source>
        <translation>Exportovať databázu...</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="163"/>
        <source>What to export</source>
        <translation>Čo exportovať</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="287"/>
        <source>Only media name</source>
        <translation type="obsolete">Len názov média</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="494"/>
        <source>Media name</source>
        <translation>Názov média</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="504"/>
        <source>Path</source>
        <translation>Cesta</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="519"/>
        <source>Comment</source>
        <translation>Komentár</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="341"/>
        <source>HTML headline</source>
        <translation>HTML hlavička</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>table header</source>
        <translation type="obsolete">hlavička tabuľky</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="549"/>
        <source>Overwrite?</source>
        <translation>Prepísať?</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="550"/>
        <source>Do you want overwrite this file?</source>
        <translation>Chcete naozaj prepísať tento súbor?</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="555"/>
        <source>Yes</source>
        <translation>Áno</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="556"/>
        <source>Discard</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="369"/>
        <source>Export CdCat database: </source>
        <translation>Export CdCat databázy:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="489"/>
        <source>Export from cdcat, catalog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="490"/>
        <source>Generated at:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="499"/>
        <source>Media number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="339"/>
        <source>export mp3 tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="340"/>
        <source>export borrow information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="465"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="524"/>
        <source>MP3 tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="528"/>
        <source>Borrow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1043"/>
        <source>Artist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1044"/>
        <source>Title:</source>
        <translation type="unfinished">Názov:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1045"/>
        <source>Album:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1046"/>
        <source>Year:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="342"/>
        <source>table header/comment line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="491"/>
        <source>field list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="324"/>
        <source>export as XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="561"/>
        <source>Exporting, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="610"/>
        <source>Error during write export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="532"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="319"/>
        <source>Only media</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>findDialog</name>
    <message>
        <location filename="../find.cpp" line="927"/>
        <source>Name</source>
        <translation>Názov</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="928"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="930"/>
        <source>Media</source>
        <translation>Médium</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="931"/>
        <source>Path</source>
        <translation>Cesta</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="932"/>
        <source>Modification</source>
        <translation>Posledná zmena</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="525"/>
        <source>Search in the database...</source>
        <translation>Hľadať...</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="567"/>
        <source>Close / Go to selected</source>
        <translation>Zatvoriť / Choď na označené</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="539"/>
        <source>Find:</source>
        <translation>Nájsť:</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="526"/>
        <source>Seek in:</source>
        <translation>Hľadať v:</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="527"/>
        <source>Owner:</source>
        <translation>Vlastník:</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="933"/>
        <source>Comment</source>
        <translation>Komentár</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="874"/>
        <source>File name</source>
        <translation>Meno súboru</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Directory name</source>
        <translation type="obsolete">Meno adresára</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="886"/>
        <source>mp3-tag Comment</source>
        <translation>mp3-tag Komentár</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="884"/>
        <source>mp3-tag Album</source>
        <translation>mp3-tag Album</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="882"/>
        <source>mp3-tag Title</source>
        <translation>mp3-tag Názov</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="880"/>
        <source>mp3-tag Artist</source>
        <translation>mp3-tag Interpret</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="859"/>
        <source>Case sensitive</source>
        <translation>Rozlišovať malé/VEĽKÉ</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="851"/>
        <source>Use easy matching instead regex</source>
        <translation>Použiť jednoduché porovnávanie</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="652"/>
        <source>All/Everybody</source>
        <translation>Všetci</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="656"/>
        <source>All media</source>
        <translation>Všetky médiá</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="562"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Zrušiť</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="518"/>
        <source>Results</source>
        <translation type="obsolete">Výsledky</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="878"/>
        <source>Content</source>
        <translation>Obsah</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="872"/>
        <source>Media / Directory name</source>
        <translation>Médium / Názov adresára</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="905"/>
        <source>Date start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="909"/>
        <source>Date end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="155"/>
        <source>Byte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="156"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="157"/>
        <source>MiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="158"/>
        <source>GiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="913"/>
        <source>Min size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="917"/>
        <source>Max size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="557"/>
        <source>&amp;Start search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="929"/>
        <source>Size</source>
        <translation type="unfinished">Veľkosť</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="893"/>
        <source>Unsharp search (slow)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="555"/>
        <source>Find in archives too</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="568"/>
        <source>Print result...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="569"/>
        <source>Export result to HTML...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="815"/>
        <source>Print cdcat result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="820"/>
        <source>Result file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="822"/>
        <source>Filename missing</source>
        <translation type="unfinished">Chýba meno súboru</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="822"/>
        <source>Please enter a filename!</source>
        <translation type="unfinished">Prosím zadajte názov súboru!</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="839"/>
        <source>Cdcat search result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="840"/>
        <source>catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="840"/>
        <source>created at:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="843"/>
        <source>used search options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="847"/>
        <source>search pattern:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="895"/>
        <source>on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="897"/>
        <source>off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="869"/>
        <source>Search in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="889"/>
        <source>archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="903"/>
        <source>other options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="993"/>
        <source>File cant open: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="993"/>
        <source>Cant open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="497"/>
        <source>Search for duplicates in the database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="518"/>
        <source>Duplicates for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="519"/>
        <source>Close</source>
        <translation type="unfinished">Zatvoriť</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="566"/>
        <source>Results: search not started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="537"/>
        <source>Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="159"/>
        <source>TiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="540"/>
        <source>Keep search result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="556"/>
        <source>Clear search results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="934"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="558"/>
        <source>Category for find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="704"/>
        <source>Selected dir: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>findDuplicatesDialog</name>
    <message>
        <location filename="../find.cpp" line="897"/>
        <source>Name</source>
        <translation type="obsolete">Názov</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="898"/>
        <source>Type</source>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="899"/>
        <source>Size</source>
        <translation type="obsolete">Veľkosť</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="900"/>
        <source>Media</source>
        <translation type="obsolete">Médium</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="901"/>
        <source>Path</source>
        <translation type="obsolete">Cesta</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="902"/>
        <source>Modification</source>
        <translation type="obsolete">Posledná zmena</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="903"/>
        <source>Comment</source>
        <translation type="obsolete">Komentár</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="909"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Zrušiť</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="914"/>
        <source>Close</source>
        <translation type="obsolete">Zatvoriť</translation>
    </message>
</context>
<context>
    <name>helpDialog</name>
    <message>
        <location filename="../help.ui" line="13"/>
        <source>help</source>
        <translation>pomoc</translation>
    </message>
    <message>
        <location filename="../help.ui" line="65"/>
        <source>close</source>
        <translation>zatvoriť</translation>
    </message>
    <message>
        <location filename="../help.ui" line="29"/>
        <source>&lt;p align=&quot;center&quot;&gt;&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;Help&lt;/b&gt;&lt;/font&gt;&lt;/p&gt;&lt;br&gt;

&lt;b&gt;What&apos;s this?&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
The cdcat is graphical (QT based) multiplatform (Linux/Windows) catalog program which scans the directories/drives you want and memorize the filesystem including the tags of mp3&apos;s and other data and store it in a small file.
The database is stored in a gzipped XML format, so you can hack it, or use it if necessary :-)
And the program can store the content of some specified files up to a limit size if you want. (for example: *.nfo)
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Usage:&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
&lt;i&gt;Create a new catalog&lt;/i&gt;: Run the &lt;tt&gt;New&lt;/tt&gt; command in the catalog menu. You have to type the name of the new catalog. You can specify the default username of the media(which you scan later), and add a comment to the catalog.&lt;br&gt;Before the scanning select the necessary readable components in the config dialog, which can be mp3 tags content of some files or etc.  If you done this, you can scan one of your media with &lt;tt&gt;Add media&lt;/tt&gt;command in the Edit menu. In that dialog you have to specyfi the directory/or drive the media you want to add. It&apos;s recommended to specify the name and the number of the media which has to be unique. (The program always generate one identical name and number) You can label the media to a owner, if necessary.
&lt;br&gt;If you scanned your media, you will be able to browse in it with the browser window (like mc) , or search in it. You can save the catalog with &lt;tt&gt;save as&lt;/tt&gt; command in the Catalog menu.
&lt;br&gt;
&lt;br&gt;
&lt;i&gt;Open an existing catalog:&lt;/i&gt;Run the &lt;tt&gt;open&lt;/tt&gt; command in the Catalog menu, and choice the file of the catalog. (*.hcf)  After the opening you will be able browse the catalog or search in it.
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Author:&lt;/b&gt;
&lt;blockquote&gt;The program was written by  Peter Deak  (hungary)&lt;br&gt;
E-mail: hyperr@freemail.hu
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;License:&lt;/b&gt;
&lt;blockquote&gt;General Public License (GPL)&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Homepage:&lt;/b&gt;
&lt;blockquote&gt;You can read about the program and get new versions, sources etc, in the hompage of cdcat:&lt;br&gt;&lt;tt&gt;http://cdcat.sourceforge.net&lt;/tt&gt;&lt;/blockquote&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;Pomoc&lt;/b&gt;&lt;/font&gt;&lt;/p&gt;&lt;br&gt;

&lt;b&gt;O programe&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;CdCat je grafický (QT) multiplatformový (Linux/Windows)
katalógový program, ktorý prehľadáva adresáre alebo disky, ktoré určíte. Zapamätáva si adresárovú a súborovú štruktúru, včetne mp3 ID3 tagov, a všetko ukladá do malého súboru. Dáta sú uložené v XML formáte a komprimované gzipom, takže ich môžete hacknúť a použiť na čokoľvek, ak samozrejme chcete :-)
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Použitie:&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
&lt;i&gt;Vytvorenie nového katalógu:&lt;/i&gt; Vyberte položku &lt;tt&gt;Nový&lt;/tt&gt; v menu Katalóg. Musíte zadať meno nového katalógu. Môžete upresniť meno predvoleného vlastníka média a pridať komentár ku katalógu.&lt;br&gt;
Ak ste to urobili, môžete oskenovať niektoré z Vašich médií. Zvoľte &lt;tt&gt;Pridať médium&lt;/tt&gt; v menu Editovať. V nasledujúcom okne musíte zadať adresár alebo disk kde je médium, ktoré chcete skenovať. Je vhodné zadať názov a sériové číslo média, obe musia byť jednoznačné. (Program vždy vytvorí jednoznačný názov a sériové číslo). Môžete zadať aj vlastníka média, ak chcete. &lt;br&gt;Ak ste preskúmali médium, môžete ho neskôr zobraziť v okne programu a prechádzať ním alebo v ňom hľadať. Katalóg môžete uložiť zvolením &lt;tt&gt;Uložiť ako&lt;/tt&gt; v menu Katalóg.
&lt;br&gt;
&lt;br&gt;
&lt;i&gt;Otvorenie existujúceho katalógu:&lt;/i&gt; Vyberte položku &lt;tt&gt;Otvoriť&lt;/tt&gt; v menu Katalóg a vyberte súbor katalógu (*.hcf).  Po otvorení môžete katalóg prezerať alebo v ňom hľadať.
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Autor:&lt;/b&gt;
&lt;blockquote&gt;Program napísal Peter Deak  (Maďarsko)&lt;br&gt;
E-mail: hyperr@freemail.hu
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Licencia:&lt;/b&gt;
&lt;blockquote&gt;General Public License (GPL)&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Homepage:&lt;/b&gt;
&lt;blockquote&gt;Informácie o programe, nové verzie, zdrojové kódy atď.
nájdete na domovskej stránke programu CdCat:&lt;br&gt;&lt;tt&gt;http://cdcat.sourceforge.net&lt;/tt&gt;&lt;/blockquote&gt;</translation>
    </message>
    <message>
        <location filename="../help.ui" line="22"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:40px; margin-right:40px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>importCsv</name>
    <message>
        <location filename="" line="0"/>
        <source>Import was successful.</source>
        <translation type="obsolete">Import prebehol úspešne.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>1 media</source>
        <translation type="obsolete">1 médium</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>media</source>
        <translation type="obsolete">médium</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>1 directory:,
</source>
        <translation type="obsolete">1 adresár</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>directories</source>
        <translation type="obsolete">adresáre</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>1 File</source>
        <translation type="obsolete">1 Súbor</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>files</source>
        <translation type="obsolete">súbory</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>are imported.</source>
        <translation type="obsolete">sú importované.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Import successful</source>
        <translation type="obsolete">Import úspešný</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>file read error</source>
        <translation type="obsolete">chyba pri čítaní súboru</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Fatal error</source>
        <translation type="obsolete">Fatálna chyba</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Fatal error occured.</source>
        <translation type="obsolete">Vyskytla sa fatálna chyba.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>file open error</source>
        <translation type="obsolete">chyba pri čítaní súboru</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>importuser</source>
        <translation type="obsolete">vlastník importu</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not read file</source>
        <translation type="obsolete">Nemožno čítať súbor</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file</source>
        <translation type="obsolete">Nemožno otvoriť súbor</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Importing CSV...</source>
        <translation type="obsolete">Importovanie CSV...</translation>
    </message>
</context>
<context>
    <name>importGtktalogCsv</name>
    <message>
        <location filename="../import.cpp" line="187"/>
        <source>Importing CSV...</source>
        <translation>Importovanie CSV...</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1344"/>
        <source>Import was successful.</source>
        <translation>Import prebehol úspešne.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1347"/>
        <source>1 media</source>
        <translation>1 médium</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1349"/>
        <source>media</source>
        <translation>médium</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1352"/>
        <source>1 directory:,
</source>
        <translation>1 adresár</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1354"/>
        <source>directories</source>
        <translation>adresáre</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1357"/>
        <source>1 File</source>
        <translation>1 Súbor</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1359"/>
        <source>files</source>
        <translation>súbory</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1361"/>
        <source>are imported.</source>
        <translation>sú importované.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1363"/>
        <source>Import successful</source>
        <translation>Import úspešný</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1367"/>
        <source>file read error</source>
        <translation>chyba pri čítaní súboru</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1367"/>
        <source>Could not read file</source>
        <translation>Nemožno čítať súbor</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1370"/>
        <source>Fatal error</source>
        <translation>Fatálna chyba</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1370"/>
        <source>Fatal error occured.</source>
        <translation>Vyskytla sa fatálna chyba.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1373"/>
        <source>file open error</source>
        <translation>chyba pri čítaní súboru</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1373"/>
        <source>Could not open file</source>
        <translation>Nemožno otvoriť súbor</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1411"/>
        <source>importuser</source>
        <translation>vlastník importu</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="187"/>
        <source>Cancel</source>
        <translation type="unfinished">Zrušiť</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1274"/>
        <source>tag: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>importGtktalogXml</name>
    <message>
        <location filename="../import.cpp" line="1814"/>
        <source>Importing XML...</source>
        <translation>Importovanie XML...</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1851"/>
        <source>XML import was successful.</source>
        <translation>XML import prebehol úspešne.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1854"/>
        <source>1 media</source>
        <translation>1 médium</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1856"/>
        <source>media</source>
        <translation>médium</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1859"/>
        <source>1 directory:,
</source>
        <translation>1 adresár</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1861"/>
        <source>directories</source>
        <translation>adresáre</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1864"/>
        <source>1 File</source>
        <translation>1 Súbor</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1866"/>
        <source>files</source>
        <translation>súbory</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1868"/>
        <source>are imported.</source>
        <translation>sú importované.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1870"/>
        <source>Import successful</source>
        <translation>Import úspešný</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1875"/>
        <source>parse error</source>
        <translation>chyba v syntaxy</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1875"/>
        <source>error during parsing</source>
        <translation>chyba pri čítaní syntaxe</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1989"/>
        <source>importuser</source>
        <translation>vlastník importu</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1814"/>
        <source>Cancel</source>
        <translation type="unfinished">Zrušiť</translation>
    </message>
</context>
<context>
    <name>importWhereIsItXml</name>
    <message>
        <location filename="../import.cpp" line="2658"/>
        <source>Importing XML...</source>
        <translation>Importovanie XML...</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2694"/>
        <source>Importing XML from WhereIsIt was successful.</source>
        <translation>Importovanie XML z WhereIsIt bolo úspešné.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2697"/>
        <source>1 media</source>
        <translation>1 médium</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2699"/>
        <source>media</source>
        <translation>médium</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2702"/>
        <source>1 directory:,
</source>
        <translation>1 adresár</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2704"/>
        <source>directories</source>
        <translation>adresáre</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2707"/>
        <source>1 File</source>
        <translation>1 Súbor</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2709"/>
        <source>files</source>
        <translation>súbory</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2711"/>
        <source>are imported.</source>
        <translation>sú importované.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2713"/>
        <source>Import successful</source>
        <translation>Import úspešný</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2718"/>
        <source>parse error</source>
        <translation>chyba v syntaxy</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2718"/>
        <source>error during parsing</source>
        <translation>chyba pri čítaní syntaxe</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2658"/>
        <source>Cancel</source>
        <translation type="unfinished">Zrušiť</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2390"/>
        <source>tag:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>importXml</name>
    <message>
        <location filename="" line="0"/>
        <source>Importing XML...</source>
        <translation type="obsolete">Importovanie XML...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>XML import was successful.</source>
        <translation type="obsolete">XML import prebehol úspešne.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>1 media</source>
        <translation type="obsolete">1 médium</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>media</source>
        <translation type="obsolete">médium</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>1 directory:,
</source>
        <translation type="obsolete">1 adresár</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>directories</source>
        <translation type="obsolete">adresáre</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>1 File</source>
        <translation type="obsolete">1 Súbor</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>files</source>
        <translation type="obsolete">súbory</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>are imported.</source>
        <translation type="obsolete">sú importované.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Import successful</source>
        <translation type="obsolete">Import úspešný</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>parse error</source>
        <translation type="obsolete">chyba v syntaxy</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>error during parsing</source>
        <translation type="obsolete">chyba pri čítaní syntaxe</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>importuser</source>
        <translation type="obsolete">vlastník importu</translation>
    </message>
</context>
<context>
    <name>newdbdialog</name>
    <message>
        <location filename="../newdbdialog.cpp" line="133"/>
        <source>DataBase&apos;s Properties</source>
        <translation>Vlastnosti databázy</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="134"/>
        <source>DataBase Name:</source>
        <translation>Meno databázy:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="135"/>
        <source>DataBase Owner:</source>
        <translation>Vlastník databázy:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="136"/>
        <source>Comment:</source>
        <translation>Komentár:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="138"/>
        <source>Catalog</source>
        <translation>Katalóg</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="139"/>
        <source>hcat-user</source>
        <translation>hcat-user</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="140"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="141"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="142"/>
        <source> </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="137"/>
        <source>Category:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="144"/>
        <source>encrypt catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="145"/>
        <source>password for catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="159"/>
        <source>Password length is too short, must be minimum 4 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="159"/>
        <source>Password too short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="163"/>
        <source>Password length is too big, must be maximal %1 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="163"/>
        <source>Password too big</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>renamedialog</name>
    <message>
        <location filename="../newdbdialog.cpp" line="227"/>
        <source>Rename node...</source>
        <translation>Premenovať položku...</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="228"/>
        <source>Give the new name:</source>
        <translation>Zadajte nové meno:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="229"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="230"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="254"/>
        <source>Error:</source>
        <translation>Chyba:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="249"/>
        <source>The new (media) name must be unique!</source>
        <translation>Nový názov musí byť jednoznačný!</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="254"/>
        <source>The new media name can&apos;t starts with &quot;@&quot; !</source>
        <translation>Názov média nemôže začínať znakom &quot;@&quot; !</translation>
    </message>
</context>
<context>
    <name>renumberdialog</name>
    <message>
        <location filename="../newdbdialog.cpp" line="372"/>
        <source>Error:</source>
        <translation>Chyba:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="301"/>
        <source>The element is NOT a Media!</source>
        <translation>Táto položka nie je médium!</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="338"/>
        <source>Re-Number media...</source>
        <translation>Zmeň číslo média...</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="339"/>
        <source>Give the new serial number:</source>
        <translation>Zadajte nové sériové číslo média:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="340"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="341"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>I Cannot understand the text as number!</source>
        <translation type="obsolete">Sériové číslo musí byť číselná hodnota!</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="372"/>
        <source>The new media-number must be unique!</source>
        <translation>Nové číslo média musí byť jednoznačné!</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="362"/>
        <source>Please enter number value!</source>
        <translation>Prosím zadajte číselnú hodnotu!</translation>
    </message>
</context>
<context>
    <name>seekEngine</name>
    <message>
        <location filename="../find.cpp" line="1077"/>
        <source>Error in the pattern:</source>
        <translation>Chyba vo vzorke:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1163"/>
        <source>There is no matching.</source>
        <translation>Nič nenájdené.</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1652"/>
        <source>dir</source>
        <translation>adr</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1658"/>
        <source>file</source>
        <translation>súbor</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1669"/>
        <source>error</source>
        <translation>chyba</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1646"/>
        <source>media</source>
        <translation>médium</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1041"/>
        <source>Searching, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1158"/>
        <source>Search cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1158"/>
        <source>You have cancelled searching.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1505"/>
        <source>File in archive: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1154"/>
        <source>Results:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1154"/>
        <source>searching is in progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1160"/>
        <source>Last search results:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
