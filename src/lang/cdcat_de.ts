<!DOCTYPE TS>
<TS>
  <context>
    <name></name>
    <message>
      <source>Not available</source>
      <translation>Nicht verfügbar</translation>
    </message>
  </context>
  <context>
    <name>AddLnk</name>
    <message>
      <source>Add a &quot;Catalog Link&quot; to the database</source>
      <translation>Eine &quot;Katalogverknüpfung&quot; zur Datenbank hinzufügen</translation>
    </message>
    <message>
      <source>Location of the .hcf file:</source>
      <translation>Ort der .hcf-Datei:</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>Ok</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>Open a file...</source>
      <translation>Datei öffnen...</translation>
    </message>
    <message>
      <source>CdCat databases (*.hcf )</source>
      <translation>CdCat-Datenbanken (*.hcf)</translation>
    </message>
  </context>
  <context>
    <name>ArchiveFile</name>
    <message>
      <source>unknown</source>
      <translation>unbekannt</translation>
    </message>
  </context>
  <context>
    <name>CatalogTypeEditDialog</name>
    <message>
      <source>Change media type</source>
      <translation>Medientyp ändern</translation>
    </message>
    <message>
      <source>Change type of media</source>
      <translation>Typ des Mediums ändern</translation>
    </message>
    <message>
      <source>CD</source>
      <translation>CD</translation>
    </message>
    <message>
      <source>DVD</source>
      <translation>DVD</translation>
    </message>
    <message>
      <source>HardDisc</source>
      <translation>Festplatte</translation>
    </message>
    <message>
      <source>Floppy</source>
      <translation>Diskette</translation>
    </message>
    <message>
      <source>NetworkPlace</source>
      <translation>Netzwerkgerät</translation>
    </message>
    <message>
      <source>FlashDrive</source>
      <translation>Flashspeicher</translation>
    </message>
    <message>
      <source>OtherDevice</source>
      <translation>Anderes Gerät</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>OK</source>
      <translation>OK</translation>
    </message>
  </context>
  <context>
    <name>CdCatConfig</name>
    <message>
      <source>Error during autoload...</source>
      <translation>Fehler beim automatischen Laden...</translation>
    </message>
    <message>
      <source>I can't open the autoload catalog.
Check the file, or change the autoload option in the config dialog!
</source>
      <translation>Katalog kann nicht automatisch geladen werden.
Datei überprüfen oder Option 'automatisches Laden' im Konfigurationsdialog ändern

</translation>
    </message>
    <message>
      <source>I can't open the autoload catalog according the first command line parameter.
Check the file!
</source>
      <translation>Das automatische Laden des mit dem ersten Kommandozeilenparameter angebenen Katalogs ist nicht möglich.
Datei überprüfen!
</translation>
    </message>
    <message>
      <source>I can't create or rewrite the ./cdcat.cfg file </source>
      <translation>Datei ./cdcat.cfg kann nicht erstellt/überschrieben werden</translation>
    </message>
    <message>
      <source>I can't create or rewrite the $(HOME)/.cdcatconfig file </source>
      <translation>Datei $(HOME)/.cdcatconfig kann nicht erstellt/überschrieben werden</translation>
    </message>
    <message>
      <source>Error while saving config file...</source>
      <translation>Fehler beim Speichern der Konfigurationsdatei...</translation>
    </message>
    <message>
      <source>I can't get the $HOME environment variable.
It is necessary because I'd like to save the config file into your home directory.</source>
      <translation>Die $HOME-Umgebungsvariable kann nicht gelesen werden.
Diese ist notwendig, da cdcat die Konfigurationsdatei in das persönliche Verzeichnis schreiben muss.</translation>
    </message>
  </context>
  <context>
    <name>CdCatMainWidget</name>
    <message>
      <source>Hyper's CD Catalogizer</source>
      <translation>Hypers CD-Katalogisierer</translation>
    </message>
    <message>
      <source>Help</source>
      <translation>Hilfe</translation>
    </message>
    <message>
      <source>Directory Tree</source>
      <translation>Verzeichnisbaum</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>Grösse</translation>
    </message>
    <message>
      <source>Add media...</source>
      <translation>Medium hinzufügen...</translation>
    </message>
    <message>
      <source>Rescan media...</source>
      <translation>Medium aktualisieren...</translation>
    </message>
    <message>
      <source>Insert Catalog...</source>
      <translation>Katalog einfügen...</translation>
    </message>
    <message>
      <source>Rename node...</source>
      <translation>Knoten umbenennen...</translation>
    </message>
    <message>
      <source>Delete node</source>
      <translation>Knoten löschen</translation>
    </message>
    <message>
      <source>Seek in database...</source>
      <translation>In Datenbank suchen...</translation>
    </message>
    <message>
      <source>Node size</source>
      <translation>Knotengrösse</translation>
    </message>
    <message>
      <source>Configuration...</source>
      <translation>Konfiguration...</translation>
    </message>
    <message>
      <source>Edit</source>
      <translation>Bearbeiten</translation>
    </message>
    <message>
      <source>Others</source>
      <translation>Andere</translation>
    </message>
    <message>
      <source>Sort media by name</source>
      <translation>Medium nach Name sortieren</translation>
    </message>
    <message>
      <source>Recent files...</source>
      <translation>Zuletzt benutze Dateien...</translation>
    </message>
    <message>
      <source>Import/Export</source>
      <translation>Import/Export</translation>
    </message>
    <message>
      <source>Re-Number media node...</source>
      <translation>Medienknoten umnummerieren...</translation>
    </message>
    <message>
      <source>Borrowing info...</source>
      <translation>Ausleihinformation...</translation>
    </message>
    <message>
      <source>Set Colors...</source>
      <translation>Farben setzen...</translation>
    </message>
    <message>
      <source>Seek in the panel</source>
      <translation>In Bedienfeld suchen</translation>
    </message>
    <message>
      <source>About Qt</source>
      <translation>Über Qt</translation>
    </message>
    <message>
      <source>Add a link to a CdCat Catalog...</source>
      <translation>Verweis auf CdCat-Datenbank hinzufügen...</translation>
    </message>
    <message>
      <source>Scanning:</source>
      <translation>Durchsuche:</translation>
    </message>
    <message>
      <source>Import database (CSV/XML)</source>
      <translation>Datenbank importieren (CSV/XML)</translation>
    </message>
    <message>
      <source>Export database (CSV/HTML/XML)</source>
      <translation>Datenbank exportieren (CSV/HTML/XML)</translation>
    </message>
    <message>
      <source>Search</source>
      <translation>Suchen</translation>
    </message>
    <message>
      <source>&amp;New...</source>
      <translation>&amp;Neu...</translation>
    </message>
    <message>
      <source>Create a new catalog</source>
      <translation>Neuen Katalog erstellen</translation>
    </message>
    <message>
      <source>&amp;Open...</source>
      <translation>&amp;Öffnen...</translation>
    </message>
    <message>
      <source>Open a existing catalog</source>
      <translation>Einen existierenden Katalog öffnen</translation>
    </message>
    <message>
      <source>&amp;Save</source>
      <translation>&amp;Speichern</translation>
    </message>
    <message>
      <source>Save catalog</source>
      <translation>Katalog speichern</translation>
    </message>
    <message>
      <source>&amp;Save as...</source>
      <translation>&amp;Speichern als...</translation>
    </message>
    <message>
      <source>save catalog with new name</source>
      <translation>Katalog unter neuem Namen speichern</translation>
    </message>
    <message>
      <source>Close catalog</source>
      <translation>Katalog schließen</translation>
    </message>
    <message>
      <source>&amp;Quit</source>
      <translation>&amp;Beenden</translation>
    </message>
    <message>
      <source>Close program</source>
      <translation>Programm schließen</translation>
    </message>
    <message>
      <source>Add new media to catalog</source>
      <translation>Neues Medium zum Katalog hinzufügen</translation>
    </message>
    <message>
      <source>Add a link to a existing cdcat catalog</source>
      <translation>Einen Verweis zu einem existierendem Katalog hinzufügen</translation>
    </message>
    <message>
      <source>Rescan existing media</source>
      <translation>Existierendes Medium neu einlesen</translation>
    </message>
    <message>
      <source>Insert catalog into database</source>
      <translation>Katalog in Datenbank einfügen</translation>
    </message>
    <message>
      <source>Rename node</source>
      <translation>Knoten umbenennen</translation>
    </message>
    <message>
      <source>Renumber node</source>
      <translation>Knoten umnummerieren</translation>
    </message>
    <message>
      <source>Seek in database for files and folders</source>
      <translation>In Datenbank nach Dateien und Ordnern suchen</translation>
    </message>
    <message>
      <source>Calculate node size</source>
      <translation>Knotengröße berechnen</translation>
    </message>
    <message>
      <source>Edit cdcat configuration</source>
      <translation>CdCat-Konfiguration bearbeiten</translation>
    </message>
    <message>
      <source>Set the colors for display</source>
      <translation>Farben für Anzeige setzen</translation>
    </message>
    <message>
      <source>Import database (CSV/XML) from various catalog programs</source>
      <translation>Datenbank von verschiedenen Katalogprogrammen importieren (CSV/XML)</translation>
    </message>
    <message>
      <source>About Cdcat</source>
      <translation>Über Cdcat</translation>
    </message>
    <message>
      <source>About the Qt toolkit</source>
      <translation>Über das Qt-Toolkit</translation>
    </message>
    <message>
      <source>Mi&amp;nimize</source>
      <translation>Mi&amp;nimieren</translation>
    </message>
    <message>
      <source>Ma&amp;ximize</source>
      <translation>Ma&amp;ximieren</translation>
    </message>
    <message>
      <source>&amp;Restore</source>
      <translation>&amp;Wiederherstellen</translation>
    </message>
    <message>
      <source>Cdcat - idle</source>
      <translation>Cdcat - Leerlauf</translation>
    </message>
    <message>
      <source>Main toolbar</source>
      <translation>Hauptwerkzeugleiste</translation>
    </message>
    <message>
      <source>Comment dock</source>
      <translation>Kommentardock</translation>
    </message>
    <message>
      <source>view tool bar</source>
      <translation>Werkzeugleiste anzeigen</translation>
    </message>
    <message>
      <source>View tool bar in main window</source>
      <translation>Werkzeugleiste in Hauptfenster anzeigen</translation>
    </message>
    <message>
      <source>view status bar</source>
      <translation>Statusleiste anzeigen</translation>
    </message>
    <message>
      <source>View status bar in main window</source>
      <translation>Statusleiste in Hauptfenster anzeigen</translation>
    </message>
    <message>
      <source>view comment dock</source>
      <translation>Kommentardock anzeigen</translation>
    </message>
    <message>
      <source>show comment dock</source>
      <translation>Kommentardock anzeigen</translation>
    </message>
    <message>
      <source>View</source>
      <translation>Anzeigen</translation>
    </message>
    <message>
      <source>show systray icon</source>
      <translation>Dock-Symbol anzeigen</translation>
    </message>
    <message>
      <source>File</source>
      <translation>Datei</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
    <message>
      <source>processing file</source>
      <translation>Datei wird verarbeitet</translation>
    </message>
    <message>
      <source>Sort media by number (ascending)</source>
      <translation>Medium nach Nummer sortieren (aufsteigend)</translation>
    </message>
    <message>
      <source>Sort media by number (descending)</source>
      <translation>Medium nach Nummer sortieren (absteigend)</translation>
    </message>
    <message>
      <source>Sort media by time (ascending)</source>
      <translation>Medium nach Zeit sortieren (aufsteigend)</translation>
    </message>
    <message>
      <source>Sort media by time (descending)</source>
      <translation>Medium nach Zeit sortieren (absteigend)</translation>
    </message>
    <message>
      <source>Sort media by name (ascending)</source>
      <translation>Medium nach Name sortieren (absteigend)</translation>
    </message>
    <message>
      <source>Sort media by name (descending)</source>
      <translation>Medium nach Name sortieren (absteigend)</translation>
    </message>
    <message>
      <source>Sort media by type (ascending)</source>
      <translation>Medium nach Typ sortieren (aufsteigend)</translation>
    </message>
    <message>
      <source>Sort media by type (descending)</source>
      <translation>Medium nach Typ sortieren (absteigend)</translation>
    </message>
    <message>
      <source>Insert Cdcat XML export...</source>
      <translation>Cdcat-XML-Export einfügen...</translation>
    </message>
    <message>
      <source>Insert Cdcat XML export</source>
      <translation>Cdcat-XML-Export einfügen</translation>
    </message>
    <message>
      <source>Insert Cdcat export XML...</source>
      <translation>Cdcat-XML-Export einfügen...</translation>
    </message>
    <message>
      <source>Insert Cdcat exported XML into database</source>
      <translation>Cdcat-XML-Export in Datenbank einfügen</translation>
    </message>
    <message>
      <source>Change password...</source>
      <translation>Passwort ändern...</translation>
    </message>
    <message>
      <source>Changes password for catalog encryption</source>
      <translation>Ändert das Passwort für Katalogverschlüsselung</translation>
    </message>
    <message>
      <source>Disable encryption</source>
      <translation>Verschlüsselung deaktivieren</translation>
    </message>
    <message>
      <source>Disables catalog encryption</source>
      <translation>Deaktiviert Katalogverschlüsselung</translation>
    </message>
    <message>
      <source>Enable encryption</source>
      <translation>Verschlüsselung aktivieren</translation>
    </message>
    <message>
      <source>Enables catalog encryption</source>
      <translation>Aktiviert Katalogverschlüsselung</translation>
    </message>
    <message>
      <source>Borrowing info</source>
      <translation>Ausleihinformationen</translation>
    </message>
    <message>
      <source>Key bindings</source>
      <translation>Kurzbefehle</translation>
    </message>
  </context>
  <context>
    <name>ColorSchemePreview</name>
    <message>
      <source>Title:</source>
      <translation>Titel:</translation>
    </message>
    <message>
      <source>Road To Hell</source>
      <translation>Road To Hell</translation>
    </message>
  </context>
  <context>
    <name>ColorSettings</name>
    <message>
      <source>ColorSettings</source>
      <translation>Farbeinstellungen</translation>
    </message>
    <message>
      <source>Comment area background and the file higlighting line</source>
      <translation>Kommentarhintergrundbereich und Dateihintergrund</translation>
    </message>
    <message>
      <source>Frame of comment area</source>
      <translation>Rahmen des Kommentarbereichs</translation>
    </message>
    <message>
      <source>Comment window static text (Program text)</source>
      <translation>Statischer Text des Kommentarfensters (Programmtext)</translation>
    </message>
    <message>
      <source>Comment window variable text (Data)</source>
      <translation>Variabler Text des Kommentarfensters (Daten)</translation>
    </message>
    <message>
      <source>color</source>
      <translation>Farbe</translation>
    </message>
    <message>
      <source>Red:</source>
      <translation>Rot:</translation>
    </message>
    <message>
      <source>Green:</source>
      <translation>Grün:</translation>
    </message>
    <message>
      <source>Blue:</source>
      <translation>Blau:</translation>
    </message>
    <message>
      <source>Ok</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
  </context>
  <context>
    <name>CommentWidget</name>
    <message>
      <source>Edit and refresh the actual comment page.</source>
      <translation>Aktuelle Kommentarseite bearbeiten und aktualisieren</translation>
    </message>
    <message>
      <source>Upper container! (..)</source>
      <translation>Oberer Container! (..)</translation>
    </message>
    <message>
      <source> CdCat Catalog root</source>
      <translation> CdCat-Katalogroot</translation>
    </message>
    <message>
      <source> Directory</source>
      <translation> Verzeichnis</translation>
    </message>
    <message>
      <source> File</source>
      <translation> Datei</translation>
    </message>
    <message>
      <source>Media</source>
      <translation>Medium</translation>
    </message>
    <message>
      <source>Size:</source>
      <translation>Grösse:</translation>
    </message>
    <message>
      <source>Owner:</source>
      <translation>Eigentümer:</translation>
    </message>
    <message>
      <source>Unknown</source>
      <translation>Unbekannt</translation>
    </message>
    <message>
      <source>Mp3-Tag:</source>
      <translation>MP3-Tag:</translation>
    </message>
    <message>
      <source>Comment:</source>
      <translation>Kommentar:</translation>
    </message>
    <message>
      <source>Shows the content of the file.</source>
      <translation>Zeigt den Inhalt der Datei an.</translation>
    </message>
    <message>
      <source>Name:</source>
      <translation>Name:</translation>
    </message>
    <message>
      <source>Type:</source>
      <translation>Typ:</translation>
    </message>
    <message>
      <source>There is no selected element.</source>
      <translation>Es ist kein Element ausgewählt.</translation>
    </message>
    <message>
      <source> Link to a CdCat catalog</source>
      <translation> Mit CdCat-Katalog verknüpfen</translation>
    </message>
    <message>
      <source>Location:</source>
      <translation>Ort:</translation>
    </message>
    <message>
      <source>Last modification:</source>
      <translation>Letzte Änderungen:</translation>
    </message>
    <message>
      <source>At media (number/name):</source>
      <translation>Auf Medium (Nummer/Name):</translation>
    </message>
    <message>
      <source>Edit and refresh the actual category page.</source>
      <translation>Aktuelle Kategorie bearbeiten und aktualisieren</translation>
    </message>
    <message>
      <source>Category:</source>
      <translation>Kategorie:</translation>
    </message>
    <message>
      <source>Archive contents:</source>
      <translation>Archivinhalt:</translation>
    </message>
    <message>
      <source>Artist:</source>
      <translation>Künstler:</translation>
    </message>
    <message>
      <source>Title:</source>
      <translation>Titel:</translation>
    </message>
    <message>
      <source>track:</source>
      <translation>Spur:</translation>
    </message>
    <message>
      <source>Album:</source>
      <translation>Album:</translation>
    </message>
    <message>
      <source>Year:</source>
      <translation>Jahr:</translation>
    </message>
    <message>
      <source>File info:</source>
      <translation>Dateinformation:</translation>
    </message>
    <message>
      <source>Rights</source>
      <translation>Rechte</translation>
    </message>
    <message>
      <source>Owner</source>
      <translation>Eigentümer</translation>
    </message>
    <message>
      <source>Group</source>
      <translation>Gruppe</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>Grösse</translation>
    </message>
    <message>
      <source>Changed</source>
      <translation>Änderung</translation>
    </message>
    <message>
      <source>Comment</source>
      <translation>Kommentar</translation>
    </message>
    <message>
      <source>Path</source>
      <translation>Pfad</translation>
    </message>
    <message>
      <source>Exif data:</source>
      <translation>Exif-Daten:</translation>
    </message>
    <message>
      <source>Thumbnail:</source>
      <translation>Vorschaubild:</translation>
    </message>
    <message>
      <source>Stored size:</source>
      <translation>Gespeicherte Größe:</translation>
    </message>
    <message>
      <source>There is no selected element:</source>
      <translation>Es gibt kein ausgewähltes Element:</translation>
    </message>
    <message>
      <source>Number</source>
      <translation>Nummer</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
    <message>
      <source>Time</source>
      <translation>Zeit</translation>
    </message>
    <message>
      <source>sorted by:</source>
      <translation>sortiert nach:</translation>
    </message>
  </context>
  <context>
    <name>ConfigDialog</name>
    <message>
      <source>Configure  CdCat...</source>
      <translation>CdCat konfigurieren...</translation>
    </message>
    <message>
      <source>Autoload DataBase on startup</source>
      <translation>Datenbank beim Start automatisch laden</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>Ok</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>Application font size.(must restart cdcat!) </source>
      <translation>Schriftgrösse der Anwendung (erfordert Neustart von CdCat!) </translation>
    </message>
    <message>
      <source>Save the XML-db.file nicer format(needs more space)</source>
      <translation>XML-DB-Datei in schönerem Format speichern (benötigt mehr Platz)</translation>
    </message>
    <message>
      <source>Scanning: mount cdrom at start / eject when finish</source>
      <translation>CDROM beim Start mounten / auswerfen, wenn beendet</translation>
    </message>
    <message>
      <source>Use own font size</source>
      <translation>Eigene Schriftgrösse benutzen</translation>
    </message>
    <message>
      <source>Number of history entries</source>
      <translation>Anzahl der Verlaufseinträge</translation>
    </message>
    <message>
      <source>Choose path to cdrom!</source>
      <translation>Pfad zum CDROM auswählen!</translation>
    </message>
    <message>
      <source>The language of CdCat interface</source>
      <translation>Die Sprache der CDdCat-Oberfäche</translation>
    </message>
    <message>
      <source>Choose a file to load automatically!</source>
      <translation>Datei zum automatischen Laden auswählen!</translation>
    </message>
    <message>
      <source>Select additional items to read</source>
      <translation>Weitere Dateieigenschaften erkennen (Auswahl)</translation>
    </message>
    <message>
      <source>Automatically save the database after every scan (for safety sake)</source>
      <translation>Datenbank automatisch nach jedem Einlesen speichern (um sicher zu gehen)</translation>
    </message>
    <message>
      <source>Display debug info on console</source>
      <translation>Debuginfo auf Konsole ausgeben</translation>
    </message>
    <message>
      <source>CdCat databases (*.hcf )</source>
      <translation>CdCat-Datenbanken (*.hcf)</translation>
    </message>
    <message>
      <source>Save catalogs always as UTF8</source>
      <translation>Kataloge immer als UTF8 speichern</translation>
    </message>
    <message>
      <source>Show progressed file at scanning in status label</source>
      <translation>Zu verarbeitende Datei beim Durchsuchen in Statusleiste anzeigen</translation>
    </message>
    <message>
      <source>font size</source>
      <translation>Schriftgröße</translation>
    </message>
    <message>
      <source>show systray icon</source>
      <translation>Dock-Symbol anzeigen</translation>
    </message>
    <message>
      <source>Show systray icon</source>
      <translation>Dock-Symbol anzeigen</translation>
    </message>
    <message>
      <source>display current scanned file in tray</source>
      <translation>aktuell eingelesene Datei in Dock anzeigen</translation>
    </message>
    <message>
      <source>display current scanned file in tray (mediainfo / archive scan)</source>
      <translation>aktuell eingelesene Datei in Dock anzeigen (Medieninfo- / Archive durchsuchen)</translation>
    </message>
    <message>
      <source>Path to cdrom device / mount dir</source>
      <translation>Pfad zum CDROM-Gerät / Mountverzeichnis</translation>
    </message>
    <message>
      <source>This is the path to the cdrom/dvd device or the path where its mounted</source>
      <translation>Dies ist der Pfad zum CDROM-/DVD-Gerät oder der Pafd wo es eingebunden ist</translation>
    </message>
  </context>
  <context>
    <name>DataBase</name>
    <message>
      <source>Error</source>
      <translation>Fehler</translation>
    </message>
    <message>
      <source>Cancel scanning</source>
      <translation>Durchsuchen abbrechen</translation>
    </message>
    <message>
      <source>I can't rewrite the file: %1</source>
      <translation>Datei kann nicht überschrieben werden: %1</translation>
    </message>
    <message>
      <source>I can't create the file: %1</source>
      <translation>Datei kann nicht erstellt werden: %1</translation>
    </message>
    <message>
      <source>I can't open the file: %1</source>
      <translation>Datei kann nicht geöffnet werden: %1</translation>
    </message>
    <message>
      <source>Cannot read directory: %1</source>
      <translation>Verzeichnis kann nicht gelesen werden: %1</translation>
    </message>
    <message>
      <source>Error while parsing file: %1</source>
      <translation>Fehler beim Parsen der Datei: %1</translation>
    </message>
    <message>
      <source>No database opened!</source>
      <translation>Keine Datenbank geöffnet!</translation>
    </message>
    <message>
      <source>Cannot read file: %1</source>
      <translation>Datei kann nicht gelesen werden: %1</translation>
    </message>
    <message>
      <source>Ignore</source>
      <translation>Ignorieren</translation>
    </message>
    <message>
      <source>Symbolic link to file:#</source>
      <translation>Symbolischer Verweis auf Datei:#</translation>
    </message>
    <message>
      <source>Symbolic link to directory:#</source>
      <translation>Symbolischer Verweis auf Verzeichnis:#</translation>
    </message>
    <message>
      <source>DEAD Symbolic link to:#</source>
      <translation>TOTER symbolischer Verweis auf:#</translation>
    </message>
    <message>
      <source>System file (e.g. FIFO, socket or device file)</source>
      <translation>Systemdatei (z.B. FIFO, Socket oder Gerätedatei)</translation>
    </message>
    <message>
      <source>Not enough memory to open the file: %1</source>
      <translation>Nicht genug Hauptspeicher um Datei %1 zu öffnen</translation>
    </message>
    <message>
      <source>Reading file, please wait...</source>
      <translation>Datei wird gelesen, bitte warten...</translation>
    </message>
    <message>
      <source>Parsing file, please wait...</source>
      <translation>Datei wird analysiert, bitte warten...</translation>
    </message>
    <message>
      <source>Converting to unicode, please wait...</source>
      <translation>Umwandlung zu Unicode, bitte warten...</translation>
    </message>
    <message>
      <source>You have cancelled catalog reading.</source>
      <translation>Sie haben das Lesen das Katalogs abgebrochen.</translation>
    </message>
    <message>
      <source>device </source>
      <translation>Gerät</translation>
    </message>
    <message>
      <source> link to </source>
      <translation> Verweis nach</translation>
    </message>
    <message>
      <source>reading mp3 info</source>
      <translation>MP3-Info wird gelesen</translation>
    </message>
    <message>
      <source>reading media info</source>
      <translation>Medien-Info wird gelesen</translation>
    </message>
    <message>
      <source>reading avi info</source>
      <translation>AVI-Info wird gelesen</translation>
    </message>
    <message>
      <source>reading file content</source>
      <translation>Dateiinhalt wird gelesen</translation>
    </message>
    <message>
      <source>reading exif data</source>
      <translation>Exif-Daten werden gelesen</translation>
    </message>
    <message>
      <source>reading thumbnail data</source>
      <translation>Vorschaubild wird gelesen</translation>
    </message>
    <message>
      <source>scanning archive</source>
      <translation>Archiv wird durchsucht</translation>
    </message>
    <message>
      <source>scanning archive, file:</source>
      <translation>Archiv wird durchsucht, Datei:</translation>
    </message>
    <message>
      <source>unknown</source>
      <translation>unbekannt</translation>
    </message>
  </context>
  <context>
    <name>DirectoryView</name>
    <message>
      <source>Name</source>
      <translation>Name</translation>
    </message>
  </context>
  <context>
    <name>GuiSlave</name>
    <message>
      <source>Directory</source>
      <translation>Verzeichnis</translation>
    </message>
    <message>
      <source>Catalog</source>
      <translation>Katalog</translation>
    </message>
    <message>
      <source>Unknown(DB)</source>
      <translation>Unbekannt(DB)</translation>
    </message>
    <message>
      <source>CD</source>
      <translation>CD</translation>
    </message>
    <message>
      <source>DVD</source>
      <translation>DVD</translation>
    </message>
    <message>
      <source>HardDisc</source>
      <translation>Festplatte</translation>
    </message>
    <message>
      <source>Floppy</source>
      <translation>Diskette</translation>
    </message>
    <message>
      <source>NetworkDrv</source>
      <translation>Netzwerkgerät</translation>
    </message>
    <message>
      <source>FlashDrv</source>
      <translation>Flashspeicher</translation>
    </message>
    <message>
      <source>OtherDevice</source>
      <translation>Anderes Gerät</translation>
    </message>
    <message>
      <source>File</source>
      <translation>Datei</translation>
    </message>
    <message>
      <source>Hyper's CD Catalogizer (modified)</source>
      <translation>Hyper´s CD-Katalogisierer (geändert)</translation>
    </message>
    <message>
      <source>Hyper's CD Catalogizer</source>
      <translation>Hyper´s CD-Katalogisierer</translation>
    </message>
    <message>
      <source>CdCat databases (*.hcf )</source>
      <translation>CdCat-Datenbanken (*.hcf)</translation>
    </message>
    <message>
      <source>Error while opening...</source>
      <translation>Fehler beim Öffnen...</translation>
    </message>
    <message>
      <source>Error while saving...</source>
      <translation>Fehler beim Speichern...</translation>
    </message>
    <message>
      <source>Closing previous database...</source>
      <translation>Vorherige Datenbank wird geschlossen...</translation>
    </message>
    <message>
      <source>Do you want to save the changes?</source>
      <translation>Sollen die Änderungen gespeichert werden?</translation>
    </message>
    <message>
      <source>Error:</source>
      <translation>Fehler:</translation>
    </message>
    <message>
      <source>Confirmation</source>
      <translation>Bestätigung</translation>
    </message>
    <message>
      <source>Warning...</source>
      <translation>Warnung...</translation>
    </message>
    <message>
      <source>You can refresh ONLY a MEDIA node!</source>
      <translation>Es kann nur ein Mediumknoten aktualisiert werden!</translation>
    </message>
    <message>
      <source>Yes</source>
      <translation>Ja</translation>
    </message>
    <message>
      <source>No</source>
      <translation>Nein</translation>
    </message>
    <message>
      <source>Open a file...</source>
      <translation>Datei öffnen...</translation>
    </message>
    <message>
      <source>Save to file...</source>
      <translation>In Datei speichern...</translation>
    </message>
    <message>
      <source>Insert a database file...</source>
      <translation>Datenbankdatei einfügen...</translation>
    </message>
    <message>
      <source>The size of &quot;%1&quot; : 
 %2 
 %3 file /%4 directory</source>
      <translation>Die Grösse von &quot;%1&quot;: 
%2 
%3 Datei(en) / %4 Verzeichnis(se)</translation>
    </message>
    <message>
      <source>Cannot mount CD</source>
      <translation>CD kann nicht gemountet werden</translation>
    </message>
    <message>
      <source>Cannot eject CD!</source>
      <translation>CD kann nicht ausgeworfen werden!</translation>
    </message>
    <message>
      <source>I can't find the &quot;mount&quot; program</source>
      <translation>Programm \&quot;mount\&quot; kann nicht gefunden werden</translation>
    </message>
    <message>
      <source>Cannot mount CD!</source>
      <translation>CD kann nicht gemountet werden!</translation>
    </message>
    <message>
      <source>Cannot eject CD</source>
      <translation>CD kann nicht ausgeworfen werden</translation>
    </message>
    <message>
      <source>I can't find the &quot;eject&quot; program</source>
      <translation>Programm \&quot;eject\&quot; kann nicht gefunden werden</translation>
    </message>
    <message>
      <source>Rename node...</source>
      <translation>Knoten umbenennen...</translation>
    </message>
    <message>
      <source>Delete node</source>
      <translation>Knoten löschen</translation>
    </message>
    <message>
      <source>Add media...</source>
      <translation>Medium hinzufügen...</translation>
    </message>
    <message>
      <source>Rescan media...</source>
      <translation>Medium aktualisieren...</translation>
    </message>
    <message>
      <source>Insert Catalog...</source>
      <translation>Katalog einfügen...</translation>
    </message>
    <message>
      <source>Rescan %1</source>
      <translation>Aktualisieren %1</translation>
    </message>
    <message>
      <source>Re-Number media...</source>
      <translation>Medium umnummerieren...</translation>
    </message>
    <message>
      <source>It is NOT a media node!</source>
      <translation>Es ist kein Medienknoten!</translation>
    </message>
    <message>
      <source>Node size</source>
      <translation>Knotengrösse</translation>
    </message>
    <message>
      <source>View/Edit Comment...</source>
      <translation>Kommentar anzeigen/bearbeiten...</translation>
    </message>
    <message>
      <source>Close all branch</source>
      <translation>Alle Zweige schliessen</translation>
    </message>
    <message>
      <source>There is no selected item in the middle list box!</source>
      <translation>Es ist kein Element im mittleren Ansichtsfenster ausgewählt!</translation>
    </message>
    <message>
      <source>An error occured while scanning,
the DataBase may be incomplete</source>
      <translation>Es trat ein Fehler während des Durchsuchens auf,
Datenbank kann unvollständig sein</translation>
    </message>
    <message>
      <source>There is no selected Media in the middle list box!</source>
      <translation>Es ist kein Medium in der mittleren Auswahlbox ausgewählt!</translation>
    </message>
    <message>
      <source>An error occured while scanning, the rescan operation was cancelled: 
%1</source>
      <translation>Es trat ein Fehler während des Durchsuchens auf, Aktualisierenoperation abgebrochen:
%1</translation>
    </message>
    <message>
      <source>Borrow this media to...</source>
      <translation>Medium ausleihen an...</translation>
    </message>
    <message>
      <source>Do you want to clear the borrowing mark from media &quot;%1&quot;?
(Say yes if you got it back.)</source>
      <translation>Soll die Ausleihmarke von Medium &quot;%1&quot; gelöscht werden?
(Ja, wenn zurückerhalten.)</translation>
    </message>
    <message>
      <source>I got it back! (clear borrowing mark)</source>
      <translation>I bekam es zurück! (Ausleihmarke löschen)</translation>
    </message>
    <message>
      <source>Catalog Link</source>
      <translation>Katalog-Verknüpfung</translation>
    </message>
    <message>
      <source>Follow the link (Open it) !</source>
      <translation>Der Verknüpfung folgen (Öffnen)!</translation>
    </message>
    <message>
      <source>The link is empty.</source>
      <translation>Die Verknüpfung ist leer.</translation>
    </message>
    <message>
      <source>Number</source>
      <translation>Nummer</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>Grösse</translation>
    </message>
    <message>
      <source>The database file has newer version than this version of cdcat can work with:
I understand maximum %1 datafile version but readed %2

Strongly recommended to upgrade your cdcat!!!
Homepage: %3</source>
      <translation>Die Datenbankdatei hat eine neuere Version als diese Version von Cdcat verarbeiten kann: 
Es wird nur bis Version %1 unterstützt, aber Version %2 wurde gelesen 

Es wird dringend empfohlen, Cdcat zu aktualisieren! 
Homepage: %3</translation>
    </message>
    <message>
      <source>No database opened.</source>
      <translation>Keine Datenbank geöffnet.</translation>
    </message>
    <message>
      <source>No item selected.</source>
      <translation>Kein Element ausgewählt.</translation>
    </message>
    <message>
      <source>Select directory</source>
      <translation>Verzeichnis auswählen</translation>
    </message>
    <message>
      <source>The result:</source>
      <translation>Das Ergebnis:</translation>
    </message>
    <message>
      <source>Scanning directory tree, please wait...</source>
      <translation>Verzeichnisbaum wird durchsucht, bitte warten...</translation>
    </message>
    <message>
      <source>View/Edit Category...</source>
      <translation>Kategorie anzeigen/bearbeiten...</translation>
    </message>
    <message>
      <source>Change media type...</source>
      <translation>Medientyp ändern...</translation>
    </message>
    <message>
      <source>Enter media name...</source>
      <translation>Medienname eingeben...</translation>
    </message>
    <message>
      <source>Media name:</source>
      <translation>Medienname:</translation>
    </message>
    <message>
      <source>Media</source>
      <translation>Medium</translation>
    </message>
    <message>
      <source>search for duplicates...</source>
      <translation>nach Duplikaten suchen...</translation>
    </message>
    <message>
      <source>Show content...</source>
      <translation>Inhalt anzeigen...</translation>
    </message>
    <message>
      <source>Show/Remove content...</source>
      <translation>Inhalt anzeigen/löschen...</translation>
    </message>
    <message>
      <source>Add a link to a CdCat Catalog...</source>
      <translation>Verweis auf CdCat-Datenbank hinzufügen...</translation>
    </message>
    <message>
      <source>Add a link to a Cdcat catalog...</source>
      <translation>Verweis auf CdCat-Datenbank hinzufügen...</translation>
    </message>
    <message>
      <source>You have cancelled catalog scanning,
the DataBase may be incomplete</source>
      <translation>Sie haben das Durchsuchen für den Katalog abgebrochen, 
die Datenbank wird unvollständig sein</translation>
    </message>
    <message>
      <source>Scan started</source>
      <translation>Durchsuchen gestartet</translation>
    </message>
    <message>
      <source>Scanning %1 into %2 has been started</source>
      <translation>Durchsuchen von %1 in %2 wurde gestartet</translation>
    </message>
    <message>
      <source>Scanning %1 into %2 has been finished</source>
      <translation>Durchsuchen von %1 in %2 wurde beendet</translation>
    </message>
    <message>
      <source>Scan finished</source>
      <translation>Surchsuchen abgeschlossen</translation>
    </message>
    <message>
      <source>Scanning %1 into %2 has been finished (NOT complete)</source>
      <translation>Durchsuchen von %1 in %2 wurde beendet (NICHT komplett)</translation>
    </message>
    <message>
      <source>Cdcat - idle</source>
      <translation>Cdcat - Leerlauf</translation>
    </message>
    <message>
      <source>Help</source>
      <translation>Hilfe</translation>
    </message>
    <message>
      <source>Whats this?</source>
      <translation>Was ist das?</translation>
    </message>
    <message>
      <source>The cdcat is graphical (QT based) multiplatform (Linux/Windows) catalog program which scans the directories/drives you want and memorize the filesystem including the tags of mp3's and other data and store it in a small file. The database is stored in a gzipped XML format, so you can hack it, or use it if necessary :-).)</source>
      <translation>CdCat ist ein graphisches, Multiplattform-Katalogprogramm, welches gewünschte Verzeichnisse/Laufwerke durchsucht und die Dateisystemstruktur (inklusive MP3-Tags) in einer kleinen Datei speichert. Die Datenbank wird in einem gezipptem XML-Format abgespeichert, so dass man diese hacken oder anderweitig benutzen kann.</translation>
    </message>
    <message>
      <source>The program can store the content of some specified files up to a limit size if you want. (for example: *.nfo)</source>
      <translation>Cdcat kann den Inhalt von einigen angegebenen Dateien bis zu einer gewünschten Grössse speichern (z.B. *.nfo)</translation>
    </message>
    <message>
      <source>Usage:</source>
      <translation>Benutzung:</translation>
    </message>
    <message>
      <source>Before the scanning select the necessary readable components in the config dialog, which can be mp3 tags content of some files or etc.</source>
      <translation>Vor dem Durchsuchen wählen Sie bitte die zu lesbaren Komponenten im  Konfigurationsdialog, wie z.B. MP3-Tags, Inhalt von einigen Dateien, etc. aus.</translation>
    </message>
    <message>
      <source>Create a new catalog</source>
      <translation>Neuen Katalog erstellen</translation>
    </message>
    <message>
      <source>Run the %1 command in the catalog menu. You have to type the name of the new catalog. You can specify the default username of the media(which you scan later), and add a comment to the catalog.</source>
      <translation>Rufen Sie den %1-Menüeintrag vom Datei-Menü auf. Geben Sie nun den Namen des Katalogs ein. Sie können den Nutzernamen, der als Voreinstellung für neue Medien benutzweird und einen Kommentar eingeben.</translation>
    </message>
    <message>
      <source>New</source>
      <translation>Neu</translation>
    </message>
    <message>
      <source>Add media</source>
      <translation>Medium hinzufügen</translation>
    </message>
    <message>
      <source>Run the %1 command in the edit menu. In that dialog you have to specify the directory/or drive the media you want to add. It's recommended to specify the name and the number of the media which has to be unique. (The program always generate one identical name and number). You can label the media to a owner, if necessary.</source>
      <translation>Rufen Sie den %1-Menüeintrag vom Datei-Menü auf. Geben Sie nun den Namen des Katalogs ein. Sie können den Nutzernamen, der als Voreinstellung für neue Medien benutzweird und einen Kommentar eingeben.</translation>
    </message>
    <message>
      <source>save as</source>
      <translation>speichern als</translation>
    </message>
    <message>
      <source>Open an existing catalog:</source>
      <translation>Einen existierenden Katalog öffnen:</translation>
    </message>
    <message>
      <source>open</source>
      <translation>öffnen</translation>
    </message>
    <message>
      <source>Author:</source>
      <translation>Autor:</translation>
    </message>
    <message>
      <source>The program was written by Peter Deak (hungary)</source>
      <translation>Das Programm wurde von Peter Deak geschrieben (Ungarn)</translation>
    </message>
    <message>
      <source>The current maintainer is %1.</source>
      <translation>Der aktuelle Maintainer ist %1.</translation>
    </message>
    <message>
      <source>License:</source>
      <translation>Lizenz:</translation>
    </message>
    <message>
      <source>General Public License (GPL)</source>
      <translation>General Public License (GPL)</translation>
    </message>
    <message>
      <source>Homepage:</source>
      <translation>Homepage:</translation>
    </message>
    <message>
      <source>You can read about the program and get new versions, sources etc, in the hompage of cdcat:</source>
      <translation>Sie können Informationen über das Programm, neue Versionen, Quellcode, etc. auf der Homepage von Cdcat erhalten:</translation>
    </message>
    <message>
      <source>Run the %1 command in the file menu, and choice the file of the catalog. (*.hcf). After the opening you will be able browse the catalog or search in it.</source>
      <translation>Rufen Sie den %1-Eintrag im Dateimenü auf und wählen die Datei des Katalogs (*.hcf). Nach dem Öffnen können Sie den Katalog durchsuchen oder in ihm suchen.</translation>
    </message>
    <message>
      <source>If you scanned your media, you will be able to browse in it with the browser window (like mc) , or search in it. You can save the catalog with %1 command in the file menu.</source>
      <translation>Wenn Sie Ihr Medium durchsucht haben, können Sie dies im Durchsuchen-Fenster durchsuchen (wie im mc) oder in ihm suchen. Sie können den Katalog mit dem %1-Kommando aus dem Dateimenü speichern.</translation>
    </message>
    <message>
      <source>Saving catalog, please wait...</source>
      <translation>Katalog wird gespeichert, bitte warten...</translation>
    </message>
    <message>
      <source>Insert a cdcat exported xml file...</source>
      <translation>Von Cdcat exportierte XML-Datei einfügen...</translation>
    </message>
    <message>
      <source>CdCat xml export (*.xml )</source>
      <translation>Cdcat-XML-Export (*.xml)</translation>
    </message>
    <message>
      <source>Change password...</source>
      <translation>Passwort ändern...</translation>
    </message>
    <message>
      <source>Password has been successfully changed</source>
      <translation>Passwort wurde erfolgreich geändert</translation>
    </message>
    <message>
      <source>Password changed</source>
      <translation>Passwort geändert</translation>
    </message>
    <message>
      <source>Password length is too short, must be minimum 4 chars</source>
      <translation>Passwortlänge ist zu kurz, muss mindestens 4 Zeichen lang sein</translation>
    </message>
    <message>
      <source>Password too short</source>
      <translation>Passwort zu kurz</translation>
    </message>
    <message>
      <source>Password length is too big, must be maximal %1 chars</source>
      <translation>Passwortlänge ist zu lang, darf maximal %1 Zeichen lang sein</translation>
    </message>
    <message>
      <source>Password too big</source>
      <translation>Passwort zu lang</translation>
    </message>
    <message>
      <source>Disable encryption</source>
      <translation>Verschlüsselung deaktivieren</translation>
    </message>
    <message>
      <source>Enable encryption</source>
      <translation>Verschlüsselung aktivieren</translation>
    </message>
    <message>
      <source>Encryption has been successfully enabled</source>
      <translation>Verschlüsselung wurde erfolgreich aktiviert</translation>
    </message>
    <message>
      <source>Encryption has been successfully disabled</source>
      <translation>Verschlüsselung wurde erfolgreich deaktiviert</translation>
    </message>
    <message>
      <source>Passwords not match</source>
      <translation>Passwörter stimmen nicht überein</translation>
    </message>
    <message>
      <source>Passwords does not match</source>
      <translation>Passwörter stimmen nicht überein</translation>
    </message>
    <message>
      <source>Do you really want to delete
&quot;%1&quot;?</source>
      <translation>Möchten Sie 
&quot;%1&quot;
wirklich löschen?</translation>
    </message>
  </context>
  <context>
    <name>HDirectoryView</name>
    <message>
      <source>Directory Tree</source>
      <translation>Verzeichnisbaum</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
  </context>
  <context>
    <name>HQListView</name>
    <message>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>Grösse</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
  </context>
  <context>
    <name>ImportDialog</name>
    <message>
      <source>Import CSV file</source>
      <translation>CSV-Datei importieren</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>Import</source>
      <translation>Importieren</translation>
    </message>
    <message>
      <source>Error:</source>
      <translation>Fehler:</translation>
    </message>
    <message>
      <source>You must be fill the &quot;Filename&quot; field!</source>
      <translation>Es muss das &quot;Dateiname&quot;-Feld ausgefüllt werden!</translation>
    </message>
    <message>
      <source>csv files(*.csv)</source>
      <translation>CSV-Dateien (*.csv)</translation>
    </message>
    <message>
      <source>Choose a file for import</source>
      <translation>Datei zum Importieren auswählen</translation>
    </message>
    <message>
      <source>Create new Database</source>
      <translation>Neue Datenbank erstellen</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
    <message>
      <source>&lt;strong>Please read the README_IMPORT before you import!&lt;/strong></source>
      <translation>&lt;strong>Achtung! Bitte README_IMPORT.DE vor dem Import lesen!&lt;/strong></translation>
    </message>
    <message>
      <source>xml files(*.xml)</source>
      <translation>XML-Dateien(*.xml)</translation>
    </message>
    <message>
      <source>File:</source>
      <translation>Datei:</translation>
    </message>
    <message>
      <source>Correct bad style from gtktalog export</source>
      <translation>Schlechten Stil von Gtktalog korrigieren</translation>
    </message>
    <message>
      <source>Separator:</source>
      <translation>Trennzeichen:</translation>
    </message>
    <message>
      <source>This is the separator in dataline
&lt;path>SEPARATOR&lt;size>SEPARATOR&lt;date>&lt;space>&lt;time></source>
      <translation>Dies ist das Trennzeiche in Datenzeile 
&lt;pfad>TRENNZEICHEN&lt;größe>TRENNZEICHEN&lt;datum>&lt;leerzeichen>&lt;zeit></translation>
    </message>
    <message>
      <source>...</source>
      <translation>...</translation>
    </message>
    <message>
      <source>Gtktalog &amp;XML</source>
      <translation>Gtktalog &amp;XML</translation>
    </message>
    <message>
      <source>&amp;WhereIsIt XML (classic)</source>
      <translation>&amp;WhereIsIt XML (klassisch)</translation>
    </message>
    <message>
      <source>Select the type of import here</source>
      <translation>Typ des Imports auswählen</translation>
    </message>
    <message>
      <source>Open the file dialog for selecting file to import.</source>
      <translation>Öffnet den Dateidialog zum Auwählen der Datei, die importiert werden soll</translation>
    </message>
    <message>
      <source>Corrects bad output style from gtktalog.
&lt;media>SEPARATOR/&lt;dir>/SEPARATOR/&lt;dir>
 will be to
&lt;media>/&lt;dir>/&lt;dir></source>
      <translation>Korrigiert schlechten Stil von gtktalog. 
&lt;medium>TRENNZEICHEN/&lt;verzeichnis>/TRENNZEICHEN/&lt;verzeichnis> 
wird zu 
&lt;medium>/&lt;verzeichnis>/&lt;verzeichnis></translation>
    </message>
    <message>
      <source>You must be fill the &quot;Separator&quot; field!</source>
      <translation>Das \&quot;Trennzeichen\&quot;-Feld muß ausgefüllt werden!</translation>
    </message>
    <message>
      <source>all files(*.*)</source>
      <translation>Alle Dateien(*.*)</translation>
    </message>
    <message>
      <source>&amp;Gtktalog (csv)</source>
      <translation>&amp;Gtktalog (CSV)</translation>
    </message>
    <message>
      <source>Select this for importing a text import (csv) generated from Gtktalog</source>
      <translation>Wählen Sie dies, um einen Textimport (CSV), welcher von Gtktalog generiert wurde, durchzuführen</translation>
    </message>
    <message>
      <source>&amp;Kat-CeDe (csv)</source>
      <translation>&amp;Kat-CeDe (CSV)</translation>
    </message>
    <message>
      <source>Select this for importing a text import (csv) generated from Kat-CeDe.</source>
      <translation>Wählen Sie dies, um einen Textimport (CSV), welcher von Kat-CeDe generiert wurde, durchzuführen</translation>
    </message>
    <message>
      <source>Select this for importing a xml report generated from gtktalog</source>
      <translation>Wählen Sie dies, um einen XML-Import, welcher von Gtktalog generiert wurde, durchzuführen</translation>
    </message>
    <message>
      <source>Select this for importing a xml report generated from WhereIsIt?</source>
      <translation>Wählen Sie dies, um einen XML-Import, welcher von WhereIsIt? generiert wurde, durchzuführen</translation>
    </message>
    <message>
      <source>&amp;Disclib (csv)</source>
      <translation>&amp;Disclib (CSV)</translation>
    </message>
    <message>
      <source>Select this for importing a text import (csv) generated from Disclib.</source>
      <translation>Wählen Sie dies, um einen Textimport (CSV), welcher von Disclib generiert wurde, durchzuführen</translation>
    </message>
    <message>
      <source>&amp;VisualCD (csv)</source>
      <translation>&amp;VisualCD (CSV)</translation>
    </message>
    <message>
      <source>Select this for importing a text import (csv) generated from VisualCD.</source>
      <translation>Wählen Sie dies, um einen Textimport (CSV), welcher von VisualCD generiert wurde, durchzuführen</translation>
    </message>
    <message>
      <source>&amp;VVV (csv)</source>
      <translation>&amp;VVV (CSV)</translation>
    </message>
    <message>
      <source>Select this for importing a text import (csv) generated from VVV.</source>
      <translation>Wählen Sie dies, um einen Textimport (CSV), welcher von VVV (Virtual Volumes View) generiert wurde, durchzuführen</translation>
    </message>
    <message>
      <source>&amp;Advanced File Organizer (csv)</source>
      <translation>&amp;Advanced File Organizer (CSV)</translation>
    </message>
    <message>
      <source>Select this for importing a text import (csv) generated from Advanced File Organizer.</source>
      <translation>Wählen Sie dies, um einen Textimport (CSV), welcher von Advanced File Organizer generiert wurde, durchzuführen</translation>
    </message>
    <message>
      <source>&amp;File Archivist</source>
      <translation>&amp;File Archivist</translation>
    </message>
    <message>
      <source>Select this for importing a File Archivist catalog.</source>
      <translation>Wählen Sie dies, um ein File Archivist-Katalog zu importieren.</translation>
    </message>
    <message>
      <source>File Archivist files(*.arch)</source>
      <translation>File Archivist-Katalogdateien(*.arch)</translation>
    </message>
    <message>
      <source>&amp;Advanced Disk Catalog (csv)</source>
      <translation>&amp;Advanced Disk Catalog (CSV)</translation>
    </message>
    <message>
      <source>Select this for importing a text import (csv) generated from Advanced Disk Catalog.</source>
      <translation>Wählen Sie dies, um einen Textimport (CSV), welcher von Advanced Disk Catalog generiert wurde, durchzuführen</translation>
    </message>
    <message>
      <source>W&amp;hereIsIt (csv)</source>
      <translation>W&amp;hereIsIt (CSV)</translation>
    </message>
    <message>
      <source>Select this for importing a text import (csv) generated from WhereIsIt.</source>
      <translation>Wählen Sie dies, um einen Textimport (CSV), welcher von WhereIsIt? generiert wurde, durchzuführen</translation>
    </message>
  </context>
  <context>
    <name>InfoDialog</name>
    <message>
      <source>About cdcat</source>
      <translation>Über Cdcat</translation>
    </message>
    <message>
      <source>close</source>
      <translation>Schliessen</translation>
    </message>
    <message>
      <source>About Cdcat</source>
      <translation>Über Cdcat</translation>
    </message>
    <message>
      <source>License</source>
      <translation>Lizenz</translation>
    </message>
    <message>
      <source>Thanks</source>
      <translation>Dank an</translation>
    </message>
    <message>
      <source>German translation &amp; additional programming</source>
      <translation>Deutsche Übersetzung &amp; zusätzliche Programmierung</translation>
    </message>
    <message>
      <source>Spanish translation</source>
      <translation>Spanische Übersetzung</translation>
    </message>
    <message>
      <source>Czech translation</source>
      <translation>Tschechische Übersetzung</translation>
    </message>
    <message>
      <source>Author:</source>
      <translation>Autor:</translation>
    </message>
    <message>
      <source>Copyright (C) 2003 Peter Deak (GPL)</source>
      <translation>Copyright (C) 2003 Peter Deak (GPL)</translation>
    </message>
    <message>
      <source>-= CdCatalog by Hyper =-</source>
      <translation>-= CdCatalog von Hyper =-</translation>
    </message>
    <message>
      <source>Version:</source>
      <translation>Version:</translation>
    </message>
    <message>
      <source>AVI reader plugin &amp; Polish translation</source>
      <translation>AVI-Leser-Plugin &amp; Polnische Übersetzung</translation>
    </message>
    <message>
      <source>Copyright (C) 2010 Christoph Thielecke (GPL)</source>
      <translation>Copyright (C) 2010 Christoph Thielecke (GPL)</translation>
    </message>
    <message>
      <source>Ported to Qt4</source>
      <translation>Portiert nach Qt4</translation>
    </message>
    <message>
      <source> (with debug)</source>
      <translation> (mit Debug)</translation>
    </message>
    <message>
      <source>Development version build at</source>
      <translation>Entwicklungsversion erstellt am</translation>
    </message>
    <message>
      <source>For more details about the GPL license and to read in other languages, visit %1.</source>
      <translation>Für weitere Details der GPL-Lizenz und um sie in anderen Sprachen zu lesen, besuchen Sie die %1.</translation>
    </message>
    <message>
      <source>GPL page on GNU website</source>
      <translation>GPL-Seite auf der GNU-Webseite</translation>
    </message>
    <message>
      <source>Features:</source>
      <translation>Fähigkeiten:</translation>
    </message>
    <message>
      <source>archive read support using lib7zip</source>
      <translation>Unterstützung zum Lesen von Archiven mit lib7zip</translation>
    </message>
    <message>
      <source>mediainfo (compiled in)</source>
      <translation>mediainfo (einkompiliert)</translation>
    </message>
    <message>
      <source>mediainfo</source>
      <translation>mediainfo</translation>
    </message>
    <message>
      <source>exif data read support</source>
      <translation>Unterstützung zum Lesen von EXIF-Ddaten</translation>
    </message>
    <message>
      <source>encrypted catalog support</source>
      <translation>Unterstützung für verschlüsselten Katalog</translation>
    </message>
  </context>
  <context>
    <name>KeyBindingDialog</name>
    <message>
      <source>Action</source>
      <translation>Aktion</translation>
    </message>
    <message>
      <source>Shortcut</source>
      <translation>Tastenkürzel</translation>
    </message>
    <message>
      <source>Key bindings</source>
      <translation>Kurzbefehle</translation>
    </message>
    <message>
      <source>close</source>
      <translation>Schließen</translation>
    </message>
  </context>
  <context>
    <name>PWw</name>
    <message>
      <source>Please Wait...</source>
      <translation>Bitte warten...</translation>
    </message>
    <message>
      <source>&amp;Cancel</source>
      <translation>&amp;Abbrechen</translation>
    </message>
  </context>
  <context>
    <name>QObject</name>
    <message>
      <source>Directory</source>
      <translation>Verzeichnis</translation>
    </message>
    <message>
      <source>Unreadable Directory</source>
      <translation>Unlesbares Verzeichnis</translation>
    </message>
    <message>
      <source>Catalog</source>
      <translation>Katalog</translation>
    </message>
    <message>
      <source>Unknown(DB)</source>
      <translation>Unbekannt(DB)</translation>
    </message>
    <message>
      <source>CD</source>
      <translation>CD</translation>
    </message>
    <message>
      <source>DVD</source>
      <translation>DVD</translation>
    </message>
    <message>
      <source>HardDisc</source>
      <translation>Festplatte</translation>
    </message>
    <message>
      <source>Floppy</source>
      <translation>Diskette</translation>
    </message>
    <message>
      <source>NetworkDrv</source>
      <translation>Netzwerkgerät</translation>
    </message>
    <message>
      <source>FlashDrv</source>
      <translation>Flashspeicher</translation>
    </message>
    <message>
      <source>OtherDevice</source>
      <translation>Anderes Gerät</translation>
    </message>
    <message>
      <source>Video:
</source>
      <translation>Video:
</translation>
    </message>
    <message>
      <source>Total Time</source>
      <translation>Gesamtzeit</translation>
    </message>
    <message>
      <source>Framerate</source>
      <translation>Framerate</translation>
    </message>
    <message>
      <source>Resolution</source>
      <translation>Auflösung</translation>
    </message>
    <message>
      <source>Artist</source>
      <translation>Künstler</translation>
    </message>
    <message>
      <source>Comments</source>
      <translation>Kommentare</translation>
    </message>
    <message>
      <source>Copyright</source>
      <translation>Copyright</translation>
    </message>
    <message>
      <source>Creation Date</source>
      <translation>Erstellungsdatum</translation>
    </message>
    <message>
      <source>Engineer</source>
      <translation>Techniker</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>Product</source>
      <translation>Produkt</translation>
    </message>
    <message>
      <source>Software</source>
      <translation>Software</translation>
    </message>
    <message>
      <source>Source</source>
      <translation>Quelle</translation>
    </message>
    <message>
      <source>Subject</source>
      <translation>Betreff</translation>
    </message>
    <message>
      <source>Technician</source>
      <translation>Techniker</translation>
    </message>
    <message>
      <source>Unknown/not implemented/broken header
</source>
      <translation>Unbekannt/nicht implemntiert/fehlerhafter Header
</translation>
    </message>
    <message>
      <source>Channels</source>
      <translation>Kanäle</translation>
    </message>
    <message>
      <source>Bitrate</source>
      <translation>Bitrate</translation>
    </message>
    <message>
      <source>Genre</source>
      <translation>Genre</translation>
    </message>
    <message>
      <source>Not a RIFF/AVI file OR header broken!</source>
      <translation>Keine RIFF-/AVI-Datei oder Header fehlerhaft!</translation>
    </message>
    <message>
      <source>Sample/s</source>
      <translation>Sample/s</translation>
    </message>
    <message>
      <source>importuser</source>
      <translation>Importbenutzer</translation>
    </message>
    <message>
      <source>Audio:
</source>
      <translation>Audio:
</translation>
    </message>
    <message>
      <source>More Data:
</source>
      <translation>Mehr Daten:</translation>
    </message>
    <message>
      <source>Enter password...</source>
      <translation>Passwort eingeben...</translation>
    </message>
    <message>
      <source>Enter password for catalog:</source>
      <translation>Passwort für Katalog eingeben:</translation>
    </message>
    <message>
      <source>Catalog (encrypted)</source>
      <translation>Katalog (verschlüsselt)</translation>
    </message>
    <message>
      <source>password empty</source>
      <translation>Passwort leer</translation>
    </message>
    <message>
      <source>cant set password</source>
      <translation>Passwort kann nicht gesetzt werden</translation>
    </message>
    <message>
      <source>decrypt failed</source>
      <translation>Entschlüsselung fehlgeschlagen</translation>
    </message>
    <message>
      <source>cant load catalog: encrypted catalog support not available</source>
      <translation>Katalog kann nicht geladen werden: Unterstützung für verschlüsselten Katalog nicht verfügbar</translation>
    </message>
    <message>
      <source>Enter password for catalog (again):</source>
      <translation>Passwort für Katalog eingeben (nochmal):</translation>
    </message>
  </context>
  <context>
    <name>SelReadable</name>
    <message>
      <source>Select readable items</source>
      <translation>Lesbare Elemente auswählen</translation>
    </message>
    <message>
      <source>Read mp3 tags</source>
      <translation>MP3-Tags lesen</translation>
    </message>
    <message>
      <source>Store content of some files</source>
      <translation>Inhalt von einigen Dateien speichern</translation>
    </message>
    <message>
      <source>Ok</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>content size limit in kByte</source>
      <translation>Begrenzung für Inhaltsgrösse in KByte</translation>
    </message>
    <message>
      <source>; separated list of readable file patterns</source>
      <translation>;-getrennte Liste von lesbaren Dateimustern</translation>
    </message>
    <message>
      <source>Default tag</source>
      <translation>Tag per Voreinstellung</translation>
    </message>
    <message>
      <source>version</source>
      <translation>Version</translation>
    </message>
    <message>
      <source>Read mp3 technical info as comment (bitrate,freq,length...)</source>
      <translation>Technische MP3-Informationen als Kommentar lesen (Bitrate, Frequenz, Länge...)</translation>
    </message>
    <message>
      <source>Read avi technical info as comment (codecs,length,...)</source>
      <translation>Technische AVI-Informationen als Kommentar lesen (Codecs, Länge, ...)</translation>
    </message>
    <message>
      <source>Supported extensions:</source>
      <translation>Unterstützte Dateiendungen:</translation>
    </message>
    <message>
      <source>Archive file display options</source>
      <translation>Anzeigeoptionen für Archivdateien</translation>
    </message>
    <message>
      <source>Scan for archive file list</source>
      <translation>Archive nach Dateiliste durchsuchen</translation>
    </message>
    <message>
      <source>Read some technical info using mediainfo</source>
      <translation>Einige technische Informationen mit mediainfo lesen</translation>
    </message>
    <message>
      <source>Read thumbnails from pictures</source>
      <translation>Vorschaubilder von Bildern laden</translation>
    </message>
    <message>
      <source>Read EXIF data from pictures</source>
      <translation>EXIF-Daten von Bildern lesen</translation>
    </message>
    <message>
      <source>x</source>
      <translation>x</translation>
    </message>
    <message>
      <source>; separated list of image file extensions, e.g. png;jpg;gif</source>
      <translation>;getrennte Liste von Bilddateiendungen, z.B. png;jpg;gif</translation>
    </message>
    <message>
      <source>Path to external content viewer (found)</source>
      <translation>Pfad zu externem Anzeiger (gefunden)</translation>
    </message>
    <message>
      <source>Path to external content viewer (not found)</source>
      <translation>Pfad zu externem Anzeiger (nicht gefunden)</translation>
    </message>
    <message>
      <source>Choose path to external context viewer</source>
      <translation>Pfad zu externem Anzeiger auswählen</translation>
    </message>
    <message>
      <source>...</source>
      <translation>...</translation>
    </message>
    <message>
      <source>Select external viewer...</source>
      <translation>Externen Anzeiger auswählen...</translation>
    </message>
    <message>
      <source>KiB</source>
      <translation>KiB</translation>
    </message>
    <message>
      <source>lib7zip found</source>
      <translation>lib7zip gefunden</translation>
    </message>
    <message>
      <source>lib7zip not supported</source>
      <translation>lib7zip nicht unterstützt</translation>
    </message>
    <message>
      <source>Archive support:</source>
      <translation>Archivunterstützung:</translation>
    </message>
    <message>
      <source>mediainfo not found</source>
      <translation>mediainfo nicht gefunden</translation>
    </message>
    <message>
      <source>mediainfo found</source>
      <translation>mediainfo gefunden</translation>
    </message>
    <message>
      <source>mediainfo status:</source>
      <translation>mediainfo-Status:</translation>
    </message>
    <message>
      <source>Permission</source>
      <translation>Berechtigung</translation>
    </message>
    <message>
      <source>User</source>
      <translation>Nutzer</translation>
    </message>
    <message>
      <source>Group</source>
      <translation>Gruppe</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>Grösse</translation>
    </message>
    <message>
      <source>Date</source>
      <translation>Datum</translation>
    </message>
    <message>
      <source>Comment</source>
      <translation>Kommentar</translation>
    </message>
    <message>
      <source>Read thumbnails</source>
      <translation>Vorschaubilder lesen</translation>
    </message>
    <message>
      <source>Size:</source>
      <translation>Grösse:</translation>
    </message>
    <message>
      <source>Thumbnail size (width) in pixels</source>
      <translation>Vorschaubildgröße (Breite) in Pixel</translation>
    </message>
    <message>
      <source>Thumbnail size (height) in pixels</source>
      <translation>Vorschaubildgröße (Höhe) in Pixel</translation>
    </message>
    <message>
      <source>Use external file content viewer</source>
      <translation>Externen Inhaltsanzeiger verwenden</translation>
    </message>
    <message>
      <source>file patterns:</source>
      <translation>Dateimuster:</translation>
    </message>
    <message>
      <source>File extensions:</source>
      <translation>Dateiendungen:</translation>
    </message>
    <message>
      <source>max size:</source>
      <translation>max. Größe:</translation>
    </message>
    <message>
      <source>Read mp3 info as comment</source>
      <translation>MP3-Info's als Kommentar lesen</translation>
    </message>
    <message>
      <source>Read avi info as comment</source>
      <translation>AVI-Info's als Kommentar lesen</translation>
    </message>
    <message>
      <source>Add exclude rule...</source>
      <translation>Aussschlußregel hinzufügen</translation>
    </message>
    <message>
      <source>Enter regular expression for exclude:</source>
      <translation>Regulären Ausdruck zum Ausschließen eingeben:</translation>
    </message>
    <message>
      <source>edit rule...</source>
      <translation>Regel bearbeiten...</translation>
    </message>
    <message>
      <source>delete rule...</source>
      <translation>Regel löschen...</translation>
    </message>
    <message>
      <source>exclude files/directories</source>
      <translation>Dateien/Verzeichnisse ausschließen</translation>
    </message>
    <message>
      <source>add exclude rule...</source>
      <translation>Ausschlußregel hinzufügen...</translation>
    </message>
    <message>
      <source>list of patterns (regular expression) for files/directories to skip on reading from filesystem</source>
      <translation>Liste von Mustern (regulärer Ausdruck) für Dateien/Verzeichnisse, die beim Lesen vom Dateisystem übersprungen werden sollen</translation>
    </message>
    <message>
      <source>regular expression is invalid</source>
      <translation>regulärer Ausdruck ist ungültig</translation>
    </message>
    <message>
      <source>regular expression is valid</source>
      <translation>regulärer Ausdruck ist gültig</translation>
    </message>
    <message>
      <source>show archive file at scanning in status</source>
      <translation>Archivdatei während des Durchsuchens in Status anzeigen</translation>
    </message>
    <message>
      <source>show archive file in status</source>
      <translation>Archivdatei in Status anzeigen</translation>
    </message>
    <message>
      <source>About regular expressions</source>
      <translation>Über reguläre Ausdrücke</translation>
    </message>
    <message>
      <source>close</source>
      <translation>schliessen</translation>
    </message>
    <message>
      <source>About regex:</source>
      <translation>Über Regex:</translation>
    </message>
    <message>
      <source>Information about regular expressions</source>
      <translation>Informationen über reguläre Ausdrücke</translation>
    </message>
    <message>
      <source>About regular expressions....</source>
      <translation>Über reguläre Ausdrücke...</translation>
    </message>
    <message>
      <source>Show introduction into regular expressions</source>
      <translation>Einführung in reguläre Ausdrücke anzeigen</translation>
    </message>
    <message>
      <source>Metacharacter</source>
      <translation>Metazeichen</translation>
    </message>
    <message>
      <source>Description</source>
      <translation>Beschreibung</translation>
    </message>
    <message>
      <source>A bracket expression. Matches a single character that is contained within the brackets. For example, &lt;code>[abc]&lt;/code> matches &quot;&lt;i>a&lt;/i>&quot;, &quot;&lt;i>b&lt;/i>&quot;, or &quot;&lt;i>c&lt;/i>&quot;. &lt;code>[a-z]&lt;/code> specifies a range which matches any lowercase letter from &quot;&lt;i>a&lt;/i>&quot; to &quot;&lt;i>z&lt;/i>&quot;. These forms can be mixed: &lt;code>[abcx-z]&lt;/code> matches &quot;&lt;i>a&lt;/i>&quot;, &quot;&lt;i>b&lt;/i>&quot;, &quot;&lt;i>c&lt;/i>&quot;, &quot;&lt;i>x&lt;/i>&quot;, &quot;&lt;i>y&lt;/i>&quot;, or &quot;&lt;i>z&lt;/i>&quot;, as does &lt;code>[a-cx-z]&lt;/code>.</source>
      <translation>Ein Klammerausdruck, welcher auf ein einzelnes Zeichen passt, dass in den Klammern eingeschlossen ist. Zum Beispiel passt &lt;code>[abc]&lt;/code> auf &quot;&lt;i>a&lt;/i>&quot;, &quot;&lt;i>b&lt;/i>&quot; oder &quot;&lt;i>c&lt;/i>&quot;. &lt;code>[a-z]&lt;/code> gibt einen Bereich an, der auf jeden Kleinbuchstaben von &quot;&lt;i>a&lt;/i>&quot; bis &quot;&lt;i>z&lt;/i>&quot; passt. Diese Formen können gemischt werden: &lt;code>[abcx-z]&lt;/code> passt auf &quot;&lt;i>a&lt;/i>&quot;, &quot;&lt;i>b&lt;/i>&quot;, &quot;&lt;i>c&lt;/i>&quot;, &quot;&lt;i>x&lt;/i>&quot;, &quot;&lt;i>y&lt;/i>&quot; oder &quot;&lt;i>z&lt;/i>&quot; wie auch &lt;code>[a-cx-z]&lt;/code>.</translation>
    </message>
    <message>
      <source>The &lt;code>-&lt;/code> character is treated as a literal character if it is the last or the first (after the &lt;code>^&lt;/code>) character within the brackets: &lt;code>[abc-]&lt;/code>, &lt;code>[-abc]&lt;/code>. Note that backslash escapes are not allowed. The &lt;code>]&lt;/code> character can be included in a bracket expression if it is the first (after the &lt;code>^&lt;/code>) character: &lt;code>[]abc]&lt;/code>.</source>
      <translation>Das &lt;code>-&lt;/code>-Zeichen wird aus  character is Literalzeichen behandelt, wenn es das letzte oder erste Zeichen (nach dem &lt;code>^&lt;/code>-Zeichen) zwischen den Klammern ist:: &lt;code>[abc-]&lt;/code>, &lt;code>[-abc]&lt;/code>. Bitte beachten Sie, dass Entwertung mit dem Backslash (\\) nicht erlaubt ist. Das &lt;code>]&lt;/code>-Zeichen kann in dem Klammerausdruck enthalten sein, wenn es das erste Zeichen (nach dem &lt;code>^&lt;/code>-Zeichen) ist: &lt;code>[]abc]&lt;/code>.</translation>
    </message>
    <message>
      <source>Matches a single character that is not contained within the brackets. For example, &lt;code>[^abc]&lt;/code> matches any character other than &quot;&lt;i>a&lt;/i>&quot;, &quot;&lt;i>b&lt;/i>&quot;, or &quot;&lt;i>c&lt;/i>&quot;. &lt;code>[^a-z]&lt;/code> matches any single character that is not a lowercase letter from &quot;&lt;i>a&lt;/i>&quot; to &quot;&lt;i>z&lt;/i>&quot;. Likewise, literal characters and ranges can be mixed.</source>
      <translation>Passt auf ein einzelnes Zeichen, dass nicht in Klammern eingeschlossen ist. Zum Beispiel, passt &lt;code>[^abc]&lt;/code>auf ein beliebiges Zeichen außer &quot;&lt;i>a&lt;/i>&quot;, &quot;&lt;i>b&lt;/i>&quot; oder &quot;&lt;i>c&lt;/i>&quot;. &lt;code>[^a-z]&lt;/code> passt auf ein einzelnes Zeichen, dass kein Kleinbuchstabe von &quot;&lt;i>a&lt;/i>&quot; bis &quot;&lt;i>z&lt;/i>&quot; ist. Ebenfalls können Literalzeichen und Bereiche gemischt werden.</translation>
    </message>
    <message>
      <source>Matches the starting position within the string. In line-based tools, it matches the starting position of any line.</source>
      <translation>Passt auf die Startposition der Zeichenkette. In Kommandozeilenwerkzeugen passt die Startposition jeder Zeile.</translation>
    </message>
    <message>
      <source>Matches what the &lt;i>n&lt;/i>th marked subexpression matched, where &lt;i>n&lt;/i> is a digit from 1 to 9. This construct is theoretically &lt;b>irregular&lt;/b> and was not adopted in the POSIX ERE syntax. Some tools allow referencing more than nine capturing groups.</source>
      <translation>Passt auf den &lt;i>n&lt;/i>-ten markierten Unterausdruck, wobei &lt;i>n&lt;/i> eine Zahl von 1 bis 9 ist. Dieses Konstrukt ist theoretisch &lt;b>irregulär&lt;/b> und wurde nicht auf in der POSIX ERE syntax angeglichen. Einige Werkzeuge erlauben das Referenzieren von mehr als 9 Aufzeichnungsgruppen.</translation>
    </message>
    <message>
      <source>Matches the preceding element zero or more times. For example, &lt;code>ab*c&lt;/code> matches &quot;&lt;i>ac&lt;/i>&quot;, &quot;&lt;i>abc&lt;/i>&quot;, &quot;&lt;i>abbbc&lt;/i>&quot;, etc. &lt;code>[xyz]*&lt;/code> matches &quot;&quot;, &quot;&lt;i>x&lt;/i>&quot;, &quot;&lt;i>y&lt;/i>&quot;, &quot;&lt;i>z&lt;/i>&quot;, &quot;&lt;i>zx&lt;/i>&quot;, &quot;&lt;i>zyx&lt;/i>&quot;, &quot;&lt;i>xyzzy&lt;/i>&quot;, and so on. &lt;code>\(ab\)*&lt;/code> matches &quot;&quot;, &quot;&lt;i>ab&lt;/i>&quot;, &quot;&lt;i>abab&lt;/i>&quot;, &quot;&lt;i>ababab&lt;/i>&quot;, and so on.</source>
      <translation>Passt auf das vorhergehende Element 0-mal oder mehrfach. Zum Beispiel, passt &lt;code>ab*c&lt;/code> auf &quot;&lt;i>ac&lt;/i>&quot;, &quot;&lt;i>abc&lt;/i>&quot;, &quot;&lt;i>abbbc&lt;/i>&quot;, etc. &lt;code>[xyz]*&lt;/code> passt auf &quot;&quot;, &quot;&lt;i>x&lt;/i>&quot;, &quot;&lt;i>y&lt;/i>&quot;, &quot;&lt;i>z&lt;/i>&quot;, &quot;&lt;i>zx&lt;/i>&quot;, &quot;&lt;i>zyx&lt;/i>&quot;, &quot;&lt;i>xyzzy&lt;/i>&quot; usw. &lt;code>\(ab\)*&lt;/code> passt auf &quot;&quot;, &quot;&lt;i>ab&lt;/i>&quot;, &quot;&lt;i>abab&lt;/i>&quot;, &quot;&lt;i>ababab&lt;/i>&quot; usw.</translation>
    </message>
    <message>
      <source>Examples:</source>
      <translation>Beispiele:</translation>
    </message>
    <message>
      <source>matches any three-character string ending with &quot;at&quot;, including &quot;&lt;i>hat&lt;/i>&quot;, &quot;&lt;i>cat&lt;/i>&quot;, and &quot;&lt;i>bat&lt;/i>&quot;.</source>
      <translation>Passt auf jede Zeichenkette, die 3 Zeichen lang ist und auf &quot;at&quot; endet, wie z.B. &quot;&lt;i>hat&lt;/i>&quot;, &quot;&lt;i>cat&lt;/i>&quot; und &quot;&lt;i>bat&lt;/i>&quot;.</translation>
    </message>
    <message>
      <source>matches &quot;&lt;i>hat&lt;/i>&quot; and &quot;&lt;i>cat&lt;/i>&quot;.</source>
      <translation>passt auf &quot;&lt;i>hat&lt;/i>&quot; und &quot;&lt;i>cat&lt;/i>&quot;.</translation>
    </message>
    <message>
      <source>matches all strings matched by &lt;code>.at&lt;/code> except &quot;&lt;i>bat&lt;/i>&quot;.</source>
      <translation>Passt auf alle Zeichenketten, die auf &lt;code>.at&lt;/code> passen, ausser auf &quot;&lt;i>bat&lt;/i>&quot;.</translation>
    </message>
    <message>
      <source>matches &quot;&lt;i>hat&lt;/i>&quot; and &quot;&lt;i>cat&lt;/i>&quot;, but only at the beginning of the string or line.</source>
      <translation>passt auf &quot;&lt;i>hat&lt;/i>&quot; und &quot;&lt;i>cat&lt;/i>&quot;, aber nur am Anfang der Zeichenkette oder Zeile</translation>
    </message>
    <message>
      <source>matches &quot;&lt;i>hat&lt;/i>&quot; and &quot;&lt;i>cat&lt;/i>&quot;, but only at the end of the string or line.</source>
      <translation>passt auf &quot;&lt;i>hat&lt;/i>&quot; und &quot;&lt;i>cat&lt;/i>&quot;, aber nur am Ende der Zeichenkette oder Zeile</translation>
    </message>
    <message>
      <source>Source:</source>
      <translation>Quelle:</translation>
    </message>
    <message>
      <source>matches any single character surrounded by &quot;[&quot; and &quot;]&quot; since the brackets are escaped, for example: &quot;&lt;i>[a]&lt;/i>&quot; and &quot;&lt;i>[b]&lt;/i>&quot;.</source>
      <translation>passt auf ein einzelnes Zeichen umrahmt von &quot;[&quot; und &quot;]&quot;, da die Klammern entwertet werden, z.B. &quot;&lt;i>[a]&lt;/i>&quot; und &quot;&lt;i>[b]&lt;/i>&quot;.</translation>
    </message>
    <message>
      <source>POSIX Basic Regular Expressions</source>
      <translation>Reguläre Basis-Ausdrücke nach POSIX</translation>
    </message>
    <message>
      <source>Use wildcard instead regex</source>
      <translation>Wildcard anstelle von Regex benutzen</translation>
    </message>
    <message>
      <source>Use wildcard expression instead regular expression</source>
      <translation>Wildcard-Ausdruck anstelle regulärem Ausdruck benutzen</translation>
    </message>
    <message>
      <source>lib7zip not found</source>
      <translation>lib7zip nicht gefunden</translation>
    </message>
    <message>
      <source>unknown</source>
      <translation>unbekannt</translation>
    </message>
    <message>
      <source>support not compiled in</source>
      <translation>Unterstützung nicht einkompiliert</translation>
    </message>
    <message>
      <source>mediainfo not supported</source>
      <translation>mediainfo nicht unterstützt</translation>
    </message>
    <message>
      <source>Supported image extensions found: </source>
      <translation>Unterstützte Bildendungen gefunden:</translation>
    </message>
    <message>
      <source>Matches any single character (many applications exclude &lt;a href=&quot;http://en.wikipedia.org/wiki/Newline&quot; title=&quot;Newline&quot;>newlines&lt;/a>, and exactly which characters are considered newlines is flavor-, character-encoding-, and platform-specific, but it is safe to assume that the line feed character is included). Within POSIX bracket expressions, the dot character matches a literal dot. For example, &lt;code>a.c&lt;/code> matches &quot;&lt;i>abc&lt;/i>&quot;, etc., but &lt;code>[a.c]&lt;/code> matches only &quot;&lt;i>a&lt;/i>&quot;, &quot;&lt;i>.&lt;/i>&quot;, or &quot;&lt;i>c&lt;/i>&quot;.</source>
      <translation>Passt auf ein einzelnes Zeichen (viele Anwendungen schließen &lt;a href=&quot;http://de.wikipedia.org/wiki/Newline&quot; title=&quot;Newline&quot;>Zeilenende&lt;/a> aus. Welche Zeichen exakt als Zeilenende und welche Zeichen als Zeilenende aufgefasst werden, hängt von der Variante, Zeichenkodierung und Plattform ab. Aber es ist sicher, dass das Zeilenvorschubzeichen enthalten ist). Mit den POSIX-Klammerausdrücken passt das Punktzeichen als Zeichen Punkt. Zum Beispiel &lt;code>a.c&lt;/code> passt auf &quot;&lt;i>abc&lt;/i>&quot;, etc., aber &lt;code>[a.c]&lt;/code> passt nur auf &quot;&lt;i>a&lt;/i>&quot;, &quot;&lt;i>.&lt;/i>&quot; oder &quot;&lt;i>c&lt;/i>&quot;.</translation>
    </message>
    <message>
      <source>&lt;a href=&quot;http://en.wikipedia.org/wiki/Regex&quot;>regular expressions&lt;/a></source>
      <translation>&lt;a href=&quot;http://de.wikipedia.org/wiki/Regex&quot;>reguläre Ausdrücke&lt;/a></translation>
    </message>
    <message>
      <source>From</source>
      <translation>Von</translation>
    </message>
  </context>
  <context>
    <name>ShowContent</name>
    <message>
      <source>Content of %1</source>
      <translation>Inhalt von %1</translation>
    </message>
    <message>
      <source>Close</source>
      <translation>Schliessen</translation>
    </message>
    <message>
      <source>Confirmation</source>
      <translation>Bestätigung</translation>
    </message>
    <message>
      <source>Yes</source>
      <translation>Ja</translation>
    </message>
    <message>
      <source>No</source>
      <translation>Nein</translation>
    </message>
    <message>
      <source>Remove the file content from the database. (Warning: Unrecoverable!)</source>
      <translation>Den Inhalt der Datei aus der Datenbank löschen (Warnung: Nicht wiederherstellbar!)</translation>
    </message>
    <message>
      <source>%1 kByte</source>
      <translation>%1 KByte</translation>
    </message>
    <message>
      <source>Save this content to a new file</source>
      <translation>Diesen Inhalt in eine neue Datei speichern</translation>
    </message>
    <message>
      <source>Select a filename below</source>
      <translation>Dateiname unten auswählen</translation>
    </message>
    <message>
      <source>I can't rewrite the file: %1</source>
      <translation>Datei kann nicht überschrieben werden: %1</translation>
    </message>
    <message>
      <source>Error while saving...</source>
      <translation>Fehler beim Speichern...</translation>
    </message>
    <message>
      <source>Sure to delete this file's content from the database?</source>
      <translation>Soll dieser Inhalt der Datei wirklich von der Datenbank gelöscht werden?</translation>
    </message>
    <message>
      <source>CdCat databases (*.hcf )</source>
      <translation>CdCat-Datenbanken (*.hcf)</translation>
    </message>
    <message>
      <source>Category of %1</source>
      <translation>Kategorie von %1</translation>
    </message>
    <message>
      <source>Set category</source>
      <translation>Kategorie setzen</translation>
    </message>
    <message>
      <source>Set content</source>
      <translation>Inhalt setzen</translation>
    </message>
    <message>
      <source>select font for display</source>
      <translation>Schriftart zum Anzeigen auswählen</translation>
    </message>
  </context>
  <context>
    <name>addDialog</name>
    <message>
      <source>Directory Browser</source>
      <translation>Verzeichnisbrowser</translation>
    </message>
    <message>
      <source>Add Media to the Database</source>
      <translation>Medium zur Datenbank hinzufügen</translation>
    </message>
    <message>
      <source>Choose a directory to scan:</source>
      <translation>Verzeichnis zum Durchsuchen auswählen:</translation>
    </message>
    <message>
      <source>CD</source>
      <translation>CD</translation>
    </message>
    <message>
      <source>DVD</source>
      <translation>DVD</translation>
    </message>
    <message>
      <source>HardDisc</source>
      <translation>Festplatte</translation>
    </message>
    <message>
      <source>Floppy</source>
      <translation>Diskette</translation>
    </message>
    <message>
      <source>NetworkPlace</source>
      <translation>Netzwerkgerät</translation>
    </message>
    <message>
      <source>FlashDrive</source>
      <translation>Flashspeicher</translation>
    </message>
    <message>
      <source>OtherDevice</source>
      <translation>Anderes Gerät</translation>
    </message>
    <message>
      <source>Error:</source>
      <translation>Fehler:</translation>
    </message>
    <message>
      <source>You must be fill the &quot;Name&quot; field!</source>
      <translation>Es muss das Feld &quot;Name&quot; ausgefüllt werden!</translation>
    </message>
    <message>
      <source>New Disk %1</source>
      <translation>Neue CD %1</translation>
    </message>
    <message>
      <source>The Value of Serial Number must be unique! Please change it!</source>
      <translation>Die Seriennummer muss einzigartig sein! Bitte den Wert ändern!</translation>
    </message>
    <message>
      <source>You haven't selected a directory! Please select one!</source>
      <translation>Es ist kein Verzeichnis ausgewählt! Bitte ein Verzeichnis auswählen!</translation>
    </message>
    <message>
      <source>The media name can't begin with the &quot;@&quot; character!</source>
      <translation>Der Medienname kann nicht mit dem \&quot;@\&quot;-Zeichen beginnen!</translation>
    </message>
    <message>
      <source>Media &amp;Name:</source>
      <translation>Medien&amp;name:</translation>
    </message>
    <message>
      <source>S&amp;erial number of Media:</source>
      <translation>S&amp;eriennumber des Mediums:</translation>
    </message>
    <message>
      <source>&amp;Owner:</source>
      <translation>&amp;Eigentümer:</translation>
    </message>
    <message>
      <source>C&amp;ategory:</source>
      <translation>K&amp;ategorie:</translation>
    </message>
    <message>
      <source>&amp;Type:</source>
      <translation>&amp;Typ:</translation>
    </message>
    <message>
      <source>Co&amp;mment:</source>
      <translation>Ko&amp;mmentar:</translation>
    </message>
    <message>
      <source>&amp;Cancel</source>
      <translation>&amp;Abbrechen</translation>
    </message>
    <message>
      <source>&amp;Scan</source>
      <translation>&amp;Durchsuchen</translation>
    </message>
    <message>
      <source>Select &amp;readable items</source>
      <translation>&amp;Lesbare Elemente auswählen</translation>
    </message>
    <message>
      <source>detect CDROM/DVD med&amp;ia name after mount</source>
      <translation>CDROM/DVD-Med&amp;ienname nach dem Einbinden erkennen</translation>
    </message>
    <message>
      <source>Enter media name...</source>
      <translation>Medienname eingeben...</translation>
    </message>
    <message>
      <source>The Media Name must be unique! Enter new media name:</source>
      <translation>Der Medienname muß eindeutig sein! Bitte neuen Medienname eingeben:</translation>
    </message>
    <message>
      <source>unknown</source>
      <translation>unbekannt</translation>
    </message>
  </context>
  <context>
    <name>borrowDialog</name>
    <message>
      <source>Ok</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>I borrow the &quot;%1&quot; named media to:</source>
      <translation>Medium &quot;%1&quot; ausgeliehen an:</translation>
    </message>
    <message>
      <source>unknown</source>
      <translation>unbekannt</translation>
    </message>
    <message>
      <source>I borrow the &quot;&quot; named media to:</source>
      <translation>Medium &quot;&quot; ausgeliehen an:</translation>
    </message>
    <message>
      <source>Borrowing...</source>
      <translation>Ausleihen...</translation>
    </message>
  </context>
  <context>
    <name>borrowingDialog</name>
    <message>
      <source>Media borrowing info:</source>
      <translation>Medium-Ausleihinfo:</translation>
    </message>
    <message>
      <source>Media</source>
      <translation>Medium</translation>
    </message>
    <message>
      <source>Borrowed</source>
      <translation>Ausgeliehen</translation>
    </message>
    <message>
      <source>where is it now?</source>
      <translation>Wo ist es jetzt?</translation>
    </message>
    <message>
      <source>Ok</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>No</source>
      <translation>Nein</translation>
    </message>
    <message>
      <source>Yes</source>
      <translation>Ja</translation>
    </message>
    <message>
      <source>unknown</source>
      <translation>unbekannt</translation>
    </message>
    <message>
      <source>Error</source>
      <translation>Fehler</translation>
    </message>
    <message>
      <source>Set &quot;Yes&quot; or &quot;No&quot; !</source>
      <translation>&quot;Ja&quot; oder &quot;Nein&quot;!</translation>
    </message>
    <message>
      <source>Warning</source>
      <translation>Warnung</translation>
    </message>
    <message>
      <source>Do you save the changes?</source>
      <translation>Sollen die Änderungen gespeichert werden?</translation>
    </message>
    <message>
      <source>Are you sure want to clear all borrow?</source>
      <translation>Sollen wirklich alle Ausleihen gelöscht werden?</translation>
    </message>
    <message>
      <source>I got it back!</source>
      <translation>I bekam  es zurück!</translation>
    </message>
    <message>
      <source>&lt;&lt; </source>
      <translation>&lt;&lt; </translation>
    </message>
    <message>
      <source>Borrowing info...</source>
      <translation>Aufnahme der Information...</translation>
    </message>
    <message>
      <source>Show only borrowed items</source>
      <translation>Nur ausgeliehene Elemente anzeigen</translation>
    </message>
    <message>
      <source>Clear all borrowing info</source>
      <translation>Alle Ausleihinformationen löschen</translation>
    </message>
  </context>
  <context>
    <name>commentEdit</name>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>OK</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>Edit comment of</source>
      <translation>Kommentar bearbeiten von</translation>
    </message>
    <message>
      <source>Edit category of</source>
      <translation>Kategorie bearbeiten von</translation>
    </message>
  </context>
  <context>
    <name>exportCdcatDB</name>
    <message>
      <source>All media</source>
      <translation>Alle Medien</translation>
    </message>
    <message>
      <source>...</source>
      <translation>...</translation>
    </message>
    <message>
      <source>&amp;OK</source>
      <translation>&amp;OK</translation>
    </message>
    <message>
      <source>Availiable media</source>
      <translation>Verfügbare Medien</translation>
    </message>
    <message>
      <source>Media to export</source>
      <translation>Medien zum Exportieren</translation>
    </message>
    <message>
      <source>separator:</source>
      <translation>Trennzeichen:</translation>
    </message>
    <message>
      <source>File to export:</source>
      <translation>Datei zum Exportieren:</translation>
    </message>
    <message>
      <source>Media</source>
      <translation>Medium</translation>
    </message>
    <message>
      <source>File</source>
      <translation>Datei</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>Grösse</translation>
    </message>
    <message>
      <source>Date</source>
      <translation>Datum</translation>
    </message>
    <message>
      <source>Filename missing</source>
      <translation>Dateiname fehlt</translation>
    </message>
    <message>
      <source>Please enter a filename!</source>
      <translation>Bitte einen Dateinamen eingeben!</translation>
    </message>
    <message>
      <source>Separator missing</source>
      <translation>Trennzeichen fehlt</translation>
    </message>
    <message>
      <source>Please enter a separator!</source>
      <translation>Bitte ein Trennzeichen eingeben!</translation>
    </message>
    <message>
      <source>All files (*.*)</source>
      <translation>Alle Dateien (*.*)</translation>
    </message>
    <message>
      <source>Choose a file!</source>
      <translation>Datei auswählen!</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
    <message>
      <source>export as HTML</source>
      <translation>Als HTML exportieren</translation>
    </message>
    <message>
      <source>export as CSV</source>
      <translation>Als CSV-Datei exportieren</translation>
    </message>
    <message>
      <source>Add media</source>
      <translation>Medium hinzufügen</translation>
    </message>
    <message>
      <source>Remove media</source>
      <translation>Medium löschen</translation>
    </message>
    <message>
      <source>Export from cdcat:</source>
      <translation>Export von cdcat:</translation>
    </message>
    <message>
      <source>I can't create or rewrite the file</source>
      <translation>Datei kann nicht erstellt oder überschrieben werden</translation>
    </message>
    <message>
      <source>Export database...</source>
      <translation>Datenbank exportieren...</translation>
    </message>
    <message>
      <source>What to export</source>
      <translation>Was soll exportiert werden</translation>
    </message>
    <message>
      <source>Media name</source>
      <translation>Medienname</translation>
    </message>
    <message>
      <source>Path</source>
      <translation>Pfad</translation>
    </message>
    <message>
      <source>Comment</source>
      <translation>Kommentar</translation>
    </message>
    <message>
      <source>HTML headline</source>
      <translation>HTML-Überschrift</translation>
    </message>
    <message>
      <source>Overwrite?</source>
      <translation>Überschreiben?</translation>
    </message>
    <message>
      <source>Do you want overwrite this file?</source>
      <translation>Soll diese Datei überschrieben werden?</translation>
    </message>
    <message>
      <source>Yes</source>
      <translation>Ja</translation>
    </message>
    <message>
      <source>Discard</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>Export CdCat database: </source>
      <translation>CdCat-Datenbank exportieren:</translation>
    </message>
    <message>
      <source>Export from cdcat, catalog:</source>
      <translation>Export von cdcat, Katalog:</translation>
    </message>
    <message>
      <source>Generated at:</source>
      <translation>Generiert am:</translation>
    </message>
    <message>
      <source>Media number</source>
      <translation>Mediennummer</translation>
    </message>
    <message>
      <source>export mp3 tag</source>
      <translation>MP3-Tag exportieren</translation>
    </message>
    <message>
      <source>export borrow information</source>
      <translation>Ausleihinformation exportieren</translation>
    </message>
    <message>
      <source>#</source>
      <translation>#</translation>
    </message>
    <message>
      <source>MP3 tag</source>
      <translation>MP3-Tag</translation>
    </message>
    <message>
      <source>Borrow</source>
      <translation>Ausgeliehen</translation>
    </message>
    <message>
      <source>Artist:</source>
      <translation>Künstler:</translation>
    </message>
    <message>
      <source>Title:</source>
      <translation>Titel:</translation>
    </message>
    <message>
      <source>Album:</source>
      <translation>Album:</translation>
    </message>
    <message>
      <source>Year:</source>
      <translation>Jahr:</translation>
    </message>
    <message>
      <source>table header/comment line</source>
      <translation>Tabellenkopf/Kommentarzeile</translation>
    </message>
    <message>
      <source>field list</source>
      <translation>Feldliste</translation>
    </message>
    <message>
      <source>export as XML</source>
      <translation>Als XML exportieren</translation>
    </message>
    <message>
      <source>Exporting, please wait...</source>
      <translation>Es wird exportiert, bitte warten...</translation>
    </message>
    <message>
      <source>Error during write export...</source>
      <translation>Fehler beim exportieren...</translation>
    </message>
    <message>
      <source>Category</source>
      <translation>Kategorie</translation>
    </message>
    <message>
      <source>Only media</source>
      <translation>Nur Medium</translation>
    </message>
    <message>
      <source>&amp;Cancel</source>
      <translation>&amp;Abbrechen</translation>
    </message>
  </context>
  <context>
    <name>findDialog</name>
    <message>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>Type</source>
      <translation>Typ</translation>
    </message>
    <message>
      <source>Media</source>
      <translation>Medium</translation>
    </message>
    <message>
      <source>Path</source>
      <translation>Pfad</translation>
    </message>
    <message>
      <source>Modification</source>
      <translation>Änderung</translation>
    </message>
    <message>
      <source>Search in the database...</source>
      <translation>In Datenbank suchen...</translation>
    </message>
    <message>
      <source>Close / Go to selected</source>
      <translation>Schliessen/ Zu ausgewähltem gehen</translation>
    </message>
    <message>
      <source>Find:</source>
      <translation>Finden:</translation>
    </message>
    <message>
      <source>Seek in:</source>
      <translation>Suchen in:</translation>
    </message>
    <message>
      <source>Owner:</source>
      <translation>Eigentümer:</translation>
    </message>
    <message>
      <source>Comment</source>
      <translation>Kommentar</translation>
    </message>
    <message>
      <source>File name</source>
      <translation>Dateiname</translation>
    </message>
    <message>
      <source>mp3-tag Comment</source>
      <translation>MP3-Tag-Kommentar</translation>
    </message>
    <message>
      <source>mp3-tag Album</source>
      <translation>MP3-Tag-Album</translation>
    </message>
    <message>
      <source>mp3-tag Title</source>
      <translation>MP3-Tag-Titel</translation>
    </message>
    <message>
      <source>mp3-tag Artist</source>
      <translation>MP3-Tag-Künstler</translation>
    </message>
    <message>
      <source>Case sensitive</source>
      <translation>Gross-/Kleinschreibung beachten</translation>
    </message>
    <message>
      <source>Use easy matching instead regex</source>
      <translation>Einfach passenden regulären Ausdruck benutzen</translation>
    </message>
    <message>
      <source>All/Everybody</source>
      <translation>Alle/Jeder</translation>
    </message>
    <message>
      <source>All media</source>
      <translation>Alle Medien</translation>
    </message>
    <message>
      <source>&amp;Cancel</source>
      <translation>&amp;Abbrechen</translation>
    </message>
    <message>
      <source>Content</source>
      <translation>Inhalt</translation>
    </message>
    <message>
      <source>Media / Directory name</source>
      <translation>Medium / Verzeichnisname</translation>
    </message>
    <message>
      <source>Date start</source>
      <translation>Datum Start:</translation>
    </message>
    <message>
      <source>Date end</source>
      <translation>Datum Ende:</translation>
    </message>
    <message>
      <source>Byte</source>
      <translation>Byte</translation>
    </message>
    <message>
      <source>KiB</source>
      <translation>KiB</translation>
    </message>
    <message>
      <source>MiB</source>
      <translation>MiB</translation>
    </message>
    <message>
      <source>GiB</source>
      <translation>GiB</translation>
    </message>
    <message>
      <source>Min size</source>
      <translation>Minmale Größe</translation>
    </message>
    <message>
      <source>Max size</source>
      <translation>Maximale Größe</translation>
    </message>
    <message>
      <source>&amp;Start search</source>
      <translation>&amp;Suche starten</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>Grösse</translation>
    </message>
    <message>
      <source>Unsharp search (slow)</source>
      <translation>Unscharfe Suche (langsam)</translation>
    </message>
    <message>
      <source>Find in archives too</source>
      <translation>Auch Dateien in Archiven finden</translation>
    </message>
    <message>
      <source>Print result...</source>
      <translation>Ergebnis drucken...</translation>
    </message>
    <message>
      <source>Export result to HTML...</source>
      <translation>Ergebnis nach HTML exportieren...</translation>
    </message>
    <message>
      <source>Print cdcat result</source>
      <translation>Cdcat-Ergebnis drucken</translation>
    </message>
    <message>
      <source>Result file name</source>
      <translation>Ergebnis-Dateiname</translation>
    </message>
    <message>
      <source>Filename missing</source>
      <translation>Dateiname fehlt</translation>
    </message>
    <message>
      <source>Please enter a filename!</source>
      <translation>Bitte einen Dateinamen eingeben!</translation>
    </message>
    <message>
      <source>Cdcat search result</source>
      <translation>Cdcat-Suchergebnis</translation>
    </message>
    <message>
      <source>catalog</source>
      <translation>Katalog</translation>
    </message>
    <message>
      <source>created at:</source>
      <translation>erstellt am:</translation>
    </message>
    <message>
      <source>used search options</source>
      <translation>benutzte Suchoptionen</translation>
    </message>
    <message>
      <source>search pattern:</source>
      <translation>Suchmuster:</translation>
    </message>
    <message>
      <source>on</source>
      <translation>an</translation>
    </message>
    <message>
      <source>off</source>
      <translation>aus</translation>
    </message>
    <message>
      <source>Search in</source>
      <translation>Suchen in</translation>
    </message>
    <message>
      <source>archives</source>
      <translation>Archiven</translation>
    </message>
    <message>
      <source>other options</source>
      <translation>andere Optionen</translation>
    </message>
    <message>
      <source>File cant open: </source>
      <translation>Datei kann nicht geöffnet werden:</translation>
    </message>
    <message>
      <source>Cant open file</source>
      <translation>Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
      <source>Search for duplicates in the database...</source>
      <translation>Nach Duplikaten in der Datenbank suchen...</translation>
    </message>
    <message>
      <source>Duplicates for:</source>
      <translation>Duplikate für:</translation>
    </message>
    <message>
      <source>Close</source>
      <translation>Schliessen</translation>
    </message>
    <message>
      <source>Results: search not started</source>
      <translation>Ergebnisse: Suche nicht gestartet</translation>
    </message>
    <message>
      <source>Extension</source>
      <translation>Erweiterung</translation>
    </message>
    <message>
      <source>TiB</source>
      <translation>TiB</translation>
    </message>
    <message>
      <source>Keep search result</source>
      <translation>Suchergebnis behalten</translation>
    </message>
    <message>
      <source>Clear search results</source>
      <translation>Suchergebnisse löschen</translation>
    </message>
    <message>
      <source>Category</source>
      <translation>Kategorie</translation>
    </message>
    <message>
      <source>Category for find</source>
      <translation>Kategorie zum Finden</translation>
    </message>
    <message>
      <source>Selected dir: </source>
      <translation>Gewähltes Verzeichnis:</translation>
    </message>
  </context>
  <context>
    <name>helpDialog</name>
    <message>
      <source>help</source>
      <translation>Hilfe</translation>
    </message>
    <message>
      <source>close</source>
      <translation>Schliessen</translation>
    </message>
    <message>
      <source>&lt;p align=&quot;center&quot;>&lt;font size=&quot;+1&quot;>&lt;b>Help&lt;/b>&lt;/font>&lt;/p>&lt;br>

&lt;b>What's this?&lt;/b>&lt;br>
&lt;blockquote>
The cdcat is graphical (QT based) multiplatform (Linux/Windows) catalog program which scans the directories/drives you want and memorize the filesystem including the tags of mp3's and other data and store it in a small file.
The database is stored in a gzipped XML format, so you can hack it, or use it if necessary :-)
And the program can store the content of some specified files up to a limit size if you want. (for example: *.nfo)
&lt;/blockquote>
&lt;br>
&lt;b>Usage:&lt;/b>&lt;br>
&lt;blockquote>
&lt;i>Create a new catalog&lt;/i>: Run the &lt;tt>New&lt;/tt> command in the catalog menu. You have to type the name of the new catalog. You can specify the default username of the media(which you scan later), and add a comment to the catalog.&lt;br>Before the scanning select the necessary readable components in the config dialog, which can be mp3 tags content of some files or etc.  If you done this, you can scan one of your media with &lt;tt>Add media&lt;/tt>command in the Edit menu. In that dialog you have to specyfi the directory/or drive the media you want to add. It's recommended to specify the name and the number of the media which has to be unique. (The program always generate one identical name and number) You can label the media to a owner, if necessary.
&lt;br>If you scanned your media, you will be able to browse in it with the browser window (like mc) , or search in it. You can save the catalog with &lt;tt>save as&lt;/tt> command in the Catalog menu.
&lt;br>
&lt;br>
&lt;i>Open an existing catalog:&lt;/i>Run the &lt;tt>open&lt;/tt> command in the Catalog menu, and choice the file of the catalog. (*.hcf)  After the opening you will be able browse the catalog or search in it.
&lt;br>
&lt;/blockquote>
&lt;br>
&lt;b>Author:&lt;/b>
&lt;blockquote>The program was written by  Peter Deak  (hungary)&lt;br>
E-mail: hyperr@freemail.hu
&lt;br>
&lt;/blockquote>
&lt;br>
&lt;b>License:&lt;/b>
&lt;blockquote>General Public License (GPL)&lt;/blockquote>
&lt;br>
&lt;b>Homepage:&lt;/b>
&lt;blockquote>You can read about the program and get new versions, sources etc, in the hompage of cdcat:&lt;br>&lt;tt>http://cdcat.sourceforge.net&lt;/tt>&lt;/blockquote></source>
      <translation>&lt;p align=&quot;center&quot;>&lt;font size=&quot;+1&quot;>&lt;b>Hilfe&lt;/b>&lt;/font>&lt;/p>&lt;br>

&lt;b>Was ist das?&lt;/b>&lt;br>
&lt;blockquote>CdCat ist ein graphisches (QT-basierendes) Multiplattform-Katalogprogramm (Linux/Windows), welches Verzeichnisse/Laufwerke durchsuchen und das Dateisystem inklusive Tags von MP3-Dateien in ener kleinen Datei speichern kann. Die Datenbank wird ein einer gepackten XML-Datei gespeichert, so dass man sie benutzen oder hacken kann ;)
&lt;/blockquote>
&lt;br>
&lt;b>Benutzung&lt;/b>
&lt;blockquote>
&lt;i>Einen neuen Katalog erstellen&lt;/i>:Das &lt;tt>Neu&lt;/tt>-Kommando aus dem Katalogmenü wählen. Dann muss der Name des neuen Katalogs eingegeben werden. Es kann der Standardbenutzer des Mediums (welches später duchsucht wird) angegeben und ein Kommentar zum Katalog hinzugefügt werden.&lt;br>Wenn dies abgeschlossen ist, kann das Medium mit dem &lt;tt>Medium hinzufügen &lt;/tt>-Kommando aus dem Bearbeitenmenü hinzugefügt werden. In diesem Dialog muss muss das Verzeichnis/oder Laufwerk des Mediums, das benutzt werden soll, angegeben werden. Es wird empfohlen, den Namen und die Nummer des Medium, welche einzigartig sein müssen, anzugeben (Das Programm generiert immer identischen Namen und Nummer). Es kann das Medium, wenn erforderlich,  einem Benutzer zugeordnet werden.
&lt;br>Wenn das Medium durchsucht wird, kann man im Browserfenster suchen (wie beim mc), oder das Medium kann durchsucht werdenn. Der Katalog kann mit dem &lt;tt>Speichern&lt;/tt>-Kommando aus dem Katalogmenü gespeichert werden.
&lt;br>
&lt;br>
&lt;i>Einen existierenden Katalog öffnen:&lt;/i>Das &lt;tt>Öffnen&lt;/tt>-Kommando aus dem Katalogmenü wählen und die Datei des Katalogs auswählen (*.hcf) Nach dem Öffnen kann man dann im Katalog blättern oder ihn durchsuchen.
&lt;br>
&lt;/blockquote>
&lt;br>
&lt;b>Autor&lt;/b>
&lt;blockquote>Das Programm wurde von Peter Deak geschrieben (Ungarn)&lt;br>
E-mail: hyperr@freemail.hu
&lt;br>
&lt;/blockquote>
&lt;br>
&lt;b>Lizenz:&lt;/b>
&lt;blockquote>General Public License (GPL)&lt;/blockquote>
&lt;br>
&lt;b>Homepage:&lt;/b>
&lt;blockquote>Mehr Infos über das Programm und neue Versionen können von der Homepage von 
 cdcat:&lt;br>&lt;tt>http://cdcat.sourceforge.net&lt;/tt> bekommen werden. &lt;/blockquote></translation>
    </message>
    <message>
      <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;>
&lt;html>&lt;head>&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; />&lt;style type=&quot;text/css&quot;>
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body style=&quot; font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;&quot;>
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:40px; margin-right:40px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;/p>&lt;/body>&lt;/html></source>
      <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;>
&lt;html>&lt;head>&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; />&lt;style type=&quot;text/css&quot;>
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body style=&quot; font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;&quot;>
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:40px; margin-right:40px; -qt-block-indent:0; text-indent:0px;&quot;>&lt;/p>&lt;/body>&lt;/html></translation>
    </message>
  </context>
  <context>
    <name>importGtktalogCsv</name>
    <message>
      <source>Importing CSV...</source>
      <translation>CSV wird importiert...</translation>
    </message>
    <message>
      <source>Import was successful.</source>
      <translation>Import war erfolgreich.</translation>
    </message>
    <message>
      <source>1 media</source>
      <translation>1 Medium</translation>
    </message>
    <message>
      <source>media</source>
      <translation>Medien</translation>
    </message>
    <message>
      <source>1 directory:,
</source>
      <translation>1 Verzeichnis:,
</translation>
    </message>
    <message>
      <source>directories</source>
      <translation>Verzeichnisse</translation>
    </message>
    <message>
      <source>1 File</source>
      <translation>1 Datei</translation>
    </message>
    <message>
      <source>files</source>
      <translation>Dateien</translation>
    </message>
    <message>
      <source>are imported.</source>
      <translation>wurden importiert.</translation>
    </message>
    <message>
      <source>Import successful</source>
      <translation>Import erfolgreich</translation>
    </message>
    <message>
      <source>file read error</source>
      <translation>Dateilesefehler</translation>
    </message>
    <message>
      <source>Could not read file</source>
      <translation>Datei kann nicht gelesen werden</translation>
    </message>
    <message>
      <source>Fatal error</source>
      <translation>Fataler Fehler</translation>
    </message>
    <message>
      <source>Fatal error occured.</source>
      <translation>Es ist ein fataler Fehler aufgetreten.</translation>
    </message>
    <message>
      <source>file open error</source>
      <translation>Datei öffnen-Fehler</translation>
    </message>
    <message>
      <source>Could not open file</source>
      <translation>Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
      <source>importuser</source>
      <translation>Importbenutzer</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>tag: </source>
      <translation>Tag: </translation>
    </message>
  </context>
  <context>
    <name>importGtktalogXml</name>
    <message>
      <source>Importing XML...</source>
      <translation>XML wird importiert...</translation>
    </message>
    <message>
      <source>XML import was successful.</source>
      <translation>XML-Import war erfolgreich.</translation>
    </message>
    <message>
      <source>1 media</source>
      <translation>1 Medium</translation>
    </message>
    <message>
      <source>media</source>
      <translation>Medien</translation>
    </message>
    <message>
      <source>1 directory:,
</source>
      <translation>1 Verzeichnis:,
</translation>
    </message>
    <message>
      <source>directories</source>
      <translation>Verzeichnisse</translation>
    </message>
    <message>
      <source>1 File</source>
      <translation>1 Datei</translation>
    </message>
    <message>
      <source>files</source>
      <translation>Dateien</translation>
    </message>
    <message>
      <source>are imported.</source>
      <translation>wurden importiert.</translation>
    </message>
    <message>
      <source>Import successful</source>
      <translation>Import erfolgreich</translation>
    </message>
    <message>
      <source>parse error</source>
      <translation>Parserfehler</translation>
    </message>
    <message>
      <source>error during parsing</source>
      <translation>Fehler während des Parsens</translation>
    </message>
    <message>
      <source>importuser</source>
      <translation>Importbenutzer</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
  </context>
  <context>
    <name>importWhereIsItXml</name>
    <message>
      <source>Importing XML...</source>
      <translation>XML wird importiert...</translation>
    </message>
    <message>
      <source>Importing XML from WhereIsIt was successful.</source>
      <translation>Importieren XML von WhereIsIt war erfolgreich.</translation>
    </message>
    <message>
      <source>1 media</source>
      <translation>1 Medium</translation>
    </message>
    <message>
      <source>media</source>
      <translation>Medien</translation>
    </message>
    <message>
      <source>1 directory:,
</source>
      <translation>1 Verzeichnis:,
</translation>
    </message>
    <message>
      <source>directories</source>
      <translation>Verzeichnisse</translation>
    </message>
    <message>
      <source>1 File</source>
      <translation>1 Datei</translation>
    </message>
    <message>
      <source>files</source>
      <translation>Dateien</translation>
    </message>
    <message>
      <source>are imported.</source>
      <translation>wurden importiert.</translation>
    </message>
    <message>
      <source>Import successful</source>
      <translation>Import erfolgreich</translation>
    </message>
    <message>
      <source>parse error</source>
      <translation>Parserfehler</translation>
    </message>
    <message>
      <source>error during parsing</source>
      <translation>Fehler während des Parsens</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>tag:</source>
      <translation>Tag:</translation>
    </message>
  </context>
  <context>
    <name>newdbdialog</name>
    <message>
      <source>DataBase's Properties</source>
      <translation>Datenbank-Eigenschaften</translation>
    </message>
    <message>
      <source>DataBase Name:</source>
      <translation>Datenbank-Name:</translation>
    </message>
    <message>
      <source>DataBase Owner:</source>
      <translation>Datenbank-Eigentümer:</translation>
    </message>
    <message>
      <source>Comment:</source>
      <translation>Kommentar:</translation>
    </message>
    <message>
      <source>Catalog</source>
      <translation>Katalog</translation>
    </message>
    <message>
      <source>hcat-user</source>
      <translation>hcat-Benutzer</translation>
    </message>
    <message>
      <source>OK</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>Category:</source>
      <translation>Kategorie:</translation>
    </message>
    <message>
      <source>encrypt catalog</source>
      <translation>Katalog verschlüsseln</translation>
    </message>
    <message>
      <source>password for catalog</source>
      <translation>Passwort für Katalog</translation>
    </message>
    <message>
      <source>Password length is too short, must be minimum 4 chars</source>
      <translation>Passwortlänge ist zu kurz, muss mindestens 4 Zeichen lang sein</translation>
    </message>
    <message>
      <source>Password too short</source>
      <translation>Passwort zu kurz</translation>
    </message>
    <message>
      <source>Password length is too big, must be maximal %1 chars</source>
      <translation>Passwortlänge ist zu lang, darf maximal %1 Zeichen lang sein</translation>
    </message>
    <message>
      <source>Password too big</source>
      <translation>Passwort zu lang</translation>
    </message>
    <message>
      <source></source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>renamedialog</name>
    <message>
      <source>Rename node...</source>
      <translation>Knoten umbenennen...</translation>
    </message>
    <message>
      <source>Give the new name:</source>
      <translation>Neuer Name:</translation>
    </message>
    <message>
      <source>OK</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>Error:</source>
      <translation>Fehler:</translation>
    </message>
    <message>
      <source>The new (media) name must be unique!</source>
      <translation>Der neue (Medien-)Name muss einzigartig sein!</translation>
    </message>
    <message>
      <source>The new media name can't starts with &quot;@&quot; !</source>
      <translation>Der Medienname kann nicht mit \&quot;@\&quot; beginnen!</translation>
    </message>
  </context>
  <context>
    <name>renumberdialog</name>
    <message>
      <source>Error:</source>
      <translation>Fehler:</translation>
    </message>
    <message>
      <source>The element is NOT a Media!</source>
      <translation>Das Element ist kein Medium!</translation>
    </message>
    <message>
      <source>Re-Number media...</source>
      <translation>Medium umnummerieren...</translation>
    </message>
    <message>
      <source>Give the new serial number:</source>
      <translation>Neue Serienummer eingeben:</translation>
    </message>
    <message>
      <source>OK</source>
      <translation>OK</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <source>The new media-number must be unique!</source>
      <translation>Die Mediennummer muss einzigartig sein!</translation>
    </message>
    <message>
      <source>Please enter number value!</source>
      <translation>Bitte einen Zahlenwert eingeben!</translation>
    </message>
  </context>
  <context>
    <name>seekEngine</name>
    <message>
      <source>Error in the pattern:</source>
      <translation>Fehler im Suchmuster:</translation>
    </message>
    <message>
      <source>dir</source>
      <translation>Verzeichnis</translation>
    </message>
    <message>
      <source>file</source>
      <translation>Datei</translation>
    </message>
    <message>
      <source>error</source>
      <translation>Fehler</translation>
    </message>
    <message>
      <source>There is no matching.</source>
      <translation>Keine Übereinstimmung gefunden</translation>
    </message>
    <message>
      <source>media</source>
      <translation>Medien</translation>
    </message>
    <message>
      <source>Searching, please wait...</source>
      <translation>Es wird gesucht, bitte warten...</translation>
    </message>
    <message>
      <source>Search cancelled</source>
      <translation>Suche abgebrochen</translation>
    </message>
    <message>
      <source>You have cancelled searching.</source>
      <translation>Sie haben die Suche abgebrochen.</translation>
    </message>
    <message>
      <source>File in archive: </source>
      <translation>Datei in Archiv:</translation>
    </message>
    <message>
      <source>Results:</source>
      <translation>Ergebnisse:</translation>
    </message>
    <message>
      <source>searching is in progress</source>
      <translation>Suche läuft</translation>
    </message>
    <message>
      <source>Last search results:</source>
      <translation>Letzte Suchergebnisse:</translation>
    </message>
  </context>
</TS>
