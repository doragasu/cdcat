<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="fr_FR">
<context>
    <name></name>
    <message>
        <location filename="../dbase.cpp" line="185"/>
        <source>Not available</source>
        <translation>Indisponible</translation>
    </message>
</context>
<context>
    <name>AddLnk</name>
    <message>
        <location filename="../adddialog.cpp" line="763"/>
        <source>Add a &quot;Catalog Link&quot; to the database</source>
        <translation>Ajouter un &quot;Lien Catalogue&quot; à la base</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="764"/>
        <source>Location of the .hcf file:</source>
        <translation>Emplacement du fichier .hcf :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="766"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="767"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="784"/>
        <source>CdCat databases (*.hcf )</source>
        <translation>Bases de données CdCat (*.hcf)</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="784"/>
        <source>Open a file...</source>
        <translation>Ouvrir un fichier...</translation>
    </message>
</context>
<context>
    <name>ArchiveFile</name>
    <message>
        <location filename="../dbase.h" line="283"/>
        <source>unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../dbase.h" line="283"/>
        <source></source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CatalogTypeEditDialog</name>
    <message>
        <location filename="../guibase.cpp" line="3138"/>
        <source>Change media type</source>
        <translation>Changer le type de média</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3139"/>
        <source>Change type of media</source>
        <translation>Changer le type de média</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3141"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3142"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3143"/>
        <source>HardDisc</source>
        <translation>Disque dur</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3144"/>
        <source>Floppy</source>
        <translation>Disquette</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3145"/>
        <source>NetworkPlace</source>
        <translation>Emplacement réseau</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3146"/>
        <source>FlashDrive</source>
        <translation>Mémoire flash</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3147"/>
        <source>OtherDevice</source>
        <translation>Autre périphérique</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3150"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3151"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>CdCatConfig</name>
    <message>
        <location filename="../config.cpp" line="213"/>
        <source>Error during autoload...</source>
        <translation>Erreur lors du chargement automatique...</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="215"/>
        <source>I can&apos;t open the autoload catalog.
Check the file, or change the autoload option in the config dialog!
</source>
        <translation>Impossible de charger automatiquement le Catalogue.
Vérifiez le fichier ou changez l&apos;option de chargement automatique dans la boite de dialogue de Configuration!</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="215"/>
        <source>I can&apos;t open the autoload catalog according the first command line parameter.
Check the file!
</source>
        <translation>Impossible de charger automatiquement le Catalogue d&apos;après le premier paramètre de la ligne de commande.
Vérifiez le fichier !</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="0"/>
        <source>I can&apos;t get the $HOME environment variable.
It is necessary couse I&apos;d like to save the config file into your home directory.</source>
        <translation type="obsolete">Impossible de trouver la variable d&apos;environnement $HOME. Elle est nécessaire afin que je puisse sauvegarder le fichier de configuration dans votre répertoire personnel.</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1347"/>
        <source>I can&apos;t create or rewrite the ./cdcat.cfg file </source>
        <translation>Impossible de créer ou écraser le fichier ./cdcat.cfg</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1349"/>
        <source>I can&apos;t create or rewrite the $(HOME)/.cdcatconfig file </source>
        <translation>Impossible de créer ou écraser le fichier $(HOME)/.cdcatconfig</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1349"/>
        <source>Error while saving config file...</source>
        <translation>Erreur lors de la sauvegarde du fichier de configuration...</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="972"/>
        <source>I can&apos;t get the $HOME environment variable.
It is necessary because I&apos;d like to save the config file into your home directory.</source>
        <translation>Impossible de trouver la variable d&apos;environnement $HOME. Elle est nécessaire afin que je puisse sauvegarder le fichier de configuration dans votre répertoire personnel.</translation>
    </message>
</context>
<context>
    <name>CdCatMainWidget</name>
    <message>
        <location filename="../mainwidget.cpp" line="646"/>
        <source>Directory Tree</source>
        <translation>Arborescence</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="649"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="650"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="171"/>
        <source>New...</source>
        <translation type="obsolete">Nouveau...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="172"/>
        <source>Open...</source>
        <translation type="obsolete">Ouvrir...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="173"/>
        <source>Save</source>
        <translation type="obsolete">Enregistrer</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="174"/>
        <source>Save As...</source>
        <translation type="obsolete">Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="177"/>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="187"/>
        <source>Quit</source>
        <translation type="obsolete">Quitter</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="677"/>
        <source>Add media...</source>
        <translation>Ajouter un média...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="681"/>
        <source>Rescan media...</source>
        <translation>Parcourir le média à nouveau...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="683"/>
        <source>Insert Catalog...</source>
        <translation>Insérer un Catalogue...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="687"/>
        <source>Rename node...</source>
        <translation>Renommer le noeud...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="693"/>
        <source>Delete node</source>
        <translation>Supprimer le noeud</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="578"/>
        <source>Sort media by number</source>
        <translation type="obsolete">Trier par numéro</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="307"/>
        <source>Sort media by name</source>
        <translation>Trier par nom</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="202"/>
        <source>Sort media by type</source>
        <translation type="obsolete">Trier par type</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="582"/>
        <source>Sort media by time</source>
        <translation type="obsolete">Trier par date</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="718"/>
        <source>Seek in database...</source>
        <translation>Rechercher dans la base...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="724"/>
        <source>Node size</source>
        <translation>Taille du noeud</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="726"/>
        <source>Configuration...</source>
        <translation>Configuration...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="735"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="514"/>
        <source>About</source>
        <translation type="obsolete">A propos de CdCat</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="350"/>
        <source>Catalog</source>
        <translation type="obsolete">Catalogue</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="638"/>
        <source>Edit</source>
        <translation>Edition</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="642"/>
        <source>Others</source>
        <translation>Autres</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="747"/>
        <source>Hyper&apos;s CD Catalogizer</source>
        <translation>Hyper&apos;s CD Catalogizer</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="493"/>
        <source>Open a catalog from a file.</source>
        <translation type="obsolete">Ouvrir un Catalogue à partir d&apos;un fichier.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="499"/>
        <source>Close the actual catalog.</source>
        <translation type="obsolete">Fermer le Catalogue courant.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="501"/>
        <source>Add a new media to the catalog.</source>
        <translation type="obsolete">Ajouter un nouveau média à ce Catalogue.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="0"/>
        <source>Delete the selected media from the catalog.</source>
        <translation type="obsolete">Supprimer le média associé à ce Catalogue.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="507"/>
        <source>Configuration of the program.</source>
        <translation type="obsolete">Configuration du programme.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="511"/>
        <source>Create a new, empty catalog and close the previous.</source>
        <translation type="obsolete">Créer un nouveau Catalogue vide et fermer le précédent.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="513"/>
        <source>Search an element in the database(catalog).You can search filenames, directory names, name parts or mp3 tags etc...</source>
        <translation type="obsolete">Rechercher un élément dans la base (Catalogue). Vous pouvez rechercher par nom de fichier, nom de répertoire, parties du nom ou balises mp3...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="369"/>
        <source>Search an element.</source>
        <translation type="obsolete">Rechercher un élément.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="672"/>
        <source>Recent files...</source>
        <translation>Fichiers récents...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="0"/>
        <source>Export database</source>
        <translation type="obsolete">Exporter la base</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="641"/>
        <source>Import/Export</source>
        <translation>Importer/Exporter</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="689"/>
        <source>Re-Number media node...</source>
        <translation>Re-numéroter le noeud...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="0"/>
        <source>Import database (csv/gtktalog)</source>
        <translation type="obsolete">Importer une base (csv/gtktalog)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="723"/>
        <source>Borrowing info...</source>
        <translation>Info empruntée...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="495"/>
        <source>Save all modifications to the disc.</source>
        <translation type="obsolete">Enregistre toutes les modifications sur le disque.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="497"/>
        <source>Save the catalog to a new file.</source>
        <translation type="obsolete">Sauvegarder le Catalogue dans un nouveau fichier.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="0"/>
        <source>Add a link to a CdCAt Catalog...</source>
        <translation type="obsolete">Ajouter le lien d&apos;un Catalogue CdCat...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="728"/>
        <source>Set Colors...</source>
        <translation>Paramétrer les couleurs...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="85"/>
        <source>No item selected</source>
        <translation type="obsolete">Aucun élément sélectionné</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="503"/>
        <source>Rescan the selected media.</source>
        <translation type="obsolete">Parcourir à nouveau le média sélectionné.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="721"/>
        <source>Seek in the panel</source>
        <translation>Rechercher dans la fenêtre</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="679"/>
        <source>Add a link to a CdCat Catalog...</source>
        <translation>Ajouter un lien à un Catalogue CdCat...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="738"/>
        <source>About Qt</source>
        <translation>A propos de QT</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="730"/>
        <source>Import database (CSV/XML)</source>
        <translation>Importer une base (CSV/XML)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="733"/>
        <source>Export database (CSV/HTML/XML)</source>
        <translation>Exporter la base (CSV/HTML/XML)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="505"/>
        <source>Delete selected node.</source>
        <translation type="obsolete">Supprimer le noeud sélectionné.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="799"/>
        <source>Scanning:</source>
        <translation>Scan en cours :</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="640"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="655"/>
        <source>&amp;New...</source>
        <translation>&amp;Nouveau...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="656"/>
        <source>Create a new catalog</source>
        <translation>Créer un nouveau catalogue</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="657"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="658"/>
        <source>Open a existing catalog</source>
        <translation>Ouvrir un catalogue existant</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="659"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="660"/>
        <source>Save catalog</source>
        <translation>Enregistrer le catalogue</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="661"/>
        <source>&amp;Save as...</source>
        <translation>Enregistrer &amp;sous...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="662"/>
        <source>save catalog with new name</source>
        <translation>Enregistrer le catalogue sous un nouveau nom</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="674"/>
        <source>Close catalog</source>
        <translation>Fermer le catalogue</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="821"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="676"/>
        <source>Close program</source>
        <translation>Quitter le programme</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="678"/>
        <source>Add new media to catalog</source>
        <translation>Ajouter un nouveau média au catalogue</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="680"/>
        <source>Add a link to a existing cdcat catalog</source>
        <translation>Ajouter un lien d&apos;un catalogue déjà existant</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="682"/>
        <source>Rescan existing media</source>
        <translation>Scanner à nouveau un média existant</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="684"/>
        <source>Insert catalog into database</source>
        <translation>Insérer un catalogue dans la base de données</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="688"/>
        <source>Rename node</source>
        <translation>Renommer le noeud</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="690"/>
        <source>Renumber node</source>
        <translation>Renuméroter le noeud</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="719"/>
        <source>Seek in database for files and folders</source>
        <translation>Rechercher fichiers et dossiers dans la base de données</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="725"/>
        <source>Calculate node size</source>
        <translation>Calculer la taille du noeud</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="727"/>
        <source>Edit cdcat configuration</source>
        <translation>Editer la configuration de cdcat</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="729"/>
        <source>Set the colors for display</source>
        <translation>Définir les couleurs pour l&apos;affichage</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="731"/>
        <source>Import database (CSV/XML) from various catalog programs</source>
        <translation>Importer une base de données (CVS/XML) de divers catalogueurs</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="737"/>
        <source>About Cdcat</source>
        <translation>A propos de CdCat</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="739"/>
        <source>About the Qt toolkit</source>
        <translation>A propos du kit Qt</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="812"/>
        <source>Mi&amp;nimize</source>
        <translation>Réduire</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="815"/>
        <source>Ma&amp;ximize</source>
        <translation>Maximiser</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="818"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restaurer</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="920"/>
        <source>Cdcat - idle</source>
        <translation>Cdcat - attente</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="81"/>
        <source>Main toolbar</source>
        <translation>Barre d&apos;outils principale</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="109"/>
        <source>Comment dock</source>
        <translation>Barre des Propriétés</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="710"/>
        <source>view tool bar</source>
        <translation>Afficher les barres d&apos;outils</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="711"/>
        <source>View tool bar in main window</source>
        <translation>Afficher la barre d&apos;outils dans la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="712"/>
        <source>view status bar</source>
        <translation>Afficher la barre d&apos;état</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="713"/>
        <source>View status bar in main window</source>
        <translation>Afficher la barre d&apos;état dans la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="714"/>
        <source>view comment dock</source>
        <translation>Afficher la barre latérale des propriétés</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="715"/>
        <source>show comment dock</source>
        <translation>Afficher la barre latérale des propriétés</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="639"/>
        <source>View</source>
        <translation>Afficher</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="717"/>
        <source>show systray icon</source>
        <translation>Afficher l&apos;icône dans la zone de notification</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="637"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="651"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../mainwidget.h" line="94"/>
        <source>processing file</source>
        <translation>traitement du fichier</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="695"/>
        <source>Sort media by number (ascending)</source>
        <translation>Trier les médias par numéro (ascendant)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="697"/>
        <source>Sort media by number (descending)</source>
        <translation>Trier les médias par numéro (descendant)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="703"/>
        <source>Sort media by time (ascending)</source>
        <translation>Trier les médias par date et heure (ascendant)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="705"/>
        <source>Sort media by time (descending)</source>
        <translation>Trier les médias par date et heure (descendant)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="251"/>
        <source>Insert Cdcat export XML...</source>
        <translation>Insérer un XML d&apos;exportation CdCat...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="254"/>
        <source>Insert Cdcat exported XML into database</source>
        <translation>Insérer un XML d&apos;exportation CdCat dans la base</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="699"/>
        <source>Sort media by name (ascending)</source>
        <translation>Trier les médias par nom (ascendant)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="701"/>
        <source>Sort media by name (descending)</source>
        <translation>Trier les médias par nom (descendant)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="707"/>
        <source>Sort media by type (ascending)</source>
        <translation>Trier les médias par type (ascendant)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="709"/>
        <source>Sort media by type (descending)</source>
        <translation>Trier les médias par type (descendant)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="685"/>
        <source>Insert Cdcat XML export...</source>
        <translation>Insérer un XML d&apos;exportation CdCat...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="686"/>
        <source>Insert Cdcat XML export</source>
        <translation>Insérer un XML d&apos;exportation CdCat</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="664"/>
        <source>Change password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="665"/>
        <source>Changes password for catalog encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="666"/>
        <source>Disable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="667"/>
        <source>Disables catalog encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="668"/>
        <source>Enable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="669"/>
        <source>Enables catalog encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="395"/>
        <source>Borrowing info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="444"/>
        <source>Key bindings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSchemePreview</name>
    <message>
        <location filename="../colorsettings.cpp" line="71"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="73"/>
        <source>Road To Hell</source>
        <translation>Road To Hell</translation>
    </message>
</context>
<context>
    <name>ColorSettings</name>
    <message>
        <location filename="../colorsettings.cpp" line="221"/>
        <source>ColorSettings</source>
        <translation>Paramétrage des couleurs</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="223"/>
        <source>Comment area background and the file higlighting line</source>
        <translation>Fond de la zone de commentaire et dela ligne sélectionnée du fichier</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="224"/>
        <source>Frame of comment area</source>
        <translation>Cadre de la zone de commentaire</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="225"/>
        <source>Comment window static text (Program text)</source>
        <translation>Texte statique de la fenêtre de Commentaire (texte du programme)</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="226"/>
        <source>Comment window variable text (Data)</source>
        <translation>Texte variable de la fenêtre de Commentaire (Données)</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="227"/>
        <source>color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="228"/>
        <source>Red:</source>
        <translation>Rouge :</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="229"/>
        <source>Green:</source>
        <translation>Vert :</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="230"/>
        <source>Blue:</source>
        <translation>Bleu :</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="231"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="232"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>CommentWidget</name>
    <message>
        <location filename="../commwidget.cpp" line="88"/>
        <source>Edit and refresh the actual comment page.</source>
        <translation>Editer et actualiser le commentaire.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="182"/>
        <source>Upper container! (..)</source>
        <translation>Conteneur supérieur! (..)</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="239"/>
        <source> CdCat Catalog root</source>
        <translation>Racine du Catalogue CdCat</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="245"/>
        <source> Directory</source>
        <translation>Répertoire</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="248"/>
        <source> File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="251"/>
        <source>Media</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="265"/>
        <source>Size:</source>
        <translation>Taille :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="328"/>
        <source>Owner:</source>
        <translation>Propriétaire :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="336"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="0"/>
        <source>Media (number/name):</source>
        <translation type="obsolete">Média (Numéro/Nom) :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="386"/>
        <source>Mp3-Tag:</source>
        <translation>Balise mp3 :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="313"/>
        <source>(Art/Tit/Alb/Year/Comm)</source>
        <translation type="obsolete">(Artiste/Titre/Album/Année/Commentaire)</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="495"/>
        <source>Comment:</source>
        <translation>Commentaires :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="83"/>
        <source>Shows the content of the file.</source>
        <translation>Affiche le contenu du fichier.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="191"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="234"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="844"/>
        <source>There is no selected element.</source>
        <translation>Aucun élément sélectionné.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="242"/>
        <source> Link to a CdCat catalog</source>
        <translation>Lier à un Catalogue CdCat</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="283"/>
        <source>Location:</source>
        <translation>Emplacement :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="297"/>
        <source>Last modification:</source>
        <translation>Dernière modification :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="98"/>
        <source>Edit and refresh the actual category page.</source>
        <translation>Editer et actualiser la catégorie de la page courante.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="204"/>
        <source>At media (number/name):</source>
        <translation>Média (numéro/nom) :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="560"/>
        <source>Category:</source>
        <translation>Catégorie :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="615"/>
        <source>Archive contents:</source>
        <translation>Contenu de l&apos;archive :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="393"/>
        <source>Artist:</source>
        <translation>Interprète:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="399"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="399"/>
        <source>track:</source>
        <translation>Piste :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="405"/>
        <source>Album:</source>
        <translation>Album :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="411"/>
        <source>Year:</source>
        <translation>Année :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="730"/>
        <source>File info:</source>
        <translation>Infos du fichier :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="636"/>
        <source>Rights</source>
        <translation>Droits d&apos;auteur</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="641"/>
        <source>Owner</source>
        <translation>Propriétaire</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="646"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="651"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="656"/>
        <source>Changed</source>
        <translation>Modifié</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="666"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="661"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="431"/>
        <source>Exif data:</source>
        <translation>données Exif</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="457"/>
        <source>Thumbnail:</source>
        <translation>Vignette</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="825"/>
        <source>Stored size:</source>
        <translation>Taille de stockage</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="845"/>
        <source>There is no selected element:</source>
        <translation>Aucun élément n&apos;est sélectionné :</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="353"/>
        <source>Number</source>
        <translation>Numéro</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="356"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="359"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="362"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="367"/>
        <source>sorted by:</source>
        <translation>trié par :</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../config.cpp" line="1609"/>
        <source>Configure  CdCat...</source>
        <translation>Configuration de CdCat...</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1613"/>
        <source>Autoload DataBase on startup</source>
        <translation>Charger automatiquement la base de données au démarrage</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1615"/>
        <source>Save the XML-db.file nicer format(needs more space)</source>
        <translation>Sauvegarder le fichier XML-db dans un format plus lisible (nécessite plus d&apos;espace)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1616"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1617"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1621"/>
        <source>Application font size.(must restart cdcat!) </source>
        <translation>Taille de la police utilisée dans l&apos;application (nécessite un redémarrage)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="0"/>
        <source>CdCat Databases (*.hcf)</source>
        <translation type="obsolete">Bases de données CdCat (*.hcf)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1612"/>
        <source>Path to cdrom device</source>
        <translation type="obsolete">Point de montage du CDROM</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1627"/>
        <source>Scanning: mount cdrom at start / eject when finish</source>
        <translation>Scan : monter le CDROM automatiquement / éjecter à la fin</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1619"/>
        <source>Use own font size</source>
        <translation>Utiliser une taille de police personnalisée</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1622"/>
        <source>Number of history entries</source>
        <translation>Nombre d&apos;entrées de l&apos;historique</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1646"/>
        <source>Choose path to cdrom!</source>
        <translation>Choisir le chemin du point de montage du CDROM !</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1631"/>
        <source>The language of CdCat interface</source>
        <translation>Langue de l&apos;interface CdCat</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1640"/>
        <source>Choose a file to load automatically!</source>
        <translation>Choisir un fichier à charger automatiquement !</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="0"/>
        <source>Automatically save the database after every scan (for dafety sake)</source>
        <translation type="obsolete">Sauvegarder automatiquement la base de données après chaque scan</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1618"/>
        <source>Select additional items to read</source>
        <translation>Choisir des éléments supplémentaires à lire</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1440"/>
        <source>Supported extensions:</source>
        <translation type="obsolete">Extensions supportées :</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1614"/>
        <source>Automatically save the database after every scan (for safety sake)</source>
        <translation>Sauvegarder automatiquement la base de données après chaque scan (par sécurité)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1438"/>
        <source>Archive file display options</source>
        <translation type="obsolete">Options d&apos;affichage des archives</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1439"/>
        <source>Scan for archive file list</source>
        <translation type="obsolete">Scanner les fichiers d&apos;une archive</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1441"/>
        <source>Show permission</source>
        <translation type="obsolete">Afficher les permissions</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1442"/>
        <source>Show user</source>
        <translation type="obsolete">Afficher l&apos;utillisateur</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1443"/>
        <source>Show group</source>
        <translation type="obsolete">Afficher le groupe</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1444"/>
        <source>Show size</source>
        <translation type="obsolete">Afficher la taille</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1445"/>
        <source>Show date</source>
        <translation type="obsolete">Afficher la date</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1446"/>
        <source>Show optional comment</source>
        <translation type="obsolete">Afficher les commentaires optionnels</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1634"/>
        <source>Display debug info on console</source>
        <translation>Afficher les informations de déboguage en console</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1635"/>
        <source>Save catalogs always as UTF8</source>
        <translation>Sauvegarder toujours les Catalogues en UTF8</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1636"/>
        <source>Show progressed file at scanning in status label</source>
        <translation>Montrer le fichier en cours de scan dans la barre d&apos;état</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1640"/>
        <source>CdCat databases (*.hcf )</source>
        <translation>Bases de données CdCat (*.hcf)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1620"/>
        <source>font size</source>
        <translation>taille de la police</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1494"/>
        <source>show systray icon</source>
        <translation>Afficher l&apos;icône dans la zone de notification</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1610"/>
        <source>Show systray icon</source>
        <translation>Afficher l&apos;icône dans la zone de notification</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1611"/>
        <source>display current scanned file in tray</source>
        <translation>Afficher le fichier scanné courant dans la zone de notification</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1612"/>
        <source>display current scanned file in tray (mediainfo / archive scan)</source>
        <translation>Afficher le fichier scanné courant (info média / archive scannée) dans la zone de notification</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1623"/>
        <source>Path to cdrom device / mount dir</source>
        <translation>Chemin du cdrom</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1624"/>
        <source>This is the path to the cdrom/dvd device or the path where its mounted</source>
        <translation>Ceci est le chemin du cdrom/dvd ou le point de montage</translation>
    </message>
</context>
<context>
    <name>DataBase</name>
    <message>
        <location filename="../dbase.cpp" line="719"/>
        <source>I can&apos;t rewrite the file: %1</source>
        <translation>Impossible d&apos;écraser le fichier : %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="744"/>
        <source>I can&apos;t create the file: %1</source>
        <translation>Impossible de créer le fichier : %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2091"/>
        <source>I can&apos;t open the file: %1</source>
        <translation>Impossible d&apos;ouvrir le fichier : %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="990"/>
        <source>Cannot read directory: %1</source>
        <translation>Impossible de lire le répertoire : %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2127"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="0"/>
        <source>Ignore directory</source>
        <translation type="obsolete">Ignorer le répertoire</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="994"/>
        <source>Cancel scanning</source>
        <translation>Annuler le scan</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="0"/>
        <source>Symbolic link#Points to:</source>
        <translation type="obsolete">Lien symbolique vers :</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2127"/>
        <source>Error while parsing file: %1</source>
        <translation>Erreur lors de l&apos;analyse du fichier : %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="769"/>
        <source>No database opened!</source>
        <translation>Aucune base de données ouverte !</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="422"/>
        <source>Please Wait...</source>
        <translation type="obsolete">Merci de patienter...</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="588"/>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2115"/>
        <source>Not enough memory to open the file: %1</source>
        <translation>Mémoire insuffisante pour ouvrir le fichier : %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="992"/>
        <source>Cannot read file: %1</source>
        <translation>Impossible de lire le fichier : %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="994"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1091"/>
        <source>Symbolic link to file:#</source>
        <translation>Lien symbolique au fichier : #</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1143"/>
        <source>Symbolic link to directory:#</source>
        <translation>Lien symbolique au répertoire : #</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1172"/>
        <source>DEAD Symbolic link to:#</source>
        <translation>Lien symbolique ROMPU vers : #</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1182"/>
        <source>System file (e.g. FIFO, socket or device file)</source>
        <translation>Fichier système (e.g. FIFO, socket ou fichier matériel)</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1876"/>
        <source>device </source>
        <translation>matériel</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1892"/>
        <source> link to </source>
        <translation>Lien vers </translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="959"/>
        <source>Reading file, please wait...</source>
        <translation>Lecture du fichier, merci de patentier...</translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1080"/>
        <source>Converting to unicode, please wait...</source>
        <translation>Conversion vers unicode, merci de patienter...</translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1087"/>
        <source>Parsing file, please wait...</source>
        <translation>Analyse du fichier, merci de patienter...</translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="893"/>
        <source>Parse error</source>
        <translation type="obsolete">Erreur d&apos;analyse</translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1094"/>
        <source>You have cancelled catalog reading.</source>
        <translation>Vous avez annulé la lecture du catalogue.</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1208"/>
        <source>reading mp3 info</source>
        <translation>lecture des infos mp3</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1262"/>
        <source>reading media info</source>
        <translation>lecture des infos du média</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1279"/>
        <source>reading avi info</source>
        <translation>lecture des infos avi</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1353"/>
        <source>reading file content</source>
        <translation>lecture du contenu du fichier</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1407"/>
        <source>reading exif data</source>
        <translation>lecture des données exif</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1436"/>
        <source>reading thumbnail data</source>
        <translation>lecture des données de la vignette</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1948"/>
        <source>scanning archive</source>
        <translation>scan de l&apos;archive</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2044"/>
        <source>scanning archive, file:</source>
        <translation>scan du de l&apos;archive, fichier :</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="571"/>
        <source>unknown</source>
        <translation>Inconnu</translation>
    </message>
</context>
<context>
    <name>DirectoryView</name>
    <message>
        <location filename="../dirview.cpp" line="185"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>GuiSlave</name>
    <message>
        <location filename="../guibase.cpp" line="622"/>
        <source>Directory</source>
        <translation>Répertoire</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="679"/>
        <source>Catalog</source>
        <translation>Catalogue</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2274"/>
        <source>Unknown(DB)</source>
        <translation>Inconnu(DB)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2278"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2282"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2286"/>
        <source>HardDisc</source>
        <translation>Disque Dur</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2290"/>
        <source>Floppy</source>
        <translation>Disquette</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2294"/>
        <source>NetworkDrv</source>
        <translation>Disque sur réseau</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2298"/>
        <source>FlashDrv</source>
        <translation>Disque Flash</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2302"/>
        <source>OtherDevice</source>
        <translation>Autre périphérique</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="710"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="926"/>
        <source>Hyper&apos;s CD Catalogizer (modified)</source>
        <translation>Hyper&apos;s CD Catalogizer (modifié)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="932"/>
        <source>Hyper&apos;s CD Catalogizer</source>
        <translation>Hyper&apos;s CD Catalogizer</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2198"/>
        <source>CdCat databases (*.hcf )</source>
        <translation>Bases de données CdCat (*.hcf)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1176"/>
        <source>Open a file...</source>
        <translation>Ouvrir un fichier...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2847"/>
        <source>Error while opening...</source>
        <translation>Erreur lors de l&apos;ouverture...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1345"/>
        <source>Save to file...</source>
        <translation>Sauvegarder dans un fichier...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1370"/>
        <source>Error while saving...</source>
        <translation>Erreur lors de la sauvegarde...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1418"/>
        <source>Closing previous database...</source>
        <translation>Fermeture de la base de données précédente...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1418"/>
        <source>Do you want to save the changes?</source>
        <translation>Sauvegarder les modifications ?</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2914"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2914"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2930"/>
        <source>Error:</source>
        <translation>Erreur :</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1434"/>
        <source>Do you really want to delete this?: 
%1</source>
        <translation type="obsolete">Faut-il vraiment supprimer : %1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2912"/>
        <source>Confirmation</source>
        <translation>Confirmation</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1811"/>
        <source>Warning...</source>
        <translation>Alerte...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1957"/>
        <source>You can refresh ONLY a MEDIA node!</source>
        <translation>SEUL un noeud MEDIA peut être réactualisé !</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2198"/>
        <source>Insert a database file...</source>
        <translation>Insérer un catalogue dans la base de données...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2379"/>
        <source>The size of &quot;%1&quot; : 
 %2 
 %3 file /%4 directory</source>
        <translation>La taille de &quot;%1&quot; :
%2
%3 fichier /%4 répertoire</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1541"/>
        <source>Cannot mount CD</source>
        <translation>Impossible de monter le CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1887"/>
        <source>Cannot eject CD!</source>
        <translation>Impossible d&apos;éjecter le CD !</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1541"/>
        <source>I can&apos;t find the &quot;mount&quot; program</source>
        <translation>Programme &quot;mount&quot; introuvable</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1905"/>
        <source>Cannot mount CD!</source>
        <translation>Impossible de monter le CD !</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1853"/>
        <source>Cannot eject CD</source>
        <translation>Impossible d&apos;éjecter le CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1853"/>
        <source>I can&apos;t find the &quot;eject&quot; program</source>
        <translation>Programme &quot;eject&quot; introuvable</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1042"/>
        <source>Rename node...</source>
        <translation>Renommer le noeud...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1043"/>
        <source>Delete node</source>
        <translation>Supprimer le noeud</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1052"/>
        <source>Rescan media...</source>
        <translation>Scanner le média à nouveau...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2542"/>
        <source>Add media...</source>
        <translation>Ajouter un média...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1077"/>
        <source>Insert Catalog...</source>
        <translation>Insérer un Catalogue...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1961"/>
        <source>Rescan %1</source>
        <translation>Rescanner %1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1053"/>
        <source>Re-Number media...</source>
        <translation>Re-numéroter le média...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2322"/>
        <source>It is NOT a media node!</source>
        <translation>Ce n&apos;est PAS un noeud média !</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1037"/>
        <source>Node size</source>
        <translation>Taille du noeud</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1034"/>
        <source>View/Edit Comment...</source>
        <translation>Voir/Editer le commentaire...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1081"/>
        <source>Close all branch</source>
        <translation>Fermer toute l&apos;arborescence</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="605"/>
        <source>Show/Remove Content...</source>
        <translation type="obsolete">Afficher/Supprimer le contenu...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2930"/>
        <source>There is no selected item in the middle list box!</source>
        <translation>Aucun élément sélectionné dans la liste déroulante du milieu !</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1811"/>
        <source>An error occured while scanning,
the DataBase may be incomplete</source>
        <translation>unne erreur est survenue pendant le scan, la base de données peut être incomplète </translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1953"/>
        <source>There is no selected Media in the middle list box!</source>
        <translation>Aucun média sélectionné dans la liste déroulante du milieu !</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2027"/>
        <source>An error occured while scanning, the rescan operation was cancelled: 
%1</source>
        <translation>unne erreur est survenue pendant le scan, l&apos;opération de scan a été annulée :
%1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1048"/>
        <source>Borrow this media to...</source>
        <translation>Emprunter ce média à...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2913"/>
        <source>Do you want to clear the borrowing mark from media &quot;%1&quot;?
(Say yes if you got it back.)</source>
        <translation>Supprimer l&apos;indicateur d&apos;emprunt du média &quot;%1&quot; ?
(Oui si vous l&apos;avez récupéré)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1050"/>
        <source>I got it back! (clear borrowing mark)</source>
        <translation>Je l&apos;ai à nouveau ! (supprimer l&apos;indicateur d&apos;emprunt)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="713"/>
        <source>Catalog Link</source>
        <translation>Lien Catalogue</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="991"/>
        <source>Follow the link (Open it) !</source>
        <translation>Suivre le lien (l&apos;ouvrir) !</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="777"/>
        <source>Add a link to a CdCAt Catalog...</source>
        <translation type="obsolete">Ajouter un lien à un Catalogue CdCat...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2807"/>
        <source>The link is empty.</source>
        <translation>Le lien est vide.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="595"/>
        <source>Number</source>
        <translation>Numéro</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="598"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="343"/>
        <source>The database file has newer version than this version of cdcat can work with:
I understand maximum %1 datafile version but readed %2

Strongly recommended to upgrade your cdcat!!!
Homepage: %3</source>
        <translation>Le fichier base de donnée est d&apos;une version trop récente pour CdCat :
CdCat comprend la version maximum %1 et celle du ficher est %2

Il est fortement recommandé de mettre à jour votre application CdCat !!!
Page d&apos;accueil : %3</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="354"/>
        <source>No database opened.</source>
        <translation>Aucune base de données ouverte.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="356"/>
        <source>No item selected.</source>
        <translation>Aucun élément sélectionné.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="0"/>
        <source>The resoult:</source>
        <translation type="obsolete">Le résultat :</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1035"/>
        <source>View/Edit Category...</source>
        <translation>Voir/Editer la catégorie...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1057"/>
        <source>Change media type...</source>
        <translation>Changer le type de média...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1985"/>
        <source>Scanning directory tree, please wait...</source>
        <translation>Scan de la structure de répertoire, patientez...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1962"/>
        <source>Select directory</source>
        <translation>Sélectionnez un répertoire</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1644"/>
        <source>MiB</source>
        <translation type="obsolete">MiO</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2384"/>
        <source>The result:</source>
        <translation>Le résultat :</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1718"/>
        <source>Enter media name...</source>
        <translation>Donner le nom du média...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1719"/>
        <source>Media name:</source>
        <translation>Nom du média :</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1720"/>
        <source>Media</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1009"/>
        <source>search for duplicates...</source>
        <translation>chercher les doublons</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="983"/>
        <source>Show content...</source>
        <translation>Afficher le contenu</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="986"/>
        <source>Show/Remove content...</source>
        <translation>Afficher/Supprimer le contenu...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1014"/>
        <source>Add a link to a CdCat Catalog...</source>
        <translation>Ajouter un lien à un Catalogue CdCat...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1076"/>
        <source>Add a link to a Cdcat catalog...</source>
        <translation>Ajouter un lien à un Catalogue CdCat...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1770"/>
        <source>You have cancelled catalog scanning,
the DataBase may be incomplete</source>
        <translation>Vous venez d&apos;annuler le scan d&apos;un catalogue,
la base de données pourrait être incomplète</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1988"/>
        <source>Scan started</source>
        <translation>Le scan est lancé</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1988"/>
        <source>Scanning %1 into %2 has been started</source>
        <translation>Le scan du fichier %1 sur %2 a été lancé</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2003"/>
        <source>Scanning %1 into %2 has been finished</source>
        <translation>Le scan du fichier %1 sur %2 est terminé</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2007"/>
        <source>Scan finished</source>
        <translation>Scan terminé</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2007"/>
        <source>Scanning %1 into %2 has been finished (NOT complete)</source>
        <translation>Le scan du fichier %1 sur %2 est terminé (incomplet)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2095"/>
        <source>Cdcat - idle</source>
        <translation>CdCat - attente</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2514"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2516"/>
        <source>Whats this?</source>
        <translation>Présentation</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2519"/>
        <source>The cdcat is graphical (QT based) multiplatform (Linux/Windows) catalog program which scans the directories/drives you want and memorize the filesystem including the tags of mp3&apos;s and other data and store it in a small file. The database is stored in a gzipped XML format, so you can hack it, or use it if necessary :-).)</source>
        <translation>Cdcat est un catalogueur graphique (basé sur QT) multi-plateforme (Linux/Windows) qui scanne les répertoires/disques désirés et mémorise le système de fichier en incluant les balises mp3 et autres type de données et les enregistre dans une petite base de données. Cette base de donnée est enregistrée dans un format XML gzippé, que vous pourrez modifier, si çà vous chante :-).)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2521"/>
        <source>The program can store the content of some specified files up to a limit size if you want. (for example: *.nfo)</source>
        <translation>Le programme peut enregistrer le contenu de certains types de fichiers spécifiés jusqu&apos;à une limite de taille, si vous le désirez. (par exemple: *.nfo)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2524"/>
        <source>Usage:</source>
        <translation>Usage :</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2527"/>
        <source>Before the scanning select the necessary readable components in the config dialog, which can be mp3 tags content of some files or etc.</source>
        <translation>Avant de scanner, sélectionnez les composants nécessaires dans la boite de configuration, les balises mp3, le contenu de certains types de fichiers, etc...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2531"/>
        <source>Create a new catalog</source>
        <translation>Créer un nouveau catalogue</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2534"/>
        <source>Run the %1 command in the catalog menu. You have to type the name of the new catalog. You can specify the default username of the media(which you scan later), and add a comment to the catalog.</source>
        <translation>Lancez la commande %1 dans le menu Fichier. Vous devez saisir le nom du nouveau catalogue. Vous pouvez spécifier le nom de l&apos;utilisateur par défaut du media (que vous aller scanner), et ajouter un commentaire au catalogue.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2534"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2538"/>
        <source>Add media</source>
        <translation>Ajouter un média</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2542"/>
        <source>Run the %1 command in the edit menu. In that dialog you have to specify the directory/or drive the media you want to add. It&apos;s recommended to specify the name and the number of the media which has to be unique. (The program always generate one identical name and number). You can label the media to a owner, if necessary.</source>
        <translation>Lancez la commande %1 dans le menu Edition. Dans cette boite, vous devez spécifier le répertoire/ou le disque du média que vous voulez ajouter. Il est recommandé de spécifier les noms et muméros du média lequels doivent être uniques. (Le programme genère un même nom et numéro identique). Vous pouvez affecter le média à un propriétaire, si nécessaire.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2547"/>
        <source>save as</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2551"/>
        <source>Open an existing catalog:</source>
        <translation>Ouvrir un catalogue existant</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2553"/>
        <source>open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2557"/>
        <source>Author:</source>
        <translation>Auteur :</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2560"/>
        <source>The program was written by Peter Deak (hungary)</source>
        <translation>Le programme a été écrit par Peter Deak (Hongrie)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2563"/>
        <source>The current maintainer is %1.</source>
        <translation>Le programmeur actuel est %1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2567"/>
        <source>License:</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2571"/>
        <source>General Public License (GPL)</source>
        <translation>Licence Publique Générale (GPL)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2575"/>
        <source>Homepage:</source>
        <translation>Page d&apos;accueil</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2579"/>
        <source>You can read about the program and get new versions, sources etc, in the hompage of cdcat:</source>
        <translation>Vous trouverez toutes les informations sur le programme, et pourrez télécharger les nouvelles versions, les sources etc, sur le site de CdCat.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2553"/>
        <source>Run the %1 command in the file menu, and choice the file of the catalog. (*.hcf). After the opening you will be able browse the catalog or search in it.</source>
        <translation>Run the %1 command in the file menu, and choice the file of the catalog. (*.hcf). After the opening you will be able browse the catalog or search in it.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2547"/>
        <source>If you scanned your media, you will be able to browse in it with the browser window (like mc) , or search in it. You can save the catalog with %1 command in the file menu.</source>
        <translation>Si vous scannez votre média, vous pourrez naviguer dans celui-ci avec la fenêtre de navigation (like mc??), ou bien encore d&apos;y effecteur des recherches. Vous pourrez enregistrer le catalogue avec la commande %1 du menu Fichier</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1352"/>
        <source>Saving catalog, please wait...</source>
        <translation>Sauvegarde du catalogue, veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2164"/>
        <source>Insert a cdcat exported xml file...</source>
        <translation>Insérer un export XML CdCat</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2164"/>
        <source>CdCat xml export (*.xml )</source>
        <translation>Export XML CdCat</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1065"/>
        <source>Change password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2955"/>
        <source>Password has been successfully changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3007"/>
        <source>Password changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2994"/>
        <source>Password length is too short, must be minimum 4 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2994"/>
        <source>Password too short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2981"/>
        <source>Password length is too big, must be maximal %1 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2981"/>
        <source>Password too big</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1066"/>
        <source>Disable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1070"/>
        <source>Enable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2986"/>
        <source>Encryption has been successfully enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3007"/>
        <source>Encryption has been successfully disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2998"/>
        <source>Passwords not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2998"/>
        <source>Passwords does not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1453"/>
        <source>Do you really want to delete
&quot;%1&quot;?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HDirectoryView</name>
    <message>
        <location filename="../hdirview.cpp" line="322"/>
        <source>Directory Tree</source>
        <translation>Arborescence</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="323"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="362"/>
        <source>Copy</source>
        <translation type="obsolete">Copier</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="365"/>
        <source>Move</source>
        <translation type="obsolete">Déplacer</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="369"/>
        <source>Link</source>
        <translation type="obsolete">Lier</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="373"/>
        <source>Unknown</source>
        <translation type="obsolete">Inconnu</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="387"/>
        <source>Drop target</source>
        <translation type="obsolete">Supprimer la cible</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="431"/>
        <source>Not implemented</source>
        <translation type="obsolete">Non implémenté</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="430"/>
        <source>Drag source</source>
        <translation type="obsolete">Emmener l&apos;origine</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="431"/>
        <source>Delete </source>
        <translation type="obsolete">Supprimer</translation>
    </message>
</context>
<context>
    <name>HQListView</name>
    <message>
        <location filename="../guibase.cpp" line="198"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="199"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="200"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
</context>
<context>
    <name>ImportDialog</name>
    <message>
        <location filename="../importdialog.cpp" line="80"/>
        <source>Create new Database</source>
        <translation>Créer une nouvelle base de données</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="186"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="187"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="230"/>
        <source>Open file dialog</source>
        <translation type="obsolete">Ouvrir la boite de dialogue d&apos;ouverture de fichier</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="234"/>
        <source>Corrects bad output style (gtktalog)</source>
        <translation type="obsolete">Corrige le mauvais style en sortie (gtktalog)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="243"/>
        <source>Error:</source>
        <translation>Erreur :</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="304"/>
        <source>csv files(*.csv)</source>
        <translation>fichiers csv (*.csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="0"/>
        <source>open file dialog</source>
        <translation type="obsolete">Ouvrir la boite de dialogue d&apos;ouverture de fichier</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="327"/>
        <source>Choose a file for import</source>
        <translation>Choisir un fichier à importer</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="190"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="230"/>
        <source>&lt;strong&gt;Please read the README_IMPORT before you import!&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Bien lire le fichier README_IMPORT avant d&apos;importer !&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="310"/>
        <source>xml files(*.xml)</source>
        <translation>fichiers xml (*.xml)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="180"/>
        <source>Import CSV file</source>
        <translation>Importer un fichier csv</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="181"/>
        <source>File:</source>
        <translation>Fichier :</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="87"/>
        <source>Correct bad style from gtktalog export</source>
        <translation>Corriger le mauvais style d&apos;un export gtktalog </translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="184"/>
        <source>Separator:</source>
        <translation>Séparateur :</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="185"/>
        <source>This is the separator in dataline
&lt;path&gt;SEPARATOR&lt;size&gt;SEPARATOR&lt;date&gt;&lt;space&gt;&lt;time&gt;</source>
        <translation>Voici le séparateur del&apos;enregistrement
&lt;path&gt;SEPARATEUR&lt;size&gt;SEPARATEUR&lt;date&gt;&lt;space&gt;&lt;time&gt;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="173"/>
        <source>Separator in data line</source>
        <translation type="obsolete">Séparateur d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="188"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="0"/>
        <source>&amp;Text (csv)</source>
        <translation type="obsolete">&amp;Texte (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="200"/>
        <source>Gtktalog &amp;XML</source>
        <translation>&amp;XML Gtktalog</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="201"/>
        <source>&amp;WhereIsIt XML (classic)</source>
        <translation>XML &amp;WhereIsI (standard)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="203"/>
        <source>Select the type of import here</source>
        <translation>Sélectionner ici le type d&apos;importation</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="0"/>
        <source>Select this for importing a text import (csv)</source>
        <translation type="obsolete">Sélectionner ceci pour importer un fichier texte (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="0"/>
        <source>Select this for importing a xml report generated frm gtktalog</source>
        <translation type="obsolete">Sélectionner ceci pour importer un fichier XML généré par gtktalog</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="227"/>
        <source>Open the file dialog for selecting file to import.</source>
        <translation>Ouvrir la boite de dialogue pour sélectionner le fichier à importer.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="229"/>
        <source>Corrects bad output style from gtktalog.
&lt;media&gt;SEPARATOR/&lt;dir&gt;/SEPARATOR/&lt;dir&gt;
 will be to
&lt;media&gt;/&lt;dir&gt;/&lt;dir&gt;</source>
        <translation>Corrige le mauvais style en sortie de gtktalog
&lt;media&gt;SEPARATEUR/&lt;dir&gt;/SEPARATEUR/&lt;dir&gt;
deviendra
&lt;media&gt;/&lt;dir&gt;/&lt;dir&gt;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="238"/>
        <source>You must be fill the &quot;Filename&quot; field!</source>
        <translation>Le champ &quot;Nom de Fichier&quot; doit être renseigné !</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="243"/>
        <source>You must be fill the &quot;Separator&quot; field!</source>
        <translation>Le champ &quot;Séparateur&quot; doit être renseigné !</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="315"/>
        <source>all files(*.*)</source>
        <translation>Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="191"/>
        <source>&amp;Gtktalog (csv)</source>
        <translation>&amp;Gtktalog (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="192"/>
        <source>&amp;Kat-CeDe (csv)</source>
        <translation>&amp;Kat-CeDe (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="193"/>
        <source>&amp;Disclib (csv)</source>
        <translation>&amp;Disclib (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="194"/>
        <source>&amp;VisualCD (csv)</source>
        <translation>&amp;VisualCD (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="195"/>
        <source>&amp;VVV (csv)</source>
        <translation>&amp;VVV (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="196"/>
        <source>&amp;Advanced File Organizer (csv)</source>
        <translation>&amp;Advanced File Organizer (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="197"/>
        <source>&amp;File Archivist</source>
        <translation>&amp;File Archivist</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="198"/>
        <source>&amp;Advanced Disk Catalog (csv)</source>
        <translation>&amp;Advanced Disk Catalog (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="199"/>
        <source>W&amp;hereIsIt (csv)</source>
        <translation>W&amp;hereIsIt (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="205"/>
        <source>Select this for importing a text import (csv) generated from Gtktalog</source>
        <translation>Sélectionner ceci pour importer un fichier texte (csv) généré par Gtktalog</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="207"/>
        <source>Select this for importing a text import (csv) generated from Kat-CeDe.</source>
        <translation>Sélectionner ceci pour importer un fichier texte (csv) généré par Kat-CeDe.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="209"/>
        <source>Select this for importing a text import (csv) generated from Disclib.</source>
        <translation>Sélectionner ceci pour importer un fichier texte (csv) généré par Disclib.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="211"/>
        <source>Select this for importing a text import (csv) generated from VisualCD.</source>
        <translation>Sélectionner ceci pour importer un fichier texte (csv) généré par VisualCD.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="213"/>
        <source>Select this for importing a text import (csv) generated from VVV.</source>
        <translation>Sélectionner ceci pour importer un fichier texte (csv) généré par VVV.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="215"/>
        <source>Select this for importing a text import (csv) generated from Advanced File Organizer.</source>
        <translation>Sélectionner ceci pour importer un fichier texte (csv) généré par Advanced File Organizer.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="217"/>
        <source>Select this for importing a File Archivist catalog.</source>
        <translation>Sélectionner ceci pour importer un catalogue File Archivist.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="219"/>
        <source>Select this for importing a text import (csv) generated from Advanced Disk Catalog.</source>
        <translation>Sélectionner ceci pour importer un fichier texte (csv) généré par Advanced Disk Catalog.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="221"/>
        <source>Select this for importing a text import (csv) generated from WhereIsIt.</source>
        <translation>Sélectionner ceci pour importer un fichier texte (csv) généré par WhereIsIt.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="223"/>
        <source>Select this for importing a xml report generated from gtktalog</source>
        <translation>Sélectionner ceci pour importer un fichier texte (csv) généré par Gtktalog</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="225"/>
        <source>Select this for importing a xml report generated from WhereIsIt?</source>
        <translation>Sélectionner ceci pour importer un fichier xml généré par WhereIsIt?</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="313"/>
        <source>File Archivist files(*.arch)</source>
        <translation>Fichier File Archivist (*.arch)</translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../info.cpp" line="143"/>
        <source>German translation &amp; additional programming</source>
        <translation>Traduction allemande &amp; aide au développement</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Spanish translation</source>
        <translation>Traduction espagnole</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="163"/>
        <source>Czech translation</source>
        <translation>Traduction Tchèque</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="456"/>
        <source>About cdcat</source>
        <translation>A propos de CdCat</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="457"/>
        <source>close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="460"/>
        <source>About Cdcat</source>
        <translation>A propos de CdCat</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="463"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="465"/>
        <source>Thanks</source>
        <translation>Remerciements</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="83"/>
        <source>-= CdCatalog by Hyper =-</source>
        <translation>-= CdCatalog by Hyper =-</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="87"/>
        <source>Version:</source>
        <translation>Version :</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="132"/>
        <source>Author:</source>
        <translation>Auteur :</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="134"/>
        <source>Copyright (C) 2003 Peter Deak (GPL)</source>
        <translation>Copyright (C) 2003 Peter Deak (GPL)</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="151"/>
        <source>AVI reader plugin &amp; Polish translation</source>
        <translation>Plugin lecture AVI &amp; traduction polonaise</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="89"/>
        <source> (with debug)</source>
        <translation> (avec déboguage)</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <source>Development version build at</source>
        <translation>Version de développement produite le</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="136"/>
        <source>Copyright (C) 2010 Christoph Thielecke (GPL)</source>
        <translation>Copyright (C) 2011 Christoph Thielecke (GPL)</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="145"/>
        <source>Ported to Qt4</source>
        <translation>porté vers Qt4</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="170"/>
        <source>For more details about the GPL license and to read in other languages, visit %1.</source>
        <translation>Pour de plus amples informations sur la licence GPL et pour une lecture dans votre langue, visitez la %1.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="170"/>
        <source>GPL page on GNU website</source>
        <translation>page GPL du site web de GNU</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="107"/>
        <source>Features:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="111"/>
        <source>archive read support using lib7zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="115"/>
        <source>mediainfo (compiled in)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="119"/>
        <source>mediainfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="123"/>
        <source>exif data read support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="127"/>
        <source>encrypted catalog support</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyBindingDialog</name>
    <message>
        <location filename="../info.cpp" line="479"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="481"/>
        <source>Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="535"/>
        <source>Key bindings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="536"/>
        <source>close</source>
        <translation type="unfinished">Fermer</translation>
    </message>
</context>
<context>
    <name>PWw</name>
    <message>
        <location filename="../adddialog.cpp" line="488"/>
        <source>Please Wait...</source>
        <translation>Merci de patienter...</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="514"/>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="588"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../hdirview.cpp" line="254"/>
        <source>Directory</source>
        <translation>Répertoire</translation>
    </message>
    <message>
        <location filename="../dirview.cpp" line="148"/>
        <source>Unreadable Directory</source>
        <translation>Répertoire illisible</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="232"/>
        <source>Catalog</source>
        <translation>Catalogue</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="236"/>
        <source>Unknown(DB)</source>
        <translation>Inconnu(DB)</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="238"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="240"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="242"/>
        <source>HardDisc</source>
        <translation>Disque Dur</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="244"/>
        <source>Floppy</source>
        <translation>Disquette</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="246"/>
        <source>NetworkDrv</source>
        <translation>Disque sur réseau</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="248"/>
        <source>FlashDrv</source>
        <translation>Disque Flash</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="250"/>
        <source>OtherDevice</source>
        <translation>Autre périphérique</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="640"/>
        <source>Video:
</source>
        <translation>Vidéo :</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="642"/>
        <source>Total Time</source>
        <translation>Durée totale</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="643"/>
        <source>Framerate</source>
        <translation>Nombre d&apos;images par seconde</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="644"/>
        <source>Resolution</source>
        <translation>Résolution</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="0"/>
        <source>Audio:</source>
        <translation type="obsolete">Audio :</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="0"/>
        <source>More Data:</source>
        <translation type="obsolete">Autres données :</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="656"/>
        <source>Artist</source>
        <translation>Interprète</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="658"/>
        <source>Comments</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="660"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="662"/>
        <source>Creation Date</source>
        <translation>Date de création</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="664"/>
        <source>Engineer</source>
        <translation>Ingénieur</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="668"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="670"/>
        <source>Product</source>
        <translation>Produit</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="672"/>
        <source>Software</source>
        <translation>Logiciel</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="674"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="676"/>
        <source>Subject</source>
        <translation>Sujet</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="678"/>
        <source>Technician</source>
        <translation>Technicien</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="763"/>
        <source>Unknown/not implemented/broken header
</source>
        <translation>Inconnu/Non implémenté/Entête détérioré
</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="648"/>
        <source>Channels</source>
        <translation>Canaux</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="650"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="666"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="492"/>
        <source>Not a RIFF/AVI file OR header broken!</source>
        <translation>Il ne s&apos;agit pas d&apos;un fichier RIFF/AVI ou Entête détérioré !
</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="649"/>
        <source>Sample/s</source>
        <translation>Echantillon(s)</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2411"/>
        <source>importuser</source>
        <translation>Propriétaire</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="646"/>
        <source>Audio:
</source>
        <translation>Audio :</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="653"/>
        <source>More Data:
</source>
        <translation>Autres données :</translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1004"/>
        <source>Enter password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1004"/>
        <source>Enter password for catalog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="230"/>
        <source>Catalog (encrypted)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1008"/>
        <source>password empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1014"/>
        <source>cant set password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1034"/>
        <source>decrypt failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1047"/>
        <source>cant load catalog: encrypted catalog support not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2976"/>
        <source>Enter password for catalog (again):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelReadable</name>
    <message>
        <location filename="../selreadable.cpp" line="830"/>
        <source>Select &apos;readable items</source>
        <translation type="obsolete">Choisir des éléments lisibles</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="848"/>
        <source>Read mp3 tags</source>
        <translation>Lire les balises mp3</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="864"/>
        <source>Store content of some files</source>
        <translation>Sauvegarder le contenu de certains fichiers</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="885"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="881"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="875"/>
        <source>content size limit in kByte</source>
        <translation>Taille maxi du contenu en kOctets</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="870"/>
        <source>; separated list of readable file patterns</source>
        <translation>Liste des motifs de fichiers lisibles séparés par des ;</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="859"/>
        <source>Default tag</source>
        <translation>Balise par défaut</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="120"/>
        <source>version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="861"/>
        <source>Read mp3 technical info as comment (bitrate,freq,length...)</source>
        <translation>Lire les infos techniques mp3 comme un commentaire (bitrate, fréquence, taille...)</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="0"/>
        <source>Read avi technical info as comment (codecs,lenhth,...)</source>
        <translation type="obsolete">Lire les infos techniques avi comme un commentaire (codec, taille...)</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="863"/>
        <source>Read avi technical info as comment (codecs,length,...)</source>
        <translation>Lire les infos techniques avi comme un commentaire (codec, taille, ...)</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="837"/>
        <source>Archive file display options</source>
        <translation>Archiver les options d&apos;affichage</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="838"/>
        <source>Scan for archive file list</source>
        <translation>Scanner une liste de fichiers d&apos;archive</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="841"/>
        <source>Supported extensions:</source>
        <translation>Extensions supportées :</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="474"/>
        <source>Show permission</source>
        <translation type="obsolete">Montrer les permissions</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="475"/>
        <source>Show user</source>
        <translation type="obsolete">Montrer l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="476"/>
        <source>Show group</source>
        <translation type="obsolete">Montrer le groupe</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="477"/>
        <source>Show size</source>
        <translation type="obsolete">Montrer la taille</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="478"/>
        <source>Show date</source>
        <translation type="obsolete">Montrer la date</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="479"/>
        <source>Show optional comment</source>
        <translation type="obsolete">Montrer les commentaires optionnels</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="868"/>
        <source>Read some technical info using mediainfo</source>
        <translation>Lire des infos techniques en utilisant mediainfo</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="850"/>
        <source>Read thumbnails from pictures</source>
        <translation>Lire les vignettes des images</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="856"/>
        <source>Read EXIF data from pictures</source>
        <translation>Lire les données EXIF des images</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="854"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="872"/>
        <source>; separated list of image file extensions, e.g. png;jpg;gif</source>
        <translation>; liste des extensions de fichiers image avec séparateur</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="622"/>
        <source>Path to external content viewer (found)</source>
        <translation>Chemin du visualiseur externe (trouvé)</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="629"/>
        <source>Path to external content viewer (not found)</source>
        <translation>Chemin du visualiseur externe (non trouvé)</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="606"/>
        <source>Choose path to external context viewer</source>
        <translation>Choisir le chemin du visualiseur externe</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="866"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="867"/>
        <source>Select external viewer...</source>
        <translation>Choix du visualiseur externe...</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="175"/>
        <source>KiB</source>
        <translation>KiO</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="413"/>
        <source>lib7zip found</source>
        <translation>lib7zip trouvé</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="417"/>
        <source>lib7zip not supported</source>
        <translation>lib7zip non supporté</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="420"/>
        <source>Archive support:</source>
        <translation>Support des archives</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="449"/>
        <source>mediainfo not found</source>
        <translation>mediainfo non trouvé</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="451"/>
        <source>mediainfo found</source>
        <translation>mediainfo trouvé</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="456"/>
        <source>mediainfo status:</source>
        <translation>Etat de mediainfo</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="842"/>
        <source>Permission</source>
        <translation>Permissions</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="843"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="844"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="845"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="846"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="847"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="849"/>
        <source>Read thumbnails</source>
        <translation>Lecture des vignettes</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="851"/>
        <source>Size:</source>
        <translation>Taille :</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="852"/>
        <source>Thumbnail size (width) in pixels</source>
        <translation>Taille (largeur) des vignettes en pixels</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="853"/>
        <source>Thumbnail size (height) in pixels</source>
        <translation>Taille (hauteur) des vignettes en pixels</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="865"/>
        <source>Use external file content viewer</source>
        <translation>Utiliser un visualiseur externe</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="869"/>
        <source>file patterns:</source>
        <translation>Extensions de fichier</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="871"/>
        <source>File extensions:</source>
        <translation>Extensions de fichier</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="874"/>
        <source>max size:</source>
        <translation>taille maxi :</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="860"/>
        <source>Read mp3 info as comment</source>
        <translation>Lire les balises mp3 comme des commentaires</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="862"/>
        <source>Read avi info as comment</source>
        <translation>Lire les balises avi comme des commentaires</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="667"/>
        <source>Add exclude rule...</source>
        <translation>Ajouter une règle d&apos;exclusion</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="667"/>
        <source>Enter regular expression for exclude:</source>
        <translation>Entrez une expression régulière d&apos;exclusion:</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="651"/>
        <source>edit rule...</source>
        <translation>Editer la règle</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="655"/>
        <source>delete rule...</source>
        <translation>Supprimer la règle</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="876"/>
        <source>exclude files/directories</source>
        <translation>exclure des fichiers/répertoires</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="883"/>
        <source>add exclude rule...</source>
        <translation>Ajouter une règle d&apos;exclusion</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="884"/>
        <source>list of patterns (regular expression) for files/directories to skip on reading from filesystem</source>
        <translation>liste des règles (expressions régulières) pour les fichiers/répertoires du système de fichier</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="695"/>
        <source>regular expression is invalid</source>
        <translation>L&apos;expression de la règle est invalide</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="699"/>
        <source>regular expression is valid</source>
        <translation>L&apos;expression de la règle est valide</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="840"/>
        <source>show archive file at scanning in status</source>
        <translation>Afficher les archives dans la barre d&apos;état durant le scan</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="839"/>
        <source>show archive file in status</source>
        <translation>Afficher les archives dans la barre d&apos;état</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="804"/>
        <source>About regular expressions</source>
        <translation>A propos des expressions régulières</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="806"/>
        <source>close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="879"/>
        <source>About regex:</source>
        <translation>A propos de regex</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="880"/>
        <source>Information about regular expressions</source>
        <translation>Information sur les expressions régulières</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="881"/>
        <source>About regular expressions....</source>
        <translation>A propos des expressions régulières....</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="882"/>
        <source>Show introduction into regular expressions</source>
        <translation>Afficher l&apos;introduction aux expressions régulières</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="744"/>
        <source>Metacharacter</source>
        <translation>Méta-caractère</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="745"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="747"/>
        <source>Matches any single character (many applications exclude &lt;a href=&quot;/wiki/Newline&quot; title=&quot;Newline&quot;&gt;newlines&lt;/a&gt;, and exactly which characters are considered newlines is flavor-, character-encoding-, and platform-specific, but it is safe to assume that the line feed character is included). Within POSIX bracket expressions, the dot character matches a literal dot. For example, &lt;code&gt;a.c&lt;/code&gt; matches &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, etc., but &lt;code&gt;[a.c]&lt;/code&gt; matches only &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;.&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;.</source>
        <translation type="obsolete">correspond à tout caractère simple (de nombreuses applications excluent les &lt;a href=&quot;/wiki/Newline&quot; title=&quot;Nouvelle ligne&quot;&gt;nouvelles lignes&lt;/a&gt;, mais quels sont exactement les caractères qui sont considérés comme des sauts de ligne-, ils dépendent du codage de caractère-, et sont spécifiques à une plateforme. Il est donc plus sûr d&apos;assumer que le caractère saut de ligne est inclus). Dans les expressions POSIX avec crochet, le point correspond litéralement à un point. Par exemple, &lt;code&gt;a.c&lt;/code&gt; correspond à &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, etc., mais &lt;code&gt;[a.c]&lt;/code&gt; correspond seulement à &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;.&lt;/i&gt;&quot;, ou &quot;&lt;i&gt;c&lt;/i&gt;&quot;.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="753"/>
        <source>A bracket expression. Matches a single character that is contained within the brackets. For example, &lt;code&gt;[abc]&lt;/code&gt; matches &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;. &lt;code&gt;[a-z]&lt;/code&gt; specifies a range which matches any lowercase letter from &quot;&lt;i&gt;a&lt;/i&gt;&quot; to &quot;&lt;i&gt;z&lt;/i&gt;&quot;. These forms can be mixed: &lt;code&gt;[abcx-z]&lt;/code&gt; matches &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, &quot;&lt;i&gt;c&lt;/i&gt;&quot;, &quot;&lt;i&gt;x&lt;/i&gt;&quot;, &quot;&lt;i&gt;y&lt;/i&gt;&quot;, or &quot;&lt;i&gt;z&lt;/i&gt;&quot;, as does &lt;code&gt;[a-cx-z]&lt;/code&gt;.</source>
        <translation>Expression entre crochet. Correspond à un caractère simple qui est contenu entre les crochets. Par exemple, &lt;code&gt;[abc]&lt;/code&gt; correspond à &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, ou &quot;&lt;i&gt;c&lt;/i&gt;&quot;. &lt;code&gt;[a-z]&lt;/code&gt; spécifie un intervalle qui correspond à chaque lettre minuscule de &quot;&lt;i&gt;a&lt;/i&gt;&quot; à &quot;&lt;i&gt;z&lt;/i&gt;&quot;. Ces formes peuvent être mélangées : &lt;code&gt;[abcx-z]&lt;/code&gt; correspond à &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, &quot;&lt;i&gt;c&lt;/i&gt;&quot;, &quot;&lt;i&gt;x&lt;/i&gt;&quot;, &quot;&lt;i&gt;y&lt;/i&gt;&quot;, ou &quot;&lt;i&gt;z&lt;/i&gt;&quot;, tout comme &lt;code&gt;[a-cx-z]&lt;/code&gt;.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="754"/>
        <source>The &lt;code&gt;-&lt;/code&gt; character is treated as a literal character if it is the last or the first (after the &lt;code&gt;^&lt;/code&gt;) character within the brackets: &lt;code&gt;[abc-]&lt;/code&gt;, &lt;code&gt;[-abc]&lt;/code&gt;. Note that backslash escapes are not allowed. The &lt;code&gt;]&lt;/code&gt; character can be included in a bracket expression if it is the first (after the &lt;code&gt;^&lt;/code&gt;) character: &lt;code&gt;[]abc]&lt;/code&gt;.</source>
        <translation>Le caractère &lt;code&gt;-&lt;/code&gt; est traité comme un caractère litéral s&apos;il est le dernier ou le premier caractère (après le &lt;code&gt;^&lt;/code&gt;) à l&apos;intérieur des crochets: &lt;code&gt;[abc-]&lt;/code&gt;, &lt;code&gt;[-abc]&lt;/code&gt;. Remarque: les caractères d&apos;échappement anti-slash ne sont pas autorisés. Le caractère &lt;code&gt;]&lt;/code&gt; peut être inclus dans une expression entre crochets s&apos;il est le premier caractère (après le &lt;code&gt;^&lt;/code&gt;): &lt;code&gt;[]abc]&lt;/code&gt;.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="759"/>
        <source>Matches a single character that is not contained within the brackets. For example, &lt;code&gt;[^abc]&lt;/code&gt; matches any character other than &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;. &lt;code&gt;[^a-z]&lt;/code&gt; matches any single character that is not a lowercase letter from &quot;&lt;i&gt;a&lt;/i&gt;&quot; to &quot;&lt;i&gt;z&lt;/i&gt;&quot;. Likewise, literal characters and ranges can be mixed.</source>
        <translation>Correspond à un caractère simple qui n&apos;est pas contenu à l&apos;intérieur des crochets. Par exemple, &lt;code&gt;[^abc]&lt;/code&gt; correspond à chaque caractère autre que &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, ou &quot;&lt;i&gt;c&lt;/i&gt;&quot;. &lt;code&gt;[^a-z]&lt;/code&gt; correspond à chaque caractère qui n&apos;est pas une lettre minuscule de &quot;&lt;i&gt;a&lt;/i&gt;&quot; vers &quot;&lt;i&gt;z&lt;/i&gt;&quot;. Les caractères litéraux et les plages peuvent également être mélangées.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="763"/>
        <source>Matches the starting position within the string. In line-based tools, it matches the starting position of any line.</source>
        <translation>Correspond à la position de départ dans la chaîne. Dans les outils basés sur les lignes, il correspond à la position de départ de chaque ligne.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="776"/>
        <source>Matches what the &lt;i&gt;n&lt;/i&gt;th marked subexpression matched, where &lt;i&gt;n&lt;/i&gt; is a digit from 1 to 9. This construct is theoretically &lt;b&gt;irregular&lt;/b&gt; and was not adopted in the POSIX ERE syntax. Some tools allow referencing more than nine capturing groups.</source>
        <translation>Correspond à la &lt;i&gt;n&lt;/i&gt;ème sous-chaîne qui répond au critère, où &lt;i&gt;n&lt;/i&gt; est un digit de 1 à 9. Sa construction, est théoriquement &lt;b&gt;irregulière&lt;/b&gt; et n&apos;a pas été adoptée dans la syntaxe POSIX ERE. Quelques outils permettent de référencer plus de neuf groupes de capture.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="780"/>
        <source>Matches the preceding element zero or more times. For example, &lt;code&gt;ab*c&lt;/code&gt; matches &quot;&lt;i&gt;ac&lt;/i&gt;&quot;, &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, &quot;&lt;i&gt;abbbc&lt;/i&gt;&quot;, etc. &lt;code&gt;[xyz]*&lt;/code&gt; matches &quot;&quot;, &quot;&lt;i&gt;x&lt;/i&gt;&quot;, &quot;&lt;i&gt;y&lt;/i&gt;&quot;, &quot;&lt;i&gt;z&lt;/i&gt;&quot;, &quot;&lt;i&gt;zx&lt;/i&gt;&quot;, &quot;&lt;i&gt;zyx&lt;/i&gt;&quot;, &quot;&lt;i&gt;xyzzy&lt;/i&gt;&quot;, and so on. &lt;code&gt;\(ab\)*&lt;/code&gt; matches &quot;&quot;, &quot;&lt;i&gt;ab&lt;/i&gt;&quot;, &quot;&lt;i&gt;abab&lt;/i&gt;&quot;, &quot;&lt;i&gt;ababab&lt;/i&gt;&quot;, and so on.</source>
        <translation>Correspond à l&apos;élément précédent, zero ou plusieurs fois. Par exemple, &lt;code&gt;ab*c&lt;/code&gt; correspond à &quot;&lt;i&gt;ac&lt;/i&gt;&quot;, &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, &quot;&lt;i&gt;abbbc&lt;/i&gt;&quot;, etc. &lt;code&gt;[xyz]*&lt;/code&gt; correspond à &quot;&quot;, &quot;&lt;i&gt;x&lt;/i&gt;&quot;, &quot;&lt;i&gt;y&lt;/i&gt;&quot;, &quot;&lt;i&gt;z&lt;/i&gt;&quot;, &quot;&lt;i&gt;zx&lt;/i&gt;&quot;, &quot;&lt;i&gt;zyx&lt;/i&gt;&quot;, &quot;&lt;i&gt;xyzzy&lt;/i&gt;&quot;, et ainsi de suite. &lt;code&gt;\(ab\)*&lt;/code&gt; correspond à &quot;&quot;, &quot;&lt;i&gt;ab&lt;/i&gt;&quot;, &quot;&lt;i&gt;abab&lt;/i&gt;&quot;, &quot;&lt;i&gt;ababab&lt;/i&gt;&quot;, et ainsi de suite.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="788"/>
        <source>Examples:</source>
        <translation>Exemples :</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="790"/>
        <source>matches any three-character string ending with &quot;at&quot;, including &quot;&lt;i&gt;hat&lt;/i&gt;&quot;, &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, and &quot;&lt;i&gt;bat&lt;/i&gt;&quot;.</source>
        <translation>correspond à chaque chaine de 3 caractères finissant par &quot;at&quot;, incluant &quot;&lt;i&gt;hat&lt;/i&gt;&quot;, &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, et &quot;&lt;i&gt;bat&lt;/i&gt;&quot;.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="765"/>
        <source>Matches the ending position of the string or the position just before a string-ending newline. In line-based tools, it matches the ending position of any line.</source>
        <translation type="obsolete">Correspond à la position de la dernière chaîne juste avant la chaine de fin de ligne. Dans les outils basé ligne, celà correspond à la position de la fin de ligne.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="791"/>
        <source>matches &quot;&lt;i&gt;hat&lt;/i&gt;&quot; and &quot;&lt;i&gt;cat&lt;/i&gt;&quot;.</source>
        <translation>correspond à &quot;&lt;i&gt;hat&lt;/i&gt;&quot; et &quot;&lt;i&gt;cat&lt;/i&gt;&quot;.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="792"/>
        <source>matches all strings matched by &lt;code&gt;.at&lt;/code&gt; except &quot;&lt;i&gt;bat&lt;/i&gt;&quot;.</source>
        <translation>correspond à chaque chaîne répondant à l&apos;expression &lt;code&gt;.at&lt;/code&gt; sauf &quot;&lt;i&gt;bat&lt;/i&gt;&quot;.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="793"/>
        <source>matches &quot;&lt;i&gt;hat&lt;/i&gt;&quot; and &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, but only at the beginning of the string or line.</source>
        <translation>correspond à &quot;&lt;i&gt;hat&lt;/i&gt;&quot; et &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, mais seulement en début de chaîne ou de ligne.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="794"/>
        <source>matches &quot;&lt;i&gt;hat&lt;/i&gt;&quot; and &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, but only at the end of the string or line.</source>
        <translation>correspond à &quot;&lt;i&gt;hat&lt;/i&gt;&quot; et &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, mais seulement en fin de chaîne ou de ligne.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="798"/>
        <source>Source:</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="795"/>
        <source>matches any single character surrounded by &quot;[&quot; and &quot;]&quot; since the brackets are escaped, for example: &quot;&lt;i&gt;[a]&lt;/i&gt;&quot; and &quot;&lt;i&gt;[b]&lt;/i&gt;&quot;.</source>
        <translation>correspond à chaque caractère simple entouré par les crochets &quot;[&quot; et &quot;]&quot; puisque les crochets sont échappés, par exemple: &quot;&lt;i&gt;[a]&lt;/i&gt;&quot; et &quot;&lt;i&gt;[b]&lt;/i&gt;&quot;.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="707"/>
        <source>POSIX Basic Regular Expressions</source>
        <translation>Expressions régulières POSIX de base&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="877"/>
        <source>Use wildcard instead regex</source>
        <translation>Utiliser les jokers au lieu des expressions régulières</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="878"/>
        <source>Use wildcard expression instead regular expression</source>
        <translation>Utiliser les expressions avec jokers au lieu des expressions régulières</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="406"/>
        <source>lib7zip not found</source>
        <translation>lib7zip non trouvé</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="411"/>
        <source>unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="457"/>
        <source>support not compiled in</source>
        <translation>support non compilé pour</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="455"/>
        <source>mediainfo not supported</source>
        <translation>mediainfo non supporté</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="873"/>
        <source>Supported image extensions found: </source>
        <translation>Extensions image supportées trouvées : </translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="836"/>
        <source>Select readable items</source>
        <translation>Choisir des éléments lisibles</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="886"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="749"/>
        <source>Matches any single character (many applications exclude &lt;a href=&quot;http://en.wikipedia.org/wiki/Newline&quot; title=&quot;Newline&quot;&gt;newlines&lt;/a&gt;, and exactly which characters are considered newlines is flavor-, character-encoding-, and platform-specific, but it is safe to assume that the line feed character is included). Within POSIX bracket expressions, the dot character matches a literal dot. For example, &lt;code&gt;a.c&lt;/code&gt; matches &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, etc., but &lt;code&gt;[a.c]&lt;/code&gt; matches only &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;.&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;.</source>
        <translation>correspond à tout caractère simple (de nombreuses applications excluent les &lt;a href=&quot;http://en.wikipedia.org/wiki/Newline&quot; title=&quot;Nouvelle ligne&quot;&gt;nouvelles lignes&lt;/a&gt;, mais quels sont exactement les caractères qui sont considérés comme des sauts de ligne-, ils dépendent du codage de caractère-, et sont spécifiques à une plateforme. Il est donc plus sûr d&apos;assumer que le caractère saut de ligne est inclus). Dans les expressions POSIX avec crochet, le point correspond litéralement à un point. Par exemple, &lt;code&gt;a.c&lt;/code&gt; correspond à &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, etc., mais &lt;code&gt;[a.c]&lt;/code&gt; correspond seulement à &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;.&lt;/i&gt;&quot;, ou &quot;&lt;i&gt;c&lt;/i&gt;&quot;.</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="798"/>
        <source>&lt;a href=&quot;http://en.wikipedia.org/wiki/Regex&quot;&gt;regular expressions&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://en.wikipedia.org/wiki/Regex&quot;&gt;expressions régulières&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="800"/>
        <source>From</source>
        <translation>De</translation>
    </message>
</context>
<context>
    <name>ShowContent</name>
    <message>
        <location filename="../showcontent.cpp" line="116"/>
        <source>Content of %1</source>
        <translation>Contenu de %1</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="206"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="252"/>
        <source>Confirmation</source>
        <translation>Confirmation</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="255"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="255"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="71"/>
        <source>Remove the file content from the database. (Warning: Unrecoverable!)</source>
        <translation>Supprimer le contenu du fichier de la base de données (Attention : annulation impossible !)</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="61"/>
        <source>%1 kByte</source>
        <translation>%1 kOctet</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="75"/>
        <source>Save this content to a new file</source>
        <translation>Sauvegarder ce contenu dans un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="0"/>
        <source>Save content...</source>
        <translation type="obsolete">Sauvegarder le contenu...</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="229"/>
        <source>Select a filename below</source>
        <translation>Choisir un nom de fichier ci-dessous</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="233"/>
        <source>I can&apos;t rewrite the file: %1</source>
        <translation>Impossible d&apos;écraser le fichier : %1</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="234"/>
        <source>Error while saving...</source>
        <translation>Erreu durantla sauvegarde...</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="254"/>
        <source>Sure to delete this file&apos;s content from the database?</source>
        <translation>Vraiment supprimer le contenu de ce fichier de la base de données ?</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="92"/>
        <source>Category of %1</source>
        <translation>Catégorie de %1</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="202"/>
        <source>Set category</source>
        <translation>Choisir la catégorie</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="204"/>
        <source>Set content</source>
        <translation>Insérer le contenu</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="229"/>
        <source>CdCat databases (*.hcf )</source>
        <translation>Bases de données CdCat (*.hcf)</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="205"/>
        <source>select font for display</source>
        <translation>choix de la police d&apos;affichage</translation>
    </message>
</context>
<context>
    <name>addDialog</name>
    <message>
        <location filename="../adddialog.cpp" line="77"/>
        <source>Directory Browser</source>
        <translation>Explorateur de répertoire</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="323"/>
        <source>New Disk %1</source>
        <translation>Nouveau disque %1</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="245"/>
        <source>Add Media to the Database</source>
        <translation>Ajouter un média à la base de données</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="246"/>
        <source>Choose a directory to scan:</source>
        <translation>Répertoire à scanner :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="0"/>
        <source>Media Name:</source>
        <translation type="obsolete">Nom du média :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="0"/>
        <source>Serial number of Media:</source>
        <translation type="obsolete">Numéro de série du média :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="0"/>
        <source>Owner:</source>
        <translation type="obsolete">Propriétaire :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="0"/>
        <source>Type:</source>
        <translation type="obsolete">Type :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="0"/>
        <source>Comment:</source>
        <translation type="obsolete">Commentaire :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="656"/>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="0"/>
        <source>OK / Scan</source>
        <translation type="obsolete">OK / Scanner</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="261"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="262"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="263"/>
        <source>HardDisc</source>
        <translation>Disque Dur</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="264"/>
        <source>Floppy</source>
        <translation>Disquette</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="265"/>
        <source>NetworkPlace</source>
        <translation>Emplacement réseau</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="266"/>
        <source>FlashDrive</source>
        <translation>Disque Flash</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="267"/>
        <source>OtherDevice</source>
        <translation>Autre périphérique</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="398"/>
        <source>Error:</source>
        <translation>Erreur :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="367"/>
        <source>You must be fill the &quot;Name&quot; field!</source>
        <translation>Le champ &quot;Nom&quot; doit être renseigné !</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="657"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="343"/>
        <source>The Media Name must be unique! Please change it!</source>
        <translation type="obsolete">Nom du média (attention, il doit être unique !</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="255"/>
        <source>Select &amp;readable items</source>
        <translation>Choisir des éléments lisibles</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="393"/>
        <source>The Value of Serial Number must be unique! Please change it!</source>
        <translation>Le numéro de série doit être unique ! Merci de le modifier !</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="398"/>
        <source>You haven&apos;t selected a directory! Please select one!</source>
        <translation>Vous n&apos;avez pas choisi de répertoire ! Merci d&apos;en choisir un !</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="372"/>
        <source>The media name can&apos;t begin with the &quot;@&quot; character!</source>
        <translation>Le nom du média ne peut pas commencer avec le caractère &quot;@&quot; !</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="247"/>
        <source>Media &amp;Name:</source>
        <translation>&amp;Nom du média :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="248"/>
        <source>S&amp;erial number of Media:</source>
        <translation>Numéro de s&amp;érie du média :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="249"/>
        <source>&amp;Owner:</source>
        <translation>&amp;Propriétaire :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="250"/>
        <source>Category:</source>
        <translation type="obsolete">Catégorie :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="251"/>
        <source>&amp;Type:</source>
        <translation>&amp;Type :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="252"/>
        <source>Co&amp;mment:</source>
        <translation>Co&amp;mmentaire :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="253"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="254"/>
        <source>&amp;Scan</source>
        <translation>&amp;Scanner</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="255"/>
        <source>Select readable items</source>
        <translation type="obsolete">Choisir des éléments lisibles</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="653"/>
        <source>Add a &quot;Catalog Link&quot; to the database</source>
        <translation type="obsolete">Ajouter un &quot;Lien Catalogue&quot; à la base de données</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="654"/>
        <source>Location of the .hcf file:</source>
        <translation type="obsolete">Emplacement du fichier .hcf :</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="674"/>
        <source>Open a file...</source>
        <translation type="obsolete">Ouvrir un fichier...</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="674"/>
        <source>CdCat databases (*.hcf )</source>
        <translation type="obsolete">Bases de données CdCat (*.hcf)</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="383"/>
        <source>Enter media name...</source>
        <translation>Donnez le nom du média</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="260"/>
        <source>unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="250"/>
        <source>C&amp;ategory:</source>
        <translation>C&amp;atégorie</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="257"/>
        <source>detect CDROM/DVD med&amp;ia name after mount</source>
        <translation>Détecter le nom des CD/DVD après insertion</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="383"/>
        <source>The Media Name must be unique! Enter new media name:</source>
        <translation>Le nom du média doit être unique! Donnez le nom du média :</translation>
    </message>
</context>
<context>
    <name>borrowDialog</name>
    <message>
        <location filename="../borrow.cpp" line="116"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="117"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="75"/>
        <source>I borrow the &quot;%1&quot; named media to:</source>
        <translation>J&apos;emprunte le média nommé &quot;%1&quot; à :</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="87"/>
        <source>unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="115"/>
        <source>I borrow the &quot;&quot; named media to:</source>
        <translation>J&apos;emprunte le média nommé &quot;&quot; à :</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="114"/>
        <source>Borrowing...</source>
        <translation>Emprunt...</translation>
    </message>
</context>
<context>
    <name>borrowingDialog</name>
    <message>
        <location filename="../borrow.cpp" line="211"/>
        <source>Media borrowing info:</source>
        <translation>Info empruntée :</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="248"/>
        <source>Media</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="249"/>
        <source>Borrowed</source>
        <translation>Emprunté</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="250"/>
        <source>where is it now?</source>
        <translation>Localisation</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="220"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="256"/>
        <source>0</source>
        <translation type="obsolete">0</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="456"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="463"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="325"/>
        <source>unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="329"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="329"/>
        <source>Set &quot;Yes&quot; or &quot;No&quot; !</source>
        <translation>Indiquer &quot;Oui&quot; ou &quot;Non&quot; !</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="391"/>
        <source>Warning</source>
        <translation>Alerte</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="352"/>
        <source>Do you save the changes?</source>
        <translation>Sauvegarder les modifications ?</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="391"/>
        <source>Are you sure want to clear all borrow?</source>
        <translation>Etes vous sûr de vouloir effacer toute l&apos;info empruntée ?</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="422"/>
        <source>I got it back!</source>
        <translation>Je l&apos;ai récupéré !</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="427"/>
        <source>&lt;&lt; </source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="210"/>
        <source>Borrowing info...</source>
        <translation>Info empruntée...</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="212"/>
        <source>Show only borrowed items</source>
        <translation>ne montrer que les éléments empruntés</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="213"/>
        <source>Clear all borrowing info</source>
        <translation>Effacer toute l&apos;info empruntée</translation>
    </message>
</context>
<context>
    <name>commentEdit</name>
    <message>
        <location filename="../borrow.cpp" line="0"/>
        <source>Edit Comment</source>
        <translation type="obsolete">Editer le commentaire</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1059"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1060"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1056"/>
        <source>Edit comment of</source>
        <translation>Editer le commentaire de</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1058"/>
        <source>Edit category of</source>
        <translation>Editer la catégorie de</translation>
    </message>
</context>
<context>
    <name>exportCdcatDB</name>
    <message>
        <location filename="../exportcdcatdb.cpp" line="310"/>
        <source>Availiable media</source>
        <translation>Médias disponibles</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="317"/>
        <source>Media to export</source>
        <translation>Medias à exporter</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="318"/>
        <source>All media</source>
        <translation>Tous les médias</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="326"/>
        <source>separator:</source>
        <translation>Séparateur :</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="327"/>
        <source>File to export:</source>
        <translation>Exporter dans le fichier :</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="328"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="329"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="330"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="398"/>
        <source>Filename missing</source>
        <translation>Nom de fichier absent</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="398"/>
        <source>Please enter a filename!</source>
        <translation>Renseigner le nom de fichier !</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="401"/>
        <source>Separator missing</source>
        <translation>Séparateur non renseigné</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="401"/>
        <source>Please enter a separator!</source>
        <translation>renseigner le séparateur !</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="0"/>
        <source>Error during write config...</source>
        <translation type="obsolete">Erreur en écrivant la configuration...</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="713"/>
        <source>All files (*.*)</source>
        <translation>Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="0"/>
        <source>file chooser</source>
        <translation type="obsolete">Sélecteur de fichier</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="713"/>
        <source>Choose a file!</source>
        <translation>Choisir un fichier !</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="315"/>
        <source>Add media</source>
        <translation>Ajouter un média</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="313"/>
        <source>Remove media</source>
        <translation>Supprimer un média</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="321"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="322"/>
        <source>export as HTML</source>
        <translation>Exporter en HTML</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="323"/>
        <source>export as CSV</source>
        <translation>Exporter en CSV</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="482"/>
        <source>Media</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="467"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="509"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="514"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="610"/>
        <source>I can&apos;t create or rewrite the file</source>
        <translation>Impossible de créer ou d&apos;écraser le fichier</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="441"/>
        <source>Export from cdcat:</source>
        <translation>Exporter de CdCat :</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="60"/>
        <source>Export database...</source>
        <translation>Exporter la base...</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="163"/>
        <source>What to export</source>
        <translation>Données à exporter</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="0"/>
        <source>Only media name</source>
        <translation type="obsolete">Nom du média uniquement</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="494"/>
        <source>Media name</source>
        <translation>Nom du média</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="504"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="519"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="341"/>
        <source>HTML headline</source>
        <translation>Entête HTML</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="0"/>
        <source>table header</source>
        <translation type="obsolete">Entête de tableau</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="549"/>
        <source>Overwrite?</source>
        <translation>Ecraser ?</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="550"/>
        <source>Do you want overwrite this file?</source>
        <translation>Voulez-vous écraser ce fichier ?</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="555"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="556"/>
        <source>Discard</source>
        <translation>Abandonner</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="369"/>
        <source>Export CdCat database: </source>
        <translation>Exporter la base CdCat :</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="319"/>
        <source>Only media</source>
        <translation>Uniquement les médias</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="324"/>
        <source>export as XML</source>
        <translation>Exporter en XML</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="499"/>
        <source>Media number</source>
        <translation>Numéro du média</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="532"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="339"/>
        <source>export mp3 tag</source>
        <translation>Exporter les balises mp3</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="340"/>
        <source>export borrow information</source>
        <translation>Exporter les informations de prêt</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="342"/>
        <source>table header/comment line</source>
        <translation>En-tête de tableau/ligne de commentaire</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="489"/>
        <source>Export from cdcat, catalog:</source>
        <translation>Exporter de CdCat, catalogue :</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="490"/>
        <source>Generated at:</source>
        <translation>Créé à :</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="465"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="524"/>
        <source>MP3 tag</source>
        <translation>Balise MP3</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="528"/>
        <source>Borrow</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="491"/>
        <source>field list</source>
        <translation>liste de champs</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="561"/>
        <source>Exporting, please wait...</source>
        <translation>Exportation, merci de patienter...</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="610"/>
        <source>Error during write export...</source>
        <translation>Erreur durant l&apos;écriture de l&apos;exportation...</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1043"/>
        <source>Artist:</source>
        <translation>Interprète:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1044"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1045"/>
        <source>Album:</source>
        <translation>Album :</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1046"/>
        <source>Year:</source>
        <translation>Année :</translation>
    </message>
</context>
<context>
    <name>findDialog</name>
    <message>
        <location filename="../find.cpp" line="927"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="928"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="930"/>
        <source>Media</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="931"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="932"/>
        <source>Modification</source>
        <translation>Modification</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="525"/>
        <source>Search in the database...</source>
        <translation>Rechercher dans la base...</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="567"/>
        <source>Close / Go to selected</source>
        <translation>Fermer / Aller à l&apos;élément sélectionné</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="539"/>
        <source>Find:</source>
        <translation>Rechercher :</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="526"/>
        <source>Seek in:</source>
        <translation>Rechercher dans :</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="527"/>
        <source>Owner:</source>
        <translation>Propriétaire :</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="933"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="874"/>
        <source>File name</source>
        <translation>Nom de fichier</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="886"/>
        <source>mp3-tag Comment</source>
        <translation>Balise mp3 Commentaire</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="884"/>
        <source>mp3-tag Album</source>
        <translation>Balise mp3 Album</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="882"/>
        <source>mp3-tag Title</source>
        <translation>Balise mp3 Titre</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="880"/>
        <source>mp3-tag Artist</source>
        <translation>Balise mp3 Interprète</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="859"/>
        <source>Case sensitive</source>
        <translation>Sensible à la casse</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="851"/>
        <source>Use easy matching instead regex</source>
        <translation>Utiliser des motifs simples plutôt que des regex</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="652"/>
        <source>All/Everybody</source>
        <translation>Tout / Tout le monde</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="656"/>
        <source>All media</source>
        <translation>Tous les médias</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="0"/>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="562"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="518"/>
        <source>Results</source>
        <translation type="obsolete">Résultats</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="878"/>
        <source>Content</source>
        <translation>Contenu</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="872"/>
        <source>Media / Directory name</source>
        <translation>Nom du média / Répertoire</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="155"/>
        <source>Byte</source>
        <translation>Octet</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="156"/>
        <source>KiB</source>
        <translation>KiO</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="157"/>
        <source>MiB</source>
        <translation>MiO</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="158"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="929"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="905"/>
        <source>Date start</source>
        <translation>Date de début</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="909"/>
        <source>Date end</source>
        <translation>Date de fin</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="913"/>
        <source>Min size</source>
        <translation>Taille minimale</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="917"/>
        <source>Max size</source>
        <translation>Taille maximale</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="893"/>
        <source>Unsharp search (slow)</source>
        <translation>Recherche sans indexation (lent)</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="555"/>
        <source>Find in archives too</source>
        <translation>Chercher dans les archives également</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="557"/>
        <source>&amp;Start search</source>
        <translation>&amp;Lancer la recherche</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="568"/>
        <source>Print result...</source>
        <translation>Imprimer les résultats</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="569"/>
        <source>Export result to HTML...</source>
        <translation>Exporter les résultats en HTML...</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="815"/>
        <source>Print cdcat result</source>
        <translation>Imprimer les résultats cdcat</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="820"/>
        <source>Result file name</source>
        <translation>Nom du fichier résultat</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="822"/>
        <source>Filename missing</source>
        <translation>Nom de fichier absent</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="822"/>
        <source>Please enter a filename!</source>
        <translation>Renseigner le nom de fichier !</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="839"/>
        <source>Cdcat search result</source>
        <translation>résultat de la recherche cdcat</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="840"/>
        <source>catalog</source>
        <translation>catalogue</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="840"/>
        <source>created at:</source>
        <translation>créé le :</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="843"/>
        <source>used search options</source>
        <translation>options de recherche utilisées</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="847"/>
        <source>search pattern:</source>
        <translation>règle de recherche</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="895"/>
        <source>on</source>
        <translation>activé</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="897"/>
        <source>off</source>
        <translation>désactivé</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="869"/>
        <source>Search in</source>
        <translation>Recherche dans</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="889"/>
        <source>archives</source>
        <translation>archives</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="903"/>
        <source>other options</source>
        <translation>autres options</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="993"/>
        <source>File cant open: </source>
        <translation>Ouverture de fichier impossible :</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="993"/>
        <source>Cant open file</source>
        <translation>Ouverture de fichier impossible</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="497"/>
        <source>Search for duplicates in the database...</source>
        <translation>Recherche de doublons dans la base de données</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="518"/>
        <source>Duplicates for:</source>
        <translation>Doublons :</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="519"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="566"/>
        <source>Results: search not started</source>
        <translation>Résultats : recherche non démarrée</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="537"/>
        <source>Extension</source>
        <translation>Extension</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="159"/>
        <source>TiB</source>
        <translation>TiO</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="540"/>
        <source>Keep search result</source>
        <translation>Conserver les résultats de la recherche</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="556"/>
        <source>Clear search results</source>
        <translation>Effacer les résultats des recherches</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="934"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="558"/>
        <source>Category for find</source>
        <translation>Catégorie recherchée</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="704"/>
        <source>Selected dir: </source>
        <translation>Répertoire sélectionné :</translation>
    </message>
</context>
<context>
    <name>findDuplicatesDialog</name>
    <message>
        <location filename="../find.cpp" line="897"/>
        <source>Name</source>
        <translation type="obsolete">Nom</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="898"/>
        <source>Type</source>
        <translation type="obsolete">Type</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="901"/>
        <source>Path</source>
        <translation type="obsolete">Chemin</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="902"/>
        <source>Modification</source>
        <translation type="obsolete">Modification</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="903"/>
        <source>Comment</source>
        <translation type="obsolete">Commentaire</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="905"/>
        <source>&amp;Start search</source>
        <translation type="obsolete">&amp;Lancer la recherche</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="909"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="914"/>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
</context>
<context>
    <name>helpDialog</name>
    <message>
        <location filename="../help.ui" line="13"/>
        <source>help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../help.ui" line="65"/>
        <source>close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../help.ui" line="29"/>
        <source>&lt;p align=&quot;center&quot;&gt;&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;Help&lt;/b&gt;&lt;/font&gt;&lt;/p&gt;&lt;br&gt;

&lt;b&gt;What&apos;s this?&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
The cdcat is graphical (QT based) multiplatform (Linux/Windows) catalog program which scans the directories/drives you want and memorize the filesystem including the tags of mp3&apos;s and other data and store it in a small file.
The database is stored in a gzipped XML format, so you can hack it, or use it if necessary :-)
And the program can store the content of some specified files up to a limit size if you want. (for example: *.nfo)
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Usage:&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
&lt;i&gt;Create a new catalog&lt;/i&gt;: Run the &lt;tt&gt;New&lt;/tt&gt; command in the catalog menu. You have to type the name of the new catalog. You can specify the default username of the media(which you scan later), and add a comment to the catalog.&lt;br&gt;Before the scanning select the necessary readable components in the config dialog, which can be mp3 tags content of some files or etc.  If you done this, you can scan one of your media with &lt;tt&gt;Add media&lt;/tt&gt;command in the Edit menu. In that dialog you have to specyfi the directory/or drive the media you want to add. It&apos;s recommended to specify the name and the number of the media which has to be unique. (The program always generate one identical name and number) You can label the media to a owner, if necessary.
&lt;br&gt;If you scanned your media, you will be able to browse in it with the browser window (like mc) , or search in it. You can save the catalog with &lt;tt&gt;save as&lt;/tt&gt; command in the Catalog menu.
&lt;br&gt;
&lt;br&gt;
&lt;i&gt;Open an existing catalog:&lt;/i&gt;Run the &lt;tt&gt;open&lt;/tt&gt; command in the Catalog menu, and choice the file of the catalog. (*.hcf)  After the opening you will be able browse the catalog or search in it.
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Author:&lt;/b&gt;
&lt;blockquote&gt;The program was written by  Peter Deak  (hungary)&lt;br&gt;
E-mail: hyperr@freemail.hu
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;License:&lt;/b&gt;
&lt;blockquote&gt;General Public License (GPL)&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Homepage:&lt;/b&gt;
&lt;blockquote&gt;You can read about the program and get new versions, sources etc, in the hompage of cdcat:&lt;br&gt;&lt;tt&gt;http://cdcat.sourceforge.net&lt;/tt&gt;&lt;/blockquote&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;Aide&lt;/b&gt;&lt;/font&gt;&lt;/p&gt;&lt;br&gt;

&lt;b&gt;What&apos;s this?&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
CdCat est un programme graphique (basé sur QT) multiplateforme (Linux / Windows) qui scanne les répertoires/disques que vous choisissez et mémorise le système de fichier (y compris les balises mp3 et autres) pour le sauvegarder dans un fichier de taille réduite.
La base de données est sauvegardée dans un fichier XML gzippé, de façon à ce que vous puissiez le lire et l&apos;utiliser si souhaité :-)
Par ailleurs le programme peut sauvegarder le contenu de certains types de fichiers avec une taille limitee que vous choisissez (par exemple : *.nfo).
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Utilisation :&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
&lt;i&gt;Créer un nouveau Catalogue&lt;/i&gt;: Exécutez la commande &lt;tt&gt;Nouveau&lt;/tt&gt; dans le menu Catalogue. Vous devez indiquer le nom de ce nouveau Catalogue. Vous pouvez préciser à ce niveau le propriétaire par défaut du média (que vous scannerez par la suite et ajouter un commentaire.&lt;br&gt;Avant de scanner, choisissez dans la boite de dialogue de Configuration les composants lisibles : il peut s&apos;agir de balises mp3 , du contenu de certains types de fichiers etc. Arrivé à ce point, vous pouvez scanner un de vos médias avec la commande &lt;tt&gt;Ajouter un média&lt;/tt&gt; dans le menu Edition menu. Dans cette boite de dialogue vous devez préciser le répertoire ou disque que vous souhaitez scanner. Il est recommandé de préciser le nom et le numéro du média qui doit être unique (le programme génèrera toujours un nom et numéro identiques). Vous pouvez associer le média à un propriétaire, si souhaité.
&lt;br&gt;Une fois scanné, vous pourrez naviguer dedans grâce au navigateur (de type MC) ou faire des recherches. Vous pouvez sauvegarder le Catalogue avec la commande &lt;tt&gt;Enregistrer sous... &lt;/tt&gt; dans le menu Catalogue.
&lt;br&gt;
&lt;br&gt;
&lt;i&gt;Ouvrir un Catalogue existant :&lt;/i&gt;Exécutez la commande &lt;tt&gt;Ouvrir&lt;/tt&gt; dans le menu Catalogue, et choisissez le ficher Catalogue (*.hcf).  Une fois ouvert, vous pourrez naviguer dedans grâce au navigateur (de type MC) ou faire des recherches.
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Auteur :&lt;/b&gt;
&lt;blockquote&gt;Le proramme a été écrit par  Peter Deak  (Hongrie)&lt;br&gt;
E-mail: hyperr@freemail.hu
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Licence :&lt;/b&gt;
&lt;blockquote&gt;General Public License (GPL)&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Page d&apos;accueil :&lt;/b&gt;
&lt;blockquote&gt;Plus d&apos;informations sur le programme (nouvelles versions, sources...) sur le site de CdCat : &lt;br&gt;&lt;tt&gt;http://cdcat.sourceforge.net&lt;/tt&gt;&lt;/blockquote&gt;</translation>
    </message>
    <message>
        <location filename="../help.ui" line="22"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:40px; margin-right:40px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>importGtktalogCsv</name>
    <message>
        <location filename="../import.cpp" line="1344"/>
        <source>Import was successful.</source>
        <translation>Importation réussie.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1347"/>
        <source>1 media</source>
        <translation>1 média</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1349"/>
        <source>media</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1352"/>
        <source>1 directory:,
</source>
        <translation>1 répertoire :,
</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1354"/>
        <source>directories</source>
        <translation>Répertoires</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1357"/>
        <source>1 File</source>
        <translation>1 Fichier</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1359"/>
        <source>files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1361"/>
        <source>are imported.</source>
        <translation>ont été importées.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1363"/>
        <source>Import successful</source>
        <translation>Importation réussie</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1367"/>
        <source>file read error</source>
        <translation>Erreur de lecture de fichier</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1367"/>
        <source>Could not read file</source>
        <translation>Impossible de lire le fichier</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1370"/>
        <source>Fatal error</source>
        <translation>Erreur fatale</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1370"/>
        <source>Fatal error occured.</source>
        <translation>Une erreur fatale s&apos;est produite.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1373"/>
        <source>file open error</source>
        <translation>Erreur d&apos;ouverture de fichier</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1373"/>
        <source>Could not open file</source>
        <translation>Impossible d&apos;ouvrir le fichier</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1411"/>
        <source>importuser</source>
        <translation>Propriétaire</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="187"/>
        <source>Importing CSV...</source>
        <translation>En cours d&apos;import CSV...</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="187"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1274"/>
        <source>tag: </source>
        <translation>balise : </translation>
    </message>
</context>
<context>
    <name>importGtktalogXml</name>
    <message>
        <location filename="../import.cpp" line="1814"/>
        <source>Importing XML...</source>
        <translation>&apos;Importation XML en cours...</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1851"/>
        <source>XML import was successful.</source>
        <translation>Importation XML réussie.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1854"/>
        <source>1 media</source>
        <translation>1 média</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1856"/>
        <source>media</source>
        <translation>média</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1859"/>
        <source>1 directory:,
</source>
        <translation>1 répertoire :,
</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1861"/>
        <source>directories</source>
        <translation>répertoires</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1864"/>
        <source>1 File</source>
        <translation>1 Fichier</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1866"/>
        <source>files</source>
        <translation>fichiers</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1868"/>
        <source>are imported.</source>
        <translation>ont été importées.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1870"/>
        <source>Import successful</source>
        <translation>Importation réussie</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1875"/>
        <source>parse error</source>
        <translation>Erreur d&apos;analyse</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1875"/>
        <source>error during parsing</source>
        <translation>Erreur pendant l&apos;analyse</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1989"/>
        <source>importuser</source>
        <translation>Propriétaire</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1814"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>importWhereIsItXml</name>
    <message>
        <location filename="../import.cpp" line="2658"/>
        <source>Importing XML...</source>
        <translation>En cours d&apos;import XML...</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2694"/>
        <source>Importing XML from WhereIsIt was successful.</source>
        <translation>Importation XML de WhereIsIt réussie.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2697"/>
        <source>1 media</source>
        <translation>1 média</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2699"/>
        <source>media</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2702"/>
        <source>1 directory:,
</source>
        <translation>1 répertoire :,
</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2704"/>
        <source>directories</source>
        <translation>Répertoires</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2707"/>
        <source>1 File</source>
        <translation>1 Fichier</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2709"/>
        <source>files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2711"/>
        <source>are imported.</source>
        <translation>ont été importées.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2713"/>
        <source>Import successful</source>
        <translation>Importation réussie</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2718"/>
        <source>parse error</source>
        <translation>Erreur d&apos;analyse</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2718"/>
        <source>error during parsing</source>
        <translation>Erreur pendant l&apos;analyse</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2658"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2390"/>
        <source>tag:</source>
        <translation>balise</translation>
    </message>
</context>
<context>
    <name>newdbdialog</name>
    <message>
        <location filename="../newdbdialog.cpp" line="133"/>
        <source>DataBase&apos;s Properties</source>
        <translation>Propriétés de la base de données</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="134"/>
        <source>DataBase Name:</source>
        <translation>Nom de la base de données :</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="135"/>
        <source>DataBase Owner:</source>
        <translation>Propriétaire de la base de données :</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="136"/>
        <source>Comment:</source>
        <translation>Commentaire :</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="138"/>
        <source>Catalog</source>
        <translation>Catalogue</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="139"/>
        <source>hcat-user</source>
        <translation>Utilisateur hcat</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="140"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="141"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="142"/>
        <source> </source>
        <translation>.</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="137"/>
        <source>Category:</source>
        <translation>Catégorie :</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="144"/>
        <source>encrypt catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="145"/>
        <source>password for catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="159"/>
        <source>Password length is too short, must be minimum 4 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="159"/>
        <source>Password too short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="163"/>
        <source>Password length is too big, must be maximal %1 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="163"/>
        <source>Password too big</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>renamedialog</name>
    <message>
        <location filename="../newdbdialog.cpp" line="227"/>
        <source>Rename node...</source>
        <translation>Renommer le noeud...</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="228"/>
        <source>Give the new name:</source>
        <translation>Nouveau nom :</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="229"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="230"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="254"/>
        <source>Error:</source>
        <translation>Erreur :</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="249"/>
        <source>The new (media) name must be unique!</source>
        <translation>Le nouveau nom du média doit être unique !</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="254"/>
        <source>The new media name can&apos;t starts with &quot;@&quot; !</source>
        <translation>Le nouveau nom du média ne peut pas commencer avec le caractère &quot;@&quot; !</translation>
    </message>
</context>
<context>
    <name>renumberdialog</name>
    <message>
        <location filename="../newdbdialog.cpp" line="372"/>
        <source>Error:</source>
        <translation>Erreur :</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="301"/>
        <source>The element is NOT a Media!</source>
        <translation>L&apos;élément n&apos;est pas un média !</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="338"/>
        <source>Re-Number media...</source>
        <translation>Re-numéroter le média...</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="339"/>
        <source>Give the new serial number:</source>
        <translation>Nouveau numéro de série du média :</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="340"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="341"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="372"/>
        <source>The new media-number must be unique!</source>
        <translation>Le nouveau nom du média doit être unique !</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="362"/>
        <source>Please enter number value!</source>
        <translation>renseigner une valeur numérique !</translation>
    </message>
</context>
<context>
    <name>seekEngine</name>
    <message>
        <location filename="../find.cpp" line="1077"/>
        <source>Error in the pattern:</source>
        <translation>Erreur dans le motif :</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1163"/>
        <source>There is no matching.</source>
        <translation>Pas de correspondance.</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1652"/>
        <source>dir</source>
        <translation>Répertoire</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1658"/>
        <source>file</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1669"/>
        <source>error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1646"/>
        <source>media</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1041"/>
        <source>Searching, please wait...</source>
        <translation>Recherche en cours, merci de patienter...</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1158"/>
        <source>Search cancelled</source>
        <translation>Recherche annulée</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1158"/>
        <source>You have cancelled searching.</source>
        <translation>Vous avez annulé la recherche.</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1505"/>
        <source>File in archive: </source>
        <translation>Fichier dans l&apos;archive :</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1154"/>
        <source>Results:</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1154"/>
        <source>searching is in progress</source>
        <translation>Recherche en cours</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1160"/>
        <source>Last search results:</source>
        <translation>Derniers résultats de recherches</translation>
    </message>
</context>
</TS>
