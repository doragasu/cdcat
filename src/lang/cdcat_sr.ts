<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name></name>
    <message>
        <location filename="../dbase.cpp" line="185"/>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddLnk</name>
    <message>
        <location filename="../adddialog.cpp" line="763"/>
        <source>Add a &quot;Catalog Link&quot; to the database</source>
        <translation type="unfinished">Dodaj &quot;Catalog Link&quot; u bazu</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="764"/>
        <source>Location of the .hcf file:</source>
        <translation type="unfinished">Lokacija .hcf fajla:</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="766"/>
        <source>Cancel</source>
        <translation type="unfinished">Poništi</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="767"/>
        <source>Ok</source>
        <translation type="unfinished">U redu</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="784"/>
        <source>CdCat databases (*.hcf )</source>
        <translation type="unfinished">CdCat baze (*.hcf)</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="784"/>
        <source>Open a file...</source>
        <translation type="unfinished">otvori fajl...</translation>
    </message>
</context>
<context>
    <name>ArchiveFile</name>
    <message>
        <location filename="../dbase.h" line="283"/>
        <source>unknown</source>
        <translation type="unfinished">Nepoznato</translation>
    </message>
</context>
<context>
    <name>CatalogTypeEditDialog</name>
    <message>
        <location filename="../guibase.cpp" line="3138"/>
        <source>Change media type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3139"/>
        <source>Change type of media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3141"/>
        <source>CD</source>
        <translation type="unfinished">CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3142"/>
        <source>DVD</source>
        <translation type="unfinished">DVD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3143"/>
        <source>HardDisc</source>
        <translation type="unfinished">HardDisk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3144"/>
        <source>Floppy</source>
        <translation type="unfinished">Flopi</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3145"/>
        <source>NetworkPlace</source>
        <translation type="unfinished">Mrežni disk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3146"/>
        <source>FlashDrive</source>
        <translation type="unfinished">Fleš disk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3147"/>
        <source>OtherDevice</source>
        <translation type="unfinished">Ostali uređaji</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3150"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3151"/>
        <source>OK</source>
        <translation type="unfinished">U redu</translation>
    </message>
</context>
<context>
    <name>CdCatConfig</name>
    <message>
        <location filename="../config.cpp" line="213"/>
        <source>Error during autoload...</source>
        <translation>Greška prilikom autoučitavanja...</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="215"/>
        <source>I can&apos;t open the autoload catalog.
Check the file, or change the autoload option in the config dialog!
</source>
        <translation>Ne mogu da otvorim katalog za autoučitavanje.
Proveri fajl, ili promeni opciju za autoučitavanje u konfiguracionom dijalogu!
</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">U redu</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="215"/>
        <source>I can&apos;t open the autoload catalog according the first command line parameter.
Check the file!
</source>
        <translation>Ne mogu da otvorim katalog za autoučitavanje sudeći po prvom parametru komandne linije.
Proveri fajl!
</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>I can&apos;t get the $HOME environment variable.
It is necessary couse I&apos;d like to save the config file into your home directory.</source>
        <translation type="obsolete">Ne mogu da nadjem promenljivu $HOME.
To je neophodno jer konfiguracioni fajl hoću da sačuvam u tvom HOME direkrorijumu .</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1347"/>
        <source>I can&apos;t create or rewrite the ./cdcat.cfg file </source>
        <translation>Ne mogu da napravim ili ponovo upišem ./cdcat.cfg fajl </translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1349"/>
        <source>I can&apos;t create or rewrite the $(HOME)/.cdcatconfig file </source>
        <translation>Ne mogu da napravim ili da ponovo upišem  $(HOME)/.cdcatconfig fajl </translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1349"/>
        <source>Error while saving config file...</source>
        <translation>Greška prilikom snimanja konfiguracionog fajla... </translation>
    </message>
    <message>
        <location filename="../config.cpp" line="972"/>
        <source>I can&apos;t get the $HOME environment variable.
It is necessary because I&apos;d like to save the config file into your home directory.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CdCatMainWidget</name>
    <message>
        <location filename="../mainwidget.cpp" line="646"/>
        <source>Directory Tree</source>
        <translation>Grananje direktorijuma</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="649"/>
        <source>Name</source>
        <translation>Ime</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="650"/>
        <source>Size</source>
        <translation>Veličina</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="171"/>
        <source>New...</source>
        <translation type="obsolete">Novo...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="172"/>
        <source>Open...</source>
        <translation type="obsolete">Otvori...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="173"/>
        <source>Save</source>
        <translation type="obsolete">Sačuvaj</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="174"/>
        <source>Save As...</source>
        <translation type="obsolete">Sačuvaj kao...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="177"/>
        <source>Close</source>
        <translation type="obsolete">Zatvori</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="187"/>
        <source>Quit</source>
        <translation type="obsolete">Prekini</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="677"/>
        <source>Add media...</source>
        <translation>Dodaj medij...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="681"/>
        <source>Rescan media...</source>
        <translation>Ponovo skeniraj  medij...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="683"/>
        <source>Insert Catalog...</source>
        <translation>Ubaci katalog...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="687"/>
        <source>Rename node...</source>
        <translation>Promeni naziv čvora...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="693"/>
        <source>Delete node</source>
        <translation>Obriši čvor</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="578"/>
        <source>Sort media by number</source>
        <translation type="obsolete">Sortiraj midije po broju</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="307"/>
        <source>Sort media by name</source>
        <translation>Sortiraj medije po nazivu</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="202"/>
        <source>Sort media by type</source>
        <translation type="obsolete">Sortiraj medije po tipu</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="582"/>
        <source>Sort media by time</source>
        <translation type="obsolete">Sortiraj medije po vremenu</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="718"/>
        <source>Seek in database...</source>
        <translation>Traži u bazi...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="724"/>
        <source>Node size</source>
        <translation>Veličina čvora</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="726"/>
        <source>Configuration...</source>
        <translation>Podešavanja...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="735"/>
        <source>Help</source>
        <translation>Pomoć</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="514"/>
        <source>About</source>
        <translation type="obsolete">O programu</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="350"/>
        <source>Catalog</source>
        <translation type="obsolete">Katalog</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="638"/>
        <source>Edit</source>
        <translation>Uredjivanje</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="642"/>
        <source>Others</source>
        <translation>Ostalo</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="747"/>
        <source>Hyper&apos;s CD Catalogizer</source>
        <translation>Hyper&apos;s CD Catalogizer</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="493"/>
        <source>Open a catalog from a file.</source>
        <translation type="obsolete">Otvori katalog iz fajla.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="499"/>
        <source>Close the actual catalog.</source>
        <translation type="obsolete">Zatvori trenutni katalog.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="501"/>
        <source>Add a new media to the catalog.</source>
        <translation type="obsolete">Dodaj novi medij u katalog.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete the selected media from the catalog.</source>
        <translation type="obsolete">Obriši izabrani medij iz kataloga.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="507"/>
        <source>Configuration of the program.</source>
        <translation type="obsolete">Podešavanja programa.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="511"/>
        <source>Create a new, empty catalog and close the previous.</source>
        <translation type="obsolete">Napravi novi, prazni katalog i zatvori predhodni.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="513"/>
        <source>Search an element in the database(catalog).You can search filenames, directory names, name parts or mp3 tags etc...</source>
        <translation type="obsolete">Traži element u bazi(katalogu). Možeš tražiti po imenu fajla, imenu direktorijuma, imenu delova ili mp3 tagova itd...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="369"/>
        <source>Search an element.</source>
        <translation type="obsolete">Traži element.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="672"/>
        <source>Recent files...</source>
        <translation>Skorašnji fajlovi...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Export database</source>
        <translation type="obsolete">Izvezi bazu</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="641"/>
        <source>Import/Export</source>
        <translation>Uvoz/Izvoz</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="689"/>
        <source>Re-Number media node...</source>
        <translation>Promeni broj čvora...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Import database (csv/gtktalog)</source>
        <translation type="obsolete">Uvezi bazu (csv/gtktalog)</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="723"/>
        <source>Borrowing info...</source>
        <translation>Informacije o pozajmljivanju...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="495"/>
        <source>Save all modifications to the disc.</source>
        <translation type="obsolete">Sačuvaj sve promene na disk.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="497"/>
        <source>Save the catalog to a new file.</source>
        <translation type="obsolete">Sačuvaj katalog u novom fajlu.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add a link to a CdCAt Catalog...</source>
        <translation type="obsolete">Dodaj link CdCat Katalogu...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="728"/>
        <source>Set Colors...</source>
        <translation>Podesi boje...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="85"/>
        <source>No item selected</source>
        <translation type="obsolete">Nijedna stavka nije izabrana</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="503"/>
        <source>Rescan the selected media.</source>
        <translation type="obsolete">Skeniraj ponovo izabrani medij.</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="721"/>
        <source>Seek in the panel</source>
        <translation>Traži u panelu</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="679"/>
        <source>Add a link to a CdCat Catalog...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="738"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="730"/>
        <source>Import database (CSV/XML)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="733"/>
        <source>Export database (CSV/HTML/XML)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="799"/>
        <source>Scanning:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="640"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="655"/>
        <source>&amp;New...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="656"/>
        <source>Create a new catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="657"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="658"/>
        <source>Open a existing catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="659"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="660"/>
        <source>Save catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="661"/>
        <source>&amp;Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="662"/>
        <source>save catalog with new name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="674"/>
        <source>Close catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="821"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="676"/>
        <source>Close program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="678"/>
        <source>Add new media to catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="680"/>
        <source>Add a link to a existing cdcat catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="682"/>
        <source>Rescan existing media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="684"/>
        <source>Insert catalog into database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="688"/>
        <source>Rename node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="690"/>
        <source>Renumber node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="719"/>
        <source>Seek in database for files and folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="725"/>
        <source>Calculate node size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="727"/>
        <source>Edit cdcat configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="729"/>
        <source>Set the colors for display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="731"/>
        <source>Import database (CSV/XML) from various catalog programs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="737"/>
        <source>About Cdcat</source>
        <translation type="unfinished">O programu Cdcat</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="739"/>
        <source>About the Qt toolkit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="812"/>
        <source>Mi&amp;nimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="815"/>
        <source>Ma&amp;ximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="818"/>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="920"/>
        <source>Cdcat - idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="81"/>
        <source>Main toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="109"/>
        <source>Comment dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="710"/>
        <source>view tool bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="711"/>
        <source>View tool bar in main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="712"/>
        <source>view status bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="713"/>
        <source>View status bar in main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="714"/>
        <source>view comment dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="715"/>
        <source>show comment dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="639"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="717"/>
        <source>show systray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="637"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="651"/>
        <source>Type</source>
        <translation type="unfinished">Tip</translation>
    </message>
    <message>
        <location filename="../mainwidget.h" line="94"/>
        <source>processing file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="695"/>
        <source>Sort media by number (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="697"/>
        <source>Sort media by number (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="703"/>
        <source>Sort media by time (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="705"/>
        <source>Sort media by time (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="699"/>
        <source>Sort media by name (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="701"/>
        <source>Sort media by name (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="707"/>
        <source>Sort media by type (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="709"/>
        <source>Sort media by type (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="685"/>
        <source>Insert Cdcat XML export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="686"/>
        <source>Insert Cdcat XML export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="251"/>
        <source>Insert Cdcat export XML...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="254"/>
        <source>Insert Cdcat exported XML into database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="664"/>
        <source>Change password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="665"/>
        <source>Changes password for catalog encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="666"/>
        <source>Disable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="667"/>
        <source>Disables catalog encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="668"/>
        <source>Enable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="669"/>
        <source>Enables catalog encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="395"/>
        <source>Borrowing info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="444"/>
        <source>Key bindings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSchemePreview</name>
    <message>
        <location filename="../colorsettings.cpp" line="71"/>
        <source>Title:</source>
        <translation>Naziv:</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="73"/>
        <source>Road To Hell</source>
        <translation>Pakleni put</translation>
    </message>
</context>
<context>
    <name>ColorSettings</name>
    <message>
        <location filename="../colorsettings.cpp" line="221"/>
        <source>ColorSettings</source>
        <translation>Podešavanja boja</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="223"/>
        <source>Comment area background and the file higlighting line</source>
        <translation>Pozadina oblasti za komentar i linije za označavanje fajla</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="224"/>
        <source>Frame of comment area</source>
        <translation>Okvir oblasti za komentar</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="225"/>
        <source>Comment window static text (Program text)</source>
        <translation>Nepromenjlivi tekst u prozoru za komentar (tekst programa)</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="226"/>
        <source>Comment window variable text (Data)</source>
        <translation>Promenljivi tekst iprozoru za komentar (podaci)</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="227"/>
        <source>color</source>
        <translation>boja</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="228"/>
        <source>Red:</source>
        <translation>Crvena:</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="229"/>
        <source>Green:</source>
        <translation>Zelena:</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="230"/>
        <source>Blue:</source>
        <translation>Plava:</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="231"/>
        <source>Ok</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../colorsettings.cpp" line="232"/>
        <source>Cancel</source>
        <translation>Poništi</translation>
    </message>
</context>
<context>
    <name>CommentWidget</name>
    <message>
        <location filename="../commwidget.cpp" line="88"/>
        <source>Edit and refresh the actual comment page.</source>
        <translation>Izmeni i osveži trenutni komentar.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="182"/>
        <source>Upper container! (..)</source>
        <translation>Gornji kontejner! (..)</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="239"/>
        <source> CdCat Catalog root</source>
        <translation> Koren CdCat kataloga</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="245"/>
        <source> Directory</source>
        <translation> Direktorijum</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="248"/>
        <source> File</source>
        <translation> Fajl</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="251"/>
        <source>Media</source>
        <translation>Medij</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="265"/>
        <source>Size:</source>
        <translation>Veličina:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="328"/>
        <source>Owner:</source>
        <translation>Vlasnik:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="336"/>
        <source>Unknown</source>
        <translation>Nepoznato</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Media (number/name):</source>
        <translation type="obsolete">Medija (broj/ime):</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="386"/>
        <source>Mp3-Tag:</source>
        <translation>mp3 tag:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="313"/>
        <source>(Art/Tit/Alb/Year/Comm)</source>
        <translation type="obsolete">(Art/Tit/Alb/Year/Comm)</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="495"/>
        <source>Comment:</source>
        <translation>Komentar:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="83"/>
        <source>Shows the content of the file.</source>
        <translation>Pokazuje sadržaj fajla.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="191"/>
        <source>Name:</source>
        <translation>Ime:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="234"/>
        <source>Type:</source>
        <translation>Tip:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="844"/>
        <source>There is no selected element.</source>
        <translation>Ne postoji izabrani element.</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="242"/>
        <source> Link to a CdCat catalog</source>
        <translation> Link ka CdCat kataloga</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="283"/>
        <source>Location:</source>
        <translation>Lokacija:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="297"/>
        <source>Last modification:</source>
        <translation>Poslednja promena:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="98"/>
        <source>Edit and refresh the actual category page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="204"/>
        <source>At media (number/name):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="560"/>
        <source>Category:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="615"/>
        <source>Archive contents:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="393"/>
        <source>Artist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="399"/>
        <source>Title:</source>
        <translation type="unfinished">Naziv:</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="399"/>
        <source>track:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="405"/>
        <source>Album:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="411"/>
        <source>Year:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="730"/>
        <source>File info:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="636"/>
        <source>Rights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="641"/>
        <source>Owner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="646"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="651"/>
        <source>Size</source>
        <translation type="unfinished">Veličina</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="656"/>
        <source>Changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="666"/>
        <source>Comment</source>
        <translation type="unfinished">Komentar</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="661"/>
        <source>Path</source>
        <translation type="unfinished">Putanja</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="431"/>
        <source>Exif data:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="457"/>
        <source>Thumbnail:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="825"/>
        <source>Stored size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="845"/>
        <source>There is no selected element:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="353"/>
        <source>Number</source>
        <translation type="unfinished">Broj</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="356"/>
        <source>Name</source>
        <translation type="unfinished">Ime</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="359"/>
        <source>Type</source>
        <translation type="unfinished">Tip</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="362"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="367"/>
        <source>sorted by:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../config.cpp" line="1609"/>
        <source>Configure  CdCat...</source>
        <translation>Podesi CdCat...</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1613"/>
        <source>Autoload DataBase on startup</source>
        <translation>Autoučitaj Bazu pri startovanju</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1615"/>
        <source>Save the XML-db.file nicer format(needs more space)</source>
        <translation>Sačuvaj XML-db. fajl u lepšem formatu (zauzima više mesta)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1616"/>
        <source>Cancel</source>
        <translation>Poništi</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1617"/>
        <source>Ok</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1621"/>
        <source>Application font size.(must restart cdcat!) </source>
        <translation>Veličina fonta u programu. (traži restartovanje CdCat-a!) </translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>CdCat Databases (*.hcf)</source>
        <translation type="obsolete">CdCat baze (*.hcf)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1612"/>
        <source>Path to cdrom device</source>
        <translation type="obsolete">Putanja ka cdrom uređaju</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1627"/>
        <source>Scanning: mount cdrom at start / eject when finish</source>
        <translation>Skeniranje: mount cdroma kada počne / eject kada završi</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1619"/>
        <source>Use own font size</source>
        <translation>Koristi svoju veličinu fonta</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1622"/>
        <source>Number of history entries</source>
        <translation>Broj istorijskih unosa</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1646"/>
        <source>Choose path to cdrom!</source>
        <translation>Izaberi putanju ka cdromu!</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1631"/>
        <source>The language of CdCat interface</source>
        <translation>Jezik interfejsa u CdCat-u</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1640"/>
        <source>Choose a file to load automatically!</source>
        <translation>Izaberi fajl koji trebam automatski učitati!</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Automatically save the database after every scan (for dafety sake)</source>
        <translation type="obsolete">Automatski sačuvaj bazu nakon svakog skeniranja (radi sigurnosti)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1618"/>
        <source>Select additional items to read</source>
        <translation>Izaberi dodatne stavke za čitanje</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1614"/>
        <source>Automatically save the database after every scan (for safety sake)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1634"/>
        <source>Display debug info on console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1635"/>
        <source>Save catalogs always as UTF8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1636"/>
        <source>Show progressed file at scanning in status label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1640"/>
        <source>CdCat databases (*.hcf )</source>
        <translation type="unfinished">CdCat baze (*.hcf)</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1620"/>
        <source>font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1494"/>
        <source>show systray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1610"/>
        <source>Show systray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1611"/>
        <source>display current scanned file in tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1612"/>
        <source>display current scanned file in tray (mediainfo / archive scan)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1623"/>
        <source>Path to cdrom device / mount dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../config.cpp" line="1624"/>
        <source>This is the path to the cdrom/dvd device or the path where its mounted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataBase</name>
    <message>
        <location filename="../dbase.cpp" line="719"/>
        <source>I can&apos;t rewrite the file: %1</source>
        <translation>Ne mogu da upišem u fajl: %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="744"/>
        <source>I can&apos;t create the file: %1</source>
        <translation>Ne mogu da kreiram fajl: %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2091"/>
        <source>I can&apos;t open the file: %1</source>
        <translation>Ne mogu da otvorim fajl: %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="990"/>
        <source>Cannot read directory: %1</source>
        <translation>Ne mogu da učitam direktorijum: %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2127"/>
        <source>Error</source>
        <translation>Greška</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ignore directory</source>
        <translation type="obsolete">Ignoriši direktorijum</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="994"/>
        <source>Cancel scanning</source>
        <translation>Prekini skeniranje</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Symbolic link#Points to:</source>
        <translation type="obsolete">Simbolički link#Ukazuje na:</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2127"/>
        <source>Error while parsing file: %1</source>
        <translation>Greška prilikom obrade fajla: %1</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="769"/>
        <source>No database opened!</source>
        <translation>Nijedna baza nije otvorena!</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="422"/>
        <source>Please Wait...</source>
        <translation type="obsolete">Sačekajte...</translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2115"/>
        <source>Not enough memory to open the file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="992"/>
        <source>Cannot read file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="994"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1091"/>
        <source>Symbolic link to file:#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1143"/>
        <source>Symbolic link to directory:#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1172"/>
        <source>DEAD Symbolic link to:#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1182"/>
        <source>System file (e.g. FIFO, socket or device file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1876"/>
        <source>device </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1892"/>
        <source> link to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="959"/>
        <source>Reading file, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1080"/>
        <source>Converting to unicode, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1087"/>
        <source>Parsing file, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1094"/>
        <source>You have cancelled catalog reading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1208"/>
        <source>reading mp3 info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1262"/>
        <source>reading media info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1279"/>
        <source>reading avi info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1353"/>
        <source>reading file content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1407"/>
        <source>reading exif data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1436"/>
        <source>reading thumbnail data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="1948"/>
        <source>scanning archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="2044"/>
        <source>scanning archive, file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dbase.cpp" line="571"/>
        <source>unknown</source>
        <translation type="unfinished">Nepoznato</translation>
    </message>
</context>
<context>
    <name>DirectoryView</name>
    <message>
        <location filename="../dirview.cpp" line="185"/>
        <source>Name</source>
        <translation>Ime</translation>
    </message>
</context>
<context>
    <name>GuiSlave</name>
    <message>
        <location filename="../guibase.cpp" line="622"/>
        <source>Directory</source>
        <translation> Direktorijum</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="679"/>
        <source>Catalog</source>
        <translation>Katalog</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2274"/>
        <source>Unknown(DB)</source>
        <translation>Nepoznato(DB)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2278"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2282"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2286"/>
        <source>HardDisc</source>
        <translation>HardDisk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2290"/>
        <source>Floppy</source>
        <translation>Flopi</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2294"/>
        <source>NetworkDrv</source>
        <translation>Mrežni disk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2298"/>
        <source>FlashDrv</source>
        <translation>Fleš disk</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2302"/>
        <source>OtherDevice</source>
        <translation>Ostali uređaji</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="710"/>
        <source>File</source>
        <translation> Fajl</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="926"/>
        <source>Hyper&apos;s CD Catalogizer (modified)</source>
        <translation>Hyper&apos;s CD Catalogizer (promenjen)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="932"/>
        <source>Hyper&apos;s CD Catalogizer</source>
        <translation>Hyper&apos;s CD Catalogizer</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2198"/>
        <source>CdCat databases (*.hcf )</source>
        <translation>CdCat baze (*.hcf)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1176"/>
        <source>Open a file...</source>
        <translation>Otvori fajl...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2847"/>
        <source>Error while opening...</source>
        <translation>Greška prilikom otvaranja...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">U redu</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1345"/>
        <source>Save to file...</source>
        <translation>Sačuvaj u fajl...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1370"/>
        <source>Error while saving...</source>
        <translation>Greška prilikom sačuvavanja...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1418"/>
        <source>Closing previous database...</source>
        <translation>Zatvaram predhodnu bazu...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1418"/>
        <source>Do you want to save the changes?</source>
        <translation>Da li želiš da sačuvaš promene?</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2914"/>
        <source>Yes</source>
        <translation>Da</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2914"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2930"/>
        <source>Error:</source>
        <translation>Greška:</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1434"/>
        <source>Do you really want to delete this?: 
%1</source>
        <translation type="obsolete">Da li zaista želiš da obrišep ovo?:
%1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2912"/>
        <source>Confirmation</source>
        <translation>Potvrda</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1811"/>
        <source>Warning...</source>
        <translation>Upozorenje...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1957"/>
        <source>You can refresh ONLY a MEDIA node!</source>
        <translation>Možeš da osvežiš SAMO MEDIA čvor!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2198"/>
        <source>Insert a database file...</source>
        <translation>Ubaci fajl baze...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2379"/>
        <source>The size of &quot;%1&quot; : 
 %2 
 %3 file /%4 directory</source>
        <translation>Veličina &quot;%1&quot; : 
 %2 
 %3 fajl /%4 direktorijum</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1541"/>
        <source>Cannot mount CD</source>
        <translation>Ne mogu da montiram CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1887"/>
        <source>Cannot eject CD!</source>
        <translation>Ne mogu da izbacim CD!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1541"/>
        <source>I can&apos;t find the &quot;mount&quot; program</source>
        <translation>Ne mogu da nađem program &quot;mount&quot;</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1905"/>
        <source>Cannot mount CD!</source>
        <translation>Ne mogu da montiram CD!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1853"/>
        <source>Cannot eject CD</source>
        <translation>Ne mogu da izbacim CD</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1853"/>
        <source>I can&apos;t find the &quot;eject&quot; program</source>
        <translation>Ne mogu da nađem program &quot;eject&quot;</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1042"/>
        <source>Rename node...</source>
        <translation>Promeni naziv čvora...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1043"/>
        <source>Delete node</source>
        <translation>Obriši čvor</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1052"/>
        <source>Rescan media...</source>
        <translation>Ponovo skeniraj  medij...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2542"/>
        <source>Add media...</source>
        <translation>Dodaj medij...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1077"/>
        <source>Insert Catalog...</source>
        <translation>Ubaci katalog...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1961"/>
        <source>Rescan %1</source>
        <translation>Ponovo skeniraj  %1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1053"/>
        <source>Re-Number media...</source>
        <translation>Promeni broj medija...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2322"/>
        <source>It is NOT a media node!</source>
        <translation>To nije media čvor!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1037"/>
        <source>Node size</source>
        <translation>Veličina čvora</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1034"/>
        <source>View/Edit Comment...</source>
        <translation>Pogledaj/Izmeni komentar...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1081"/>
        <source>Close all branch</source>
        <translation>Zatvori sve grane</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="605"/>
        <source>Show/Remove Content...</source>
        <translation type="obsolete">Prokaži/Izbaci sadržaj...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2930"/>
        <source>There is no selected item in the middle list box!</source>
        <translation>Ne postoji izabrana stavka u srednjem spisku!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1811"/>
        <source>An error occured while scanning,
the DataBase may be incomplete</source>
        <translation>Prilikom skeniranja je došlo do greške,
moguće da je baza nepotpuna</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1953"/>
        <source>There is no selected Media in the middle list box!</source>
        <translation>Ne postoji izabrani medij u srednjem spisku!</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2027"/>
        <source>An error occured while scanning, the rescan operation was cancelled: 
%1</source>
        <translation>Prilikom skeniranja je došlo do greške, opcija ponovnog skeniranje je poništena:
%1</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1048"/>
        <source>Borrow this media to...</source>
        <translation>Pozajmi medij...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2913"/>
        <source>Do you want to clear the borrowing mark from media &quot;%1&quot;?
(Say yes if you got it back.)</source>
        <translation>Da li želiš da poništiš oznaku pozajmljen za medij &quot;%1&quot;?
(Kaži Da ako ti je medij vraćen.)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1050"/>
        <source>I got it back! (clear borrowing mark)</source>
        <translation>Vraćen mi je! (oriši oznaku pozajmljen)</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="713"/>
        <source>Catalog Link</source>
        <translation>Link kataloga</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="991"/>
        <source>Follow the link (Open it) !</source>
        <translation>Prati link (Otvori ga) !</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="777"/>
        <source>Add a link to a CdCAt Catalog...</source>
        <translation type="obsolete">Dodaj link CdCat Katalogu...</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2807"/>
        <source>The link is empty.</source>
        <translation>Ovaj link je prazan.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="595"/>
        <source>Number</source>
        <translation>Broj</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="598"/>
        <source>Size</source>
        <translation>Veličina</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="343"/>
        <source>The database file has newer version than this version of cdcat can work with:
I understand maximum %1 datafile version but readed %2

Strongly recommended to upgrade your cdcat!!!
Homepage: %3</source>
        <translation>Fajl baze podake je napravljen novijom verzijom CdCat-a:
Ja razumem najviše  %1 verziju baze  ali je ovo %2

Preporučljivo je koristiti noviju verziju CdCat-a!!!
Homepage: %3</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="354"/>
        <source>No database opened.</source>
        <translation>Nijedna baza nije otvorena.</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="356"/>
        <source>No item selected.</source>
        <translation>Nijedna stavka nije izabrana.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The resoult:</source>
        <translation type="obsolete">Rezultat:</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1035"/>
        <source>View/Edit Category...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1057"/>
        <source>Change media type...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1985"/>
        <source>Scanning directory tree, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1962"/>
        <source>Select directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2384"/>
        <source>The result:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1718"/>
        <source>Enter media name...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1719"/>
        <source>Media name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1720"/>
        <source>Media</source>
        <translation type="unfinished">Medij</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1009"/>
        <source>search for duplicates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="983"/>
        <source>Show content...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="986"/>
        <source>Show/Remove content...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1014"/>
        <source>Add a link to a CdCat Catalog...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1076"/>
        <source>Add a link to a Cdcat catalog...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1770"/>
        <source>You have cancelled catalog scanning,
the DataBase may be incomplete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1988"/>
        <source>Scan started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1988"/>
        <source>Scanning %1 into %2 has been started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2003"/>
        <source>Scanning %1 into %2 has been finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2007"/>
        <source>Scan finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2007"/>
        <source>Scanning %1 into %2 has been finished (NOT complete)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2095"/>
        <source>Cdcat - idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2514"/>
        <source>Help</source>
        <translation type="unfinished">Pomoć</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2516"/>
        <source>Whats this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2519"/>
        <source>The cdcat is graphical (QT based) multiplatform (Linux/Windows) catalog program which scans the directories/drives you want and memorize the filesystem including the tags of mp3&apos;s and other data and store it in a small file. The database is stored in a gzipped XML format, so you can hack it, or use it if necessary :-).)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2521"/>
        <source>The program can store the content of some specified files up to a limit size if you want. (for example: *.nfo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2524"/>
        <source>Usage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2527"/>
        <source>Before the scanning select the necessary readable components in the config dialog, which can be mp3 tags content of some files or etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2531"/>
        <source>Create a new catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2534"/>
        <source>Run the %1 command in the catalog menu. You have to type the name of the new catalog. You can specify the default username of the media(which you scan later), and add a comment to the catalog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2534"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2538"/>
        <source>Add media</source>
        <translation type="unfinished">Dodaj medij</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2542"/>
        <source>Run the %1 command in the edit menu. In that dialog you have to specify the directory/or drive the media you want to add. It&apos;s recommended to specify the name and the number of the media which has to be unique. (The program always generate one identical name and number). You can label the media to a owner, if necessary.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2547"/>
        <source>save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2551"/>
        <source>Open an existing catalog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2553"/>
        <source>open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2557"/>
        <source>Author:</source>
        <translation type="unfinished">Autor:</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2560"/>
        <source>The program was written by Peter Deak (hungary)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2563"/>
        <source>The current maintainer is %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2567"/>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2571"/>
        <source>General Public License (GPL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2575"/>
        <source>Homepage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2579"/>
        <source>You can read about the program and get new versions, sources etc, in the hompage of cdcat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2553"/>
        <source>Run the %1 command in the file menu, and choice the file of the catalog. (*.hcf). After the opening you will be able browse the catalog or search in it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2547"/>
        <source>If you scanned your media, you will be able to browse in it with the browser window (like mc) , or search in it. You can save the catalog with %1 command in the file menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1352"/>
        <source>Saving catalog, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2164"/>
        <source>Insert a cdcat exported xml file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2164"/>
        <source>CdCat xml export (*.xml )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1065"/>
        <source>Change password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2955"/>
        <source>Password has been successfully changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3007"/>
        <source>Password changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2994"/>
        <source>Password length is too short, must be minimum 4 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2994"/>
        <source>Password too short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2981"/>
        <source>Password length is too big, must be maximal %1 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2981"/>
        <source>Password too big</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1066"/>
        <source>Disable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1070"/>
        <source>Enable encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2986"/>
        <source>Encryption has been successfully enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="3007"/>
        <source>Encryption has been successfully disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2998"/>
        <source>Passwords not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2998"/>
        <source>Passwords does not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="1453"/>
        <source>Do you really want to delete
&quot;%1&quot;?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HDirectoryView</name>
    <message>
        <location filename="../hdirview.cpp" line="322"/>
        <source>Directory Tree</source>
        <translation>Grananje direktorijuma</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="323"/>
        <source>Type</source>
        <translation>Tip</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="362"/>
        <source>Copy</source>
        <translation type="obsolete">Kopiraj</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="365"/>
        <source>Move</source>
        <translation type="obsolete">Pomeri</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="369"/>
        <source>Link</source>
        <translation type="obsolete">Link</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="373"/>
        <source>Unknown</source>
        <translation type="obsolete">Nepoznato</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="387"/>
        <source>Drop target</source>
        <translation type="obsolete">Ispusti cilj</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="431"/>
        <source>Not implemented</source>
        <translation type="obsolete">Nije implementirano</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="430"/>
        <source>Drag source</source>
        <translation type="obsolete">Povuci izvor</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="431"/>
        <source>Delete </source>
        <translation type="obsolete">Obriši </translation>
    </message>
</context>
<context>
    <name>HQListView</name>
    <message>
        <location filename="../guibase.cpp" line="198"/>
        <source>Name</source>
        <translation>Ime</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="199"/>
        <source>Size</source>
        <translation>Veličina</translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="200"/>
        <source>Type</source>
        <translation>Tip</translation>
    </message>
</context>
<context>
    <name>ImportDialog</name>
    <message>
        <location filename="../importdialog.cpp" line="80"/>
        <source>Create new Database</source>
        <translation>Napravi novu bazu podataka</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="186"/>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="187"/>
        <source>Import</source>
        <translation>Uvezi</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="230"/>
        <source>Open file dialog</source>
        <translation type="obsolete">Dijalog za Otvaranje fajla</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="234"/>
        <source>Corrects bad output style (gtktalog)</source>
        <translation type="obsolete">Ispravi loš izlazni fajl (gtkatalog)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="243"/>
        <source>Error:</source>
        <translation>Greška:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">U redu</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="304"/>
        <source>csv files(*.csv)</source>
        <translation>cvs fajlovi(*.cvs)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>open file dialog</source>
        <translation type="obsolete">Dijalog za Otvaranje fajla</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="327"/>
        <source>Choose a file for import</source>
        <translation>Izaberi fajl za uvoz</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="190"/>
        <source>Type</source>
        <translation>Tip</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="230"/>
        <source>&lt;strong&gt;Please read the README_IMPORT before you import!&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Pročitajte fajl README_IMPORT pre nego što uvezete fajl!&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="310"/>
        <source>xml files(*.xml)</source>
        <translation>xml fajlovi(*.xml)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="180"/>
        <source>Import CSV file</source>
        <translation>Uvezi CVS fajl</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="181"/>
        <source>File:</source>
        <translation>Fajl:</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="87"/>
        <source>Correct bad style from gtktalog export</source>
        <translation>Ispravi loš stil iz gtktalog izvoza</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="184"/>
        <source>Separator:</source>
        <translation>Separator:</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="185"/>
        <source>This is the separator in dataline
&lt;path&gt;SEPARATOR&lt;size&gt;SEPARATOR&lt;date&gt;&lt;space&gt;&lt;time&gt;</source>
        <translation>Ovo je linija separatora
&lt;putanja&gt;SEPARATOR&lt;veličina&gt;SEPARATOR&lt;datum&gt;&lt;prostor&gt;&lt;vreme&gt;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="173"/>
        <source>Separator in data line</source>
        <translation type="obsolete">Separator u liniji podataka</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="188"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Text (csv)</source>
        <translation type="obsolete">&amp;Tekst (csv)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="200"/>
        <source>Gtktalog &amp;XML</source>
        <translation>Gtktalog &amp;XML</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="201"/>
        <source>&amp;WhereIsIt XML (classic)</source>
        <translation>&amp;WhereIsIt XML (classic)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="203"/>
        <source>Select the type of import here</source>
        <translation>Ovde izaberi tip uvoza</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select this for importing a text import (csv)</source>
        <translation type="obsolete">Označi ovo za uvoz iz tekst fajla (csv)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select this for importing a xml report generated frm gtktalog</source>
        <translation type="obsolete">Označi ovo za uvoz iz xml report fajla napravljenom iz gtktaloga</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="227"/>
        <source>Open the file dialog for selecting file to import.</source>
        <translation>Dijalog za otvaranje fajla za izbor fajla za uvoz.</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="229"/>
        <source>Corrects bad output style from gtktalog.
&lt;media&gt;SEPARATOR/&lt;dir&gt;/SEPARATOR/&lt;dir&gt;
 will be to
&lt;media&gt;/&lt;dir&gt;/&lt;dir&gt;</source>
        <translation>Ispravi loš izlazni fajl iz gtkatalog.
&lt;media&gt;SEPARATOR/&lt;dir&gt;/SEPARATOR/&lt;dir&gt;
 will be to
&lt;media&gt;/&lt;dir&gt;/&lt;dir&gt;</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="238"/>
        <source>You must be fill the &quot;Filename&quot; field!</source>
        <translation>Morate popuniti polje &quot;Ime fajla&quot;!</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="243"/>
        <source>You must be fill the &quot;Separator&quot; field!</source>
        <translation>Morate popuniti polje &quot;Separator&quot;!</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="315"/>
        <source>all files(*.*)</source>
        <translation>svi fajlovi(*.*)</translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="191"/>
        <source>&amp;Gtktalog (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="192"/>
        <source>&amp;Kat-CeDe (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="193"/>
        <source>&amp;Disclib (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="194"/>
        <source>&amp;VisualCD (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="195"/>
        <source>&amp;VVV (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="196"/>
        <source>&amp;Advanced File Organizer (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="197"/>
        <source>&amp;File Archivist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="198"/>
        <source>&amp;Advanced Disk Catalog (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="199"/>
        <source>W&amp;hereIsIt (csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="205"/>
        <source>Select this for importing a text import (csv) generated from Gtktalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="207"/>
        <source>Select this for importing a text import (csv) generated from Kat-CeDe.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="209"/>
        <source>Select this for importing a text import (csv) generated from Disclib.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="211"/>
        <source>Select this for importing a text import (csv) generated from VisualCD.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="213"/>
        <source>Select this for importing a text import (csv) generated from VVV.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="215"/>
        <source>Select this for importing a text import (csv) generated from Advanced File Organizer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="217"/>
        <source>Select this for importing a File Archivist catalog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="219"/>
        <source>Select this for importing a text import (csv) generated from Advanced Disk Catalog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="221"/>
        <source>Select this for importing a text import (csv) generated from WhereIsIt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="223"/>
        <source>Select this for importing a xml report generated from gtktalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="225"/>
        <source>Select this for importing a xml report generated from WhereIsIt?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../importdialog.cpp" line="313"/>
        <source>File Archivist files(*.arch)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../info.cpp" line="143"/>
        <source>German translation &amp; additional programming</source>
        <translation>Nemački prevod &amp; dodatno programiranje</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Spanish translation</source>
        <translation>Španski prevod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="163"/>
        <source>Czech translation</source>
        <translation>Češki prevod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="456"/>
        <source>About cdcat</source>
        <translation>O programu cdcat</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="457"/>
        <source>close</source>
        <translation>zatvori</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="460"/>
        <source>About Cdcat</source>
        <translation>O programu Cdcat</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="463"/>
        <source>License</source>
        <translation>Licenca</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="465"/>
        <source>Thanks</source>
        <translation>Zahvalnost</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="83"/>
        <source>-= CdCatalog by Hyper =-</source>
        <translation>-= CdCatalog by Hyper =-</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="87"/>
        <source>Version:</source>
        <translation>Verzija:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="132"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="134"/>
        <source>Copyright (C) 2003 Peter Deak (GPL)</source>
        <translation>Copyright (C) 2003 Peter Deak (GPL)</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="151"/>
        <source>AVI reader plugin &amp; Polish translation</source>
        <translation>Plugin za čitanje AVI fajla &amp; Poljski prevod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="89"/>
        <source> (with debug)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <source>Development version build at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="136"/>
        <source>Copyright (C) 2010 Christoph Thielecke (GPL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="145"/>
        <source>Ported to Qt4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="170"/>
        <source>For more details about the GPL license and to read in other languages, visit %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="170"/>
        <source>GPL page on GNU website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="107"/>
        <source>Features:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="111"/>
        <source>archive read support using lib7zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="115"/>
        <source>mediainfo (compiled in)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="119"/>
        <source>mediainfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="123"/>
        <source>exif data read support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="127"/>
        <source>encrypted catalog support</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyBindingDialog</name>
    <message>
        <location filename="../info.cpp" line="479"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="481"/>
        <source>Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="535"/>
        <source>Key bindings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="536"/>
        <source>close</source>
        <translation type="unfinished">zatvori</translation>
    </message>
</context>
<context>
    <name>PWw</name>
    <message>
        <location filename="../adddialog.cpp" line="488"/>
        <source>Please Wait...</source>
        <translation type="unfinished">Sačekajte...</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="588"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Otkaži</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../hdirview.cpp" line="254"/>
        <source>Directory</source>
        <translation> Direktorijum</translation>
    </message>
    <message>
        <location filename="../dirview.cpp" line="148"/>
        <source>Unreadable Directory</source>
        <translation>Nečitljiv direktorijum</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="232"/>
        <source>Catalog</source>
        <translation>Katalog</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="236"/>
        <source>Unknown(DB)</source>
        <translation>Nepoznato(DB)</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="238"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="240"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="242"/>
        <source>HardDisc</source>
        <translation>HardDisk</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="244"/>
        <source>Floppy</source>
        <translation>Flopi</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="246"/>
        <source>NetworkDrv</source>
        <translation>Mrežni disk</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="248"/>
        <source>FlashDrv</source>
        <translation>Fleš disk</translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="250"/>
        <source>OtherDevice</source>
        <translation>Ostali uređaji</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="640"/>
        <source>Video:
</source>
        <translation>Video:
</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="642"/>
        <source>Total Time</source>
        <translation>Ukupno vreme</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="643"/>
        <source>Framerate</source>
        <translation>Framerate</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="644"/>
        <source>Resolution</source>
        <translation>Rezolucija</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Audio:</source>
        <translation type="obsolete">Audio:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>More Data:</source>
        <translation type="obsolete">Još podataka:</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="656"/>
        <source>Artist</source>
        <translation>Izvođač</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="658"/>
        <source>Comments</source>
        <translation>Komenteri</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="660"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="662"/>
        <source>Creation Date</source>
        <translation>Datum kreiranja</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="664"/>
        <source>Engineer</source>
        <translation>Inženjer</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="668"/>
        <source>Name</source>
        <translation>Ime</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="670"/>
        <source>Product</source>
        <translation>Proizvod</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="672"/>
        <source>Software</source>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="674"/>
        <source>Source</source>
        <translation>Izvorni kod</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="676"/>
        <source>Subject</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="678"/>
        <source>Technician</source>
        <translation>Tehničar</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="763"/>
        <source>Unknown/not implemented/broken header
</source>
        <translation>Nepoznat/ne implementirani/loš heder
</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="648"/>
        <source>Channels</source>
        <translation>Kanali</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="650"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="666"/>
        <source>Genre</source>
        <translation>Žanr</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="492"/>
        <source>Not a RIFF/AVI file OR header broken!</source>
        <translation>Fajl nije RIFF/AVI ili je loš heder!</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="649"/>
        <source>Sample/s</source>
        <translation>Sample/s</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2411"/>
        <source>importuser</source>
        <translation type="unfinished">uvezi korisnika</translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="646"/>
        <source>Audio:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tparser.cpp" line="653"/>
        <source>More Data:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1004"/>
        <source>Enter password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1004"/>
        <source>Enter password for catalog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hdirview.cpp" line="230"/>
        <source>Catalog (encrypted)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1008"/>
        <source>password empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1014"/>
        <source>cant set password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1034"/>
        <source>decrypt failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wdbfile.cpp" line="1047"/>
        <source>cant load catalog: encrypted catalog support not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guibase.cpp" line="2976"/>
        <source>Enter password for catalog (again):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelReadable</name>
    <message>
        <location filename="../selreadable.cpp" line="836"/>
        <source>Select readable items</source>
        <translation>Izaberite stavke za čitanje</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="848"/>
        <source>Read mp3 tags</source>
        <translation>Citaj mp3 tagove</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="864"/>
        <source>Store content of some files</source>
        <translation>Sačuvaj sadržaj nekih fajlova</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="885"/>
        <source>Ok</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="886"/>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="875"/>
        <source>content size limit in kByte</source>
        <translation>maksimalna veličina fajla u kB</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="870"/>
        <source>; separated list of readable file patterns</source>
        <translation>; razdvojena lista</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="859"/>
        <source>Default tag</source>
        <translation>Podrazumevani tag</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="120"/>
        <source>version</source>
        <translation>verzija</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="861"/>
        <source>Read mp3 technical info as comment (bitrate,freq,length...)</source>
        <translation>Čitaj tehničke informacije iz mp3 fajla kao komentar (birate, freq, dužina...)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Read avi technical info as comment (codecs,lenhth,...)</source>
        <translation type="obsolete">Čitaj tehničke informacije iz avi fajla kao komentar (kodek, birate, dužina...)</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="863"/>
        <source>Read avi technical info as comment (codecs,length,...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="841"/>
        <source>Supported extensions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="837"/>
        <source>Archive file display options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="838"/>
        <source>Scan for archive file list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="868"/>
        <source>Read some technical info using mediainfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="850"/>
        <source>Read thumbnails from pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="856"/>
        <source>Read EXIF data from pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="854"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="872"/>
        <source>; separated list of image file extensions, e.g. png;jpg;gif</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="622"/>
        <source>Path to external content viewer (found)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="629"/>
        <source>Path to external content viewer (not found)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="606"/>
        <source>Choose path to external context viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="866"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="867"/>
        <source>Select external viewer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="175"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="413"/>
        <source>lib7zip found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="417"/>
        <source>lib7zip not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="420"/>
        <source>Archive support:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="449"/>
        <source>mediainfo not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="451"/>
        <source>mediainfo found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="456"/>
        <source>mediainfo status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="842"/>
        <source>Permission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="843"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="844"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="845"/>
        <source>Size</source>
        <translation type="unfinished">Veličina</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="846"/>
        <source>Date</source>
        <translation type="unfinished">Datum</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="847"/>
        <source>Comment</source>
        <translation type="unfinished">Komentar</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="849"/>
        <source>Read thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="851"/>
        <source>Size:</source>
        <translation type="unfinished">Veličina:</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="852"/>
        <source>Thumbnail size (width) in pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="853"/>
        <source>Thumbnail size (height) in pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="865"/>
        <source>Use external file content viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="869"/>
        <source>file patterns:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="871"/>
        <source>File extensions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="874"/>
        <source>max size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="860"/>
        <source>Read mp3 info as comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="862"/>
        <source>Read avi info as comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="667"/>
        <source>Add exclude rule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="667"/>
        <source>Enter regular expression for exclude:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="651"/>
        <source>edit rule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="655"/>
        <source>delete rule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="876"/>
        <source>exclude files/directories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="883"/>
        <source>add exclude rule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="884"/>
        <source>list of patterns (regular expression) for files/directories to skip on reading from filesystem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="695"/>
        <source>regular expression is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="699"/>
        <source>regular expression is valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="840"/>
        <source>show archive file at scanning in status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="839"/>
        <source>show archive file in status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="804"/>
        <source>About regular expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="806"/>
        <source>close</source>
        <translation type="unfinished">zatvori</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="879"/>
        <source>About regex:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="880"/>
        <source>Information about regular expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="881"/>
        <source>About regular expressions....</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="882"/>
        <source>Show introduction into regular expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="744"/>
        <source>Metacharacter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="745"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="753"/>
        <source>A bracket expression. Matches a single character that is contained within the brackets. For example, &lt;code&gt;[abc]&lt;/code&gt; matches &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;. &lt;code&gt;[a-z]&lt;/code&gt; specifies a range which matches any lowercase letter from &quot;&lt;i&gt;a&lt;/i&gt;&quot; to &quot;&lt;i&gt;z&lt;/i&gt;&quot;. These forms can be mixed: &lt;code&gt;[abcx-z]&lt;/code&gt; matches &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, &quot;&lt;i&gt;c&lt;/i&gt;&quot;, &quot;&lt;i&gt;x&lt;/i&gt;&quot;, &quot;&lt;i&gt;y&lt;/i&gt;&quot;, or &quot;&lt;i&gt;z&lt;/i&gt;&quot;, as does &lt;code&gt;[a-cx-z]&lt;/code&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="754"/>
        <source>The &lt;code&gt;-&lt;/code&gt; character is treated as a literal character if it is the last or the first (after the &lt;code&gt;^&lt;/code&gt;) character within the brackets: &lt;code&gt;[abc-]&lt;/code&gt;, &lt;code&gt;[-abc]&lt;/code&gt;. Note that backslash escapes are not allowed. The &lt;code&gt;]&lt;/code&gt; character can be included in a bracket expression if it is the first (after the &lt;code&gt;^&lt;/code&gt;) character: &lt;code&gt;[]abc]&lt;/code&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="759"/>
        <source>Matches a single character that is not contained within the brackets. For example, &lt;code&gt;[^abc]&lt;/code&gt; matches any character other than &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;b&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;. &lt;code&gt;[^a-z]&lt;/code&gt; matches any single character that is not a lowercase letter from &quot;&lt;i&gt;a&lt;/i&gt;&quot; to &quot;&lt;i&gt;z&lt;/i&gt;&quot;. Likewise, literal characters and ranges can be mixed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="763"/>
        <source>Matches the starting position within the string. In line-based tools, it matches the starting position of any line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="776"/>
        <source>Matches what the &lt;i&gt;n&lt;/i&gt;th marked subexpression matched, where &lt;i&gt;n&lt;/i&gt; is a digit from 1 to 9. This construct is theoretically &lt;b&gt;irregular&lt;/b&gt; and was not adopted in the POSIX ERE syntax. Some tools allow referencing more than nine capturing groups.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="780"/>
        <source>Matches the preceding element zero or more times. For example, &lt;code&gt;ab*c&lt;/code&gt; matches &quot;&lt;i&gt;ac&lt;/i&gt;&quot;, &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, &quot;&lt;i&gt;abbbc&lt;/i&gt;&quot;, etc. &lt;code&gt;[xyz]*&lt;/code&gt; matches &quot;&quot;, &quot;&lt;i&gt;x&lt;/i&gt;&quot;, &quot;&lt;i&gt;y&lt;/i&gt;&quot;, &quot;&lt;i&gt;z&lt;/i&gt;&quot;, &quot;&lt;i&gt;zx&lt;/i&gt;&quot;, &quot;&lt;i&gt;zyx&lt;/i&gt;&quot;, &quot;&lt;i&gt;xyzzy&lt;/i&gt;&quot;, and so on. &lt;code&gt;\(ab\)*&lt;/code&gt; matches &quot;&quot;, &quot;&lt;i&gt;ab&lt;/i&gt;&quot;, &quot;&lt;i&gt;abab&lt;/i&gt;&quot;, &quot;&lt;i&gt;ababab&lt;/i&gt;&quot;, and so on.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="788"/>
        <source>Examples:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="790"/>
        <source>matches any three-character string ending with &quot;at&quot;, including &quot;&lt;i&gt;hat&lt;/i&gt;&quot;, &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, and &quot;&lt;i&gt;bat&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="791"/>
        <source>matches &quot;&lt;i&gt;hat&lt;/i&gt;&quot; and &quot;&lt;i&gt;cat&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="792"/>
        <source>matches all strings matched by &lt;code&gt;.at&lt;/code&gt; except &quot;&lt;i&gt;bat&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="793"/>
        <source>matches &quot;&lt;i&gt;hat&lt;/i&gt;&quot; and &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, but only at the beginning of the string or line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="794"/>
        <source>matches &quot;&lt;i&gt;hat&lt;/i&gt;&quot; and &quot;&lt;i&gt;cat&lt;/i&gt;&quot;, but only at the end of the string or line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="798"/>
        <source>Source:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="795"/>
        <source>matches any single character surrounded by &quot;[&quot; and &quot;]&quot; since the brackets are escaped, for example: &quot;&lt;i&gt;[a]&lt;/i&gt;&quot; and &quot;&lt;i&gt;[b]&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="707"/>
        <source>POSIX Basic Regular Expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="877"/>
        <source>Use wildcard instead regex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="878"/>
        <source>Use wildcard expression instead regular expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="406"/>
        <source>lib7zip not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="411"/>
        <source>unknown</source>
        <translation type="unfinished">Nepoznato</translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="457"/>
        <source>support not compiled in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="455"/>
        <source>mediainfo not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="873"/>
        <source>Supported image extensions found: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="749"/>
        <source>Matches any single character (many applications exclude &lt;a href=&quot;http://en.wikipedia.org/wiki/Newline&quot; title=&quot;Newline&quot;&gt;newlines&lt;/a&gt;, and exactly which characters are considered newlines is flavor-, character-encoding-, and platform-specific, but it is safe to assume that the line feed character is included). Within POSIX bracket expressions, the dot character matches a literal dot. For example, &lt;code&gt;a.c&lt;/code&gt; matches &quot;&lt;i&gt;abc&lt;/i&gt;&quot;, etc., but &lt;code&gt;[a.c]&lt;/code&gt; matches only &quot;&lt;i&gt;a&lt;/i&gt;&quot;, &quot;&lt;i&gt;.&lt;/i&gt;&quot;, or &quot;&lt;i&gt;c&lt;/i&gt;&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="798"/>
        <source>&lt;a href=&quot;http://en.wikipedia.org/wiki/Regex&quot;&gt;regular expressions&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selreadable.cpp" line="800"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowContent</name>
    <message>
        <location filename="../showcontent.cpp" line="116"/>
        <source>Content of %1</source>
        <translation>Sadržaj %1</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="206"/>
        <source>Close</source>
        <translation>Zatvori</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="252"/>
        <source>Confirmation</source>
        <translation>Potvrda</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="255"/>
        <source>Yes</source>
        <translation>Da</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="255"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="71"/>
        <source>Remove the file content from the database. (Warning: Unrecoverable!)</source>
        <translation>Ukloni sadržaj fajla iz baze. (Upozorenje: Nepovrativo!)</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="61"/>
        <source>%1 kByte</source>
        <translation>%1 kB</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="75"/>
        <source>Save this content to a new file</source>
        <translation>Sačuvaj ovaj sadržaj u novi fajl</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save content...</source>
        <translation type="obsolete">Sačuvaj sadržaj...</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="229"/>
        <source>Select a filename below</source>
        <translation>Izaberite  ime fajla</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="233"/>
        <source>I can&apos;t rewrite the file: %1</source>
        <translation>Ne mogu da upišem u fajl: %1</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="234"/>
        <source>Error while saving...</source>
        <translation>Greška prilikom sačuvavanja...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">U redu</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="254"/>
        <source>Sure to delete this file&apos;s content from the database?</source>
        <translation>Sigurno želite da obrišete sadržaj ovog fajla iz baze?</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="92"/>
        <source>Category of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="202"/>
        <source>Set category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="204"/>
        <source>Set content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="229"/>
        <source>CdCat databases (*.hcf )</source>
        <translation type="unfinished">CdCat baze (*.hcf)</translation>
    </message>
    <message>
        <location filename="../showcontent.cpp" line="205"/>
        <source>select font for display</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>addDialog</name>
    <message>
        <location filename="../adddialog.cpp" line="77"/>
        <source>Directory Browser</source>
        <translation>Brovser direktorijuma</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="323"/>
        <source>New Disk %1</source>
        <translation>Novi disk %1</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="245"/>
        <source>Add Media to the Database</source>
        <translation>Dodaj medij Bazi</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="246"/>
        <source>Choose a directory to scan:</source>
        <translation>Izaberi direktorijum za skeniranje:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Media Name:</source>
        <translation type="obsolete">Naziv medija:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Serial number of Media:</source>
        <translation type="obsolete">Serijski broj medija:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Owner:</source>
        <translation type="obsolete">Vlasnik:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Type:</source>
        <translation type="obsolete">Tip:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Comment:</source>
        <translation type="obsolete">Komentar:</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="656"/>
        <source>Cancel</source>
        <translation type="obsolete">Otkaži</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK / Scan</source>
        <translation type="obsolete">U redu / Skeniraj</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="261"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="262"/>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="263"/>
        <source>HardDisc</source>
        <translation>HardDisk</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="264"/>
        <source>Floppy</source>
        <translation>Flopi</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="265"/>
        <source>NetworkPlace</source>
        <translation>Mrežni disk</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="266"/>
        <source>FlashDrive</source>
        <translation>Fleš disk</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="267"/>
        <source>OtherDevice</source>
        <translation>Ostali uređaji</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="398"/>
        <source>Error:</source>
        <translation>Greška:</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="367"/>
        <source>You must be fill the &quot;Name&quot; field!</source>
        <translation>Morate popuniti polje &quot;Ime&quot;!</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="657"/>
        <source>Ok</source>
        <translation type="obsolete">U redu</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="343"/>
        <source>The Media Name must be unique! Please change it!</source>
        <translation type="obsolete">Naziv medija mora biti jedinstven! Promenite ga!</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select readable items</source>
        <translation type="obsolete">Izaberite stavke za čitanje</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="393"/>
        <source>The Value of Serial Number must be unique! Please change it!</source>
        <translation>Serijski broj medija mora biti jedinstven! Promenite ga!</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="398"/>
        <source>You haven&apos;t selected a directory! Please select one!</source>
        <translation>Niste izabrali direktorijum! Izaberite jedan!</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="372"/>
        <source>The media name can&apos;t begin with the &quot;@&quot; character!</source>
        <translation>Naziv medija ne može da počinje sa  &quot;@&quot; !</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="247"/>
        <source>Media &amp;Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="248"/>
        <source>S&amp;erial number of Media:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="249"/>
        <source>&amp;Owner:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="250"/>
        <source>C&amp;ategory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="251"/>
        <source>&amp;Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="252"/>
        <source>Co&amp;mment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="253"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Otkaži</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="254"/>
        <source>&amp;Scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="255"/>
        <source>Select &amp;readable items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="257"/>
        <source>detect CDROM/DVD med&amp;ia name after mount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="653"/>
        <source>Add a &quot;Catalog Link&quot; to the database</source>
        <translation type="obsolete">Dodaj &quot;Catalog Link&quot; u bazu</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="654"/>
        <source>Location of the .hcf file:</source>
        <translation type="obsolete">Lokacija .hcf fajla:</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="674"/>
        <source>CdCat databases (*.hcf )</source>
        <translation type="obsolete">CdCat baze (*.hcf)</translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="383"/>
        <source>Enter media name...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="383"/>
        <source>The Media Name must be unique! Enter new media name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../adddialog.cpp" line="260"/>
        <source>unknown</source>
        <translation type="unfinished">Nepoznato</translation>
    </message>
</context>
<context>
    <name>borrowDialog</name>
    <message>
        <location filename="../borrow.cpp" line="116"/>
        <source>Ok</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="117"/>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="75"/>
        <source>I borrow the &quot;%1&quot; named media to:</source>
        <translation>Medij &quot;%1&quot; sam pozajmio/la:</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="87"/>
        <source>unknown</source>
        <translation>Nepoznato</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="115"/>
        <source>I borrow the &quot;&quot; named media to:</source>
        <translation>Medij &quot;%1&quot; sam pozajmio/la:</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="114"/>
        <source>Borrowing...</source>
        <translation>Pozajmljivanje...</translation>
    </message>
</context>
<context>
    <name>borrowingDialog</name>
    <message>
        <location filename="../borrow.cpp" line="211"/>
        <source>Media borrowing info:</source>
        <translation>Informacije o pozajmljivanju medija:</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="248"/>
        <source>Media</source>
        <translation>Medij</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="249"/>
        <source>Borrowed</source>
        <translation>Pozajmljien</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="250"/>
        <source>where is it now?</source>
        <translation>gde je sada?</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="220"/>
        <source>Ok</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="256"/>
        <source>0</source>
        <translation type="obsolete">0</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="456"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="463"/>
        <source>Yes</source>
        <translation>Da</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="325"/>
        <source>unknown</source>
        <translation>Nepoznato</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="329"/>
        <source>Error</source>
        <translation>Greška</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="329"/>
        <source>Set &quot;Yes&quot; or &quot;No&quot; !</source>
        <translation>Podesi &quot;Da&quot; ili &quot;Ne&quot; !</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="391"/>
        <source>Warning</source>
        <translation>Upozorenje</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="352"/>
        <source>Do you save the changes?</source>
        <translation>Da li želiš da sačuvaš promene?</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="391"/>
        <source>Are you sure want to clear all borrow?</source>
        <translation>Da li ste sigurni da želite da obrišete sva pozajmljivanja?</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="422"/>
        <source>I got it back!</source>
        <translation>Vrećen mi je!</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="427"/>
        <source>&lt;&lt; </source>
        <translation>&lt;&lt; </translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="210"/>
        <source>Borrowing info...</source>
        <translation>Informacije o pozajmljivanju...</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="212"/>
        <source>Show only borrowed items</source>
        <translation>Prikaži samo pozajmljne stvari</translation>
    </message>
    <message>
        <location filename="../borrow.cpp" line="213"/>
        <source>Clear all borrowing info</source>
        <translation>Obriši sve informacie o pozajmljivanju</translation>
    </message>
</context>
<context>
    <name>commentEdit</name>
    <message>
        <location filename="" line="0"/>
        <source>Edit Comment</source>
        <translation type="obsolete">Izmeni komentar</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1059"/>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1060"/>
        <source>OK</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1056"/>
        <source>Edit comment of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commwidget.cpp" line="1058"/>
        <source>Edit category of</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>exportCdcatDB</name>
    <message>
        <location filename="../exportcdcatdb.cpp" line="310"/>
        <source>Availiable media</source>
        <translation>Prisutni mediji</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="317"/>
        <source>Media to export</source>
        <translation>Medij za izvoz</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="318"/>
        <source>All media</source>
        <translation>Svi mediji</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="326"/>
        <source>separator:</source>
        <translation>Separator:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="327"/>
        <source>File to export:</source>
        <translation>Fajl za izvoz:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="328"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="329"/>
        <source>&amp;OK</source>
        <translation>&amp;U redu</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="330"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Otkaži</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="398"/>
        <source>Filename missing</source>
        <translation>Nedostaje ime fajla</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="398"/>
        <source>Please enter a filename!</source>
        <translation>Unesite ime fajla!</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="401"/>
        <source>Separator missing</source>
        <translation>Nedostaje separator</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="401"/>
        <source>Please enter a separator!</source>
        <translation>Unesite separator!</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error during write config...</source>
        <translation type="obsolete">Greška prilikom čuvanja konfiguracije...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">U redu</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="713"/>
        <source>All files (*.*)</source>
        <translation>Svi fajlovi(*.*)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>file chooser</source>
        <translation type="obsolete">izbor fajla</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="713"/>
        <source>Choose a file!</source>
        <translation>Izaberi fajl!</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="315"/>
        <source>Add media</source>
        <translation>Dodaj medij</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="313"/>
        <source>Remove media</source>
        <translation>Ukloni medij</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="321"/>
        <source>Type</source>
        <translation>Tip</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="322"/>
        <source>export as HTML</source>
        <translation>Izvezi kao HTML</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="323"/>
        <source>export as CSV</source>
        <translation>Izvezi kao CSV</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="482"/>
        <source>Media</source>
        <translation>Medij</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="467"/>
        <source>File</source>
        <translation>Fajl</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="509"/>
        <source>Size</source>
        <translation>Veličina</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="514"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="610"/>
        <source>I can&apos;t create or rewrite the file</source>
        <translation>Ne mogu da kreiram ili upišem fajl</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="441"/>
        <source>Export from cdcat:</source>
        <translation>Izvoz iz cdcata:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="60"/>
        <source>Export database...</source>
        <translation>Izvezi bazu...</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="163"/>
        <source>What to export</source>
        <translation>Šta da izvezem</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Only media name</source>
        <translation type="obsolete">Samo naziv medija</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="494"/>
        <source>Media name</source>
        <translation>Naziv medija</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="504"/>
        <source>Path</source>
        <translation>Putanja</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="519"/>
        <source>Comment</source>
        <translation>Komentar</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="341"/>
        <source>HTML headline</source>
        <translation>HTML naslov</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>table header</source>
        <translation type="obsolete">header tabele</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="549"/>
        <source>Overwrite?</source>
        <translation>Upisati preko?</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="550"/>
        <source>Do you want overwrite this file?</source>
        <translation>Da li želite da upišete preko ovog fajla?</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="555"/>
        <source>Yes</source>
        <translation>Da</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="556"/>
        <source>Discard</source>
        <translation>Zaboravi</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="369"/>
        <source>Export CdCat database: </source>
        <translation>Izvezi CdCat bazu: </translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="319"/>
        <source>Only media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="324"/>
        <source>export as XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="499"/>
        <source>Media number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="532"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="339"/>
        <source>export mp3 tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="340"/>
        <source>export borrow information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="342"/>
        <source>table header/comment line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="489"/>
        <source>Export from cdcat, catalog:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="490"/>
        <source>Generated at:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="465"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="524"/>
        <source>MP3 tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="528"/>
        <source>Borrow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="491"/>
        <source>field list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="561"/>
        <source>Exporting, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="610"/>
        <source>Error during write export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1043"/>
        <source>Artist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1044"/>
        <source>Title:</source>
        <translation type="unfinished">Naziv:</translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1045"/>
        <source>Album:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportcdcatdb.cpp" line="1046"/>
        <source>Year:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>findDialog</name>
    <message>
        <location filename="../find.cpp" line="927"/>
        <source>Name</source>
        <translation>Ime</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="928"/>
        <source>Type</source>
        <translation>Tip</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="930"/>
        <source>Media</source>
        <translation>Medij</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="931"/>
        <source>Path</source>
        <translation>Putanja</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="932"/>
        <source>Modification</source>
        <translation>Promena</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="525"/>
        <source>Search in the database...</source>
        <translation>Traži u bazi...</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="567"/>
        <source>Close / Go to selected</source>
        <translation>Zatvori / Idi na izabran</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="539"/>
        <source>Find:</source>
        <translation>Traži:</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="526"/>
        <source>Seek in:</source>
        <translation>Traži u:</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="527"/>
        <source>Owner:</source>
        <translation>Vlasnik:</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="933"/>
        <source>Comment</source>
        <translation>Komentar</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="874"/>
        <source>File name</source>
        <translation>Ime fajla</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="886"/>
        <source>mp3-tag Comment</source>
        <translation>Komentar it mpr taga</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="884"/>
        <source>mp3-tag Album</source>
        <translation>Album iz mp3 taga</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="882"/>
        <source>mp3-tag Title</source>
        <translation>Naslov iz mp3 taga</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="880"/>
        <source>mp3-tag Artist</source>
        <translation>Izvođač iz mp3 taga</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="859"/>
        <source>Case sensitive</source>
        <translation>Razlikujem velika i mala slova</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="851"/>
        <source>Use easy matching instead regex</source>
        <translation>Koristi približno poklapanje umesto regex</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="652"/>
        <source>All/Everybody</source>
        <translation>Sve/Svi</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="656"/>
        <source>All media</source>
        <translation>Svi mediji</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;U redu</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="562"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Otkaži</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="518"/>
        <source>Results</source>
        <translation type="obsolete">Rezultati</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="878"/>
        <source>Content</source>
        <translation>Sadržaj</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="872"/>
        <source>Media / Directory name</source>
        <translation>Naziv medija/direktorijuma</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="155"/>
        <source>Byte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="156"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="157"/>
        <source>MiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="158"/>
        <source>GiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="929"/>
        <source>Size</source>
        <translation type="unfinished">Veličina</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="905"/>
        <source>Date start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="909"/>
        <source>Date end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="913"/>
        <source>Min size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="917"/>
        <source>Max size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="893"/>
        <source>Unsharp search (slow)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="555"/>
        <source>Find in archives too</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="557"/>
        <source>&amp;Start search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="568"/>
        <source>Print result...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="569"/>
        <source>Export result to HTML...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="815"/>
        <source>Print cdcat result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="820"/>
        <source>Result file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="822"/>
        <source>Filename missing</source>
        <translation type="unfinished">Nedostaje ime fajla</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="822"/>
        <source>Please enter a filename!</source>
        <translation type="unfinished">Unesite ime fajla!</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="839"/>
        <source>Cdcat search result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="840"/>
        <source>catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="840"/>
        <source>created at:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="843"/>
        <source>used search options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="847"/>
        <source>search pattern:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="895"/>
        <source>on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="897"/>
        <source>off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="869"/>
        <source>Search in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="889"/>
        <source>archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="903"/>
        <source>other options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="993"/>
        <source>File cant open: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="993"/>
        <source>Cant open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="497"/>
        <source>Search for duplicates in the database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="518"/>
        <source>Duplicates for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="519"/>
        <source>Close</source>
        <translation type="unfinished">Zatvori</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="566"/>
        <source>Results: search not started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="537"/>
        <source>Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="159"/>
        <source>TiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="540"/>
        <source>Keep search result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="556"/>
        <source>Clear search results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="934"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="558"/>
        <source>Category for find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="704"/>
        <source>Selected dir: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>findDuplicatesDialog</name>
    <message>
        <location filename="../find.cpp" line="897"/>
        <source>Name</source>
        <translation type="obsolete">Ime</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="898"/>
        <source>Type</source>
        <translation type="obsolete">Tip</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="899"/>
        <source>Size</source>
        <translation type="obsolete">Veličina</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="900"/>
        <source>Media</source>
        <translation type="obsolete">Medij</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="901"/>
        <source>Path</source>
        <translation type="obsolete">Putanja</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="902"/>
        <source>Modification</source>
        <translation type="obsolete">Promena</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="903"/>
        <source>Comment</source>
        <translation type="obsolete">Komentar</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="909"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Otkaži</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="914"/>
        <source>Close</source>
        <translation type="obsolete">Zatvori</translation>
    </message>
</context>
<context>
    <name>helpDialog</name>
    <message>
        <location filename="../help.ui" line="13"/>
        <source>help</source>
        <translation>pomoć</translation>
    </message>
    <message>
        <location filename="../help.ui" line="65"/>
        <source>close</source>
        <translation>zatvori</translation>
    </message>
    <message>
        <location filename="../help.ui" line="29"/>
        <source>&lt;p align=&quot;center&quot;&gt;&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;Help&lt;/b&gt;&lt;/font&gt;&lt;/p&gt;&lt;br&gt;

&lt;b&gt;What&apos;s this?&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
The cdcat is graphical (QT based) multiplatform (Linux/Windows) catalog program which scans the directories/drives you want and memorize the filesystem including the tags of mp3&apos;s and other data and store it in a small file.
The database is stored in a gzipped XML format, so you can hack it, or use it if necessary :-)
And the program can store the content of some specified files up to a limit size if you want. (for example: *.nfo)
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Usage:&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
&lt;i&gt;Create a new catalog&lt;/i&gt;: Run the &lt;tt&gt;New&lt;/tt&gt; command in the catalog menu. You have to type the name of the new catalog. You can specify the default username of the media(which you scan later), and add a comment to the catalog.&lt;br&gt;Before the scanning select the necessary readable components in the config dialog, which can be mp3 tags content of some files or etc.  If you done this, you can scan one of your media with &lt;tt&gt;Add media&lt;/tt&gt;command in the Edit menu. In that dialog you have to specyfi the directory/or drive the media you want to add. It&apos;s recommended to specify the name and the number of the media which has to be unique. (The program always generate one identical name and number) You can label the media to a owner, if necessary.
&lt;br&gt;If you scanned your media, you will be able to browse in it with the browser window (like mc) , or search in it. You can save the catalog with &lt;tt&gt;save as&lt;/tt&gt; command in the Catalog menu.
&lt;br&gt;
&lt;br&gt;
&lt;i&gt;Open an existing catalog:&lt;/i&gt;Run the &lt;tt&gt;open&lt;/tt&gt; command in the Catalog menu, and choice the file of the catalog. (*.hcf)  After the opening you will be able browse the catalog or search in it.
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Author:&lt;/b&gt;
&lt;blockquote&gt;The program was written by  Peter Deak  (hungary)&lt;br&gt;
E-mail: hyperr@freemail.hu
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;License:&lt;/b&gt;
&lt;blockquote&gt;General Public License (GPL)&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Homepage:&lt;/b&gt;
&lt;blockquote&gt;You can read about the program and get new versions, sources etc, in the hompage of cdcat:&lt;br&gt;&lt;tt&gt;http://cdcat.sourceforge.net&lt;/tt&gt;&lt;/blockquote&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;&lt;font size=&quot;+1&quot;&gt;&lt;b&gt;Help&lt;/b&gt;&lt;/font&gt;&lt;/p&gt;&lt;br&gt;

&lt;b&gt;Šta je ovo?&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
Cdcat je grafički (baziran na QTu) više platformski (Linux/Windows) katalog program koji skenira direktorijume i drajvove čiji fajl sistem želite da zapamtite.
Baza podataka se čuva u gzipped XML format, pa ga možete hakovati, ili koristiti ako je neophodno :-)
Program takođe može sačuvati i ceo fajl do određene veličine. (na primer: *.nfo)
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Način korišćena:&lt;/b&gt;&lt;br&gt;
&lt;blockquote&gt;
&lt;i&gt;Napravi novi katalog&lt;/i&gt;:Pokreni komandu &lt;tt&gt;Novo&lt;/tt&gt; iz menija Katalog. Morate uneti naziv novog kataloga. Možete uneti i ime korisnika koji je vlasnik medija (kojeg ćete kasnije skenirati), i dodati komentar katalogu.&lt;br&gt;Pre skeniranja  izaberite neophodne komponente za čitanje u konfiguracionom dijalogu, koje mogu biti mp3 tagovi sadržaj nekih fajlova itd.  Ako ste ovo uradili, Možete skenirati medij komandom &lt;tt&gt;Dodaj medij&lt;/tt&gt; u meniju Uređivanje. U tom dijalogu morate odrediti direktorijum ili uređaj čiji fajl sistem želite da sačuvate. Preporučljivo je da unesete naziv i jedinstveni broj medija. (Program uvek generiše iso ime i broj) Medij možete nazvati i po vlasniku ako je potrebno.
&lt;br&gt;Ako ste skeniralji medij, moći ćete da ga razgledate preko brovser prozora (kao mc) , ili da ga pretražujete. Katalog možete sačuvate komandom &lt;tt&gt;sačuvaj kao&lt;/tt&gt; u meniju Katalog.
&lt;br&gt;
&lt;br&gt;
&lt;i&gt;Otvaranje postojećeg kataloga:&lt;/i&gt;Pokrenite komandu &lt;tt&gt;otvori&lt;/tt&gt; iz menija Katalog, i izaberite fajl kataloga. (*.hcf)  nakon otvaranja ćete moći da razgledate i pretražujete katalog.
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Autor:&lt;/b&gt;
&lt;blockquote&gt;Program je napisao  Peter Deak  (Mađarska)&lt;br&gt;
E-mail: hyperr@freemail.hu
&lt;br&gt;
&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Licenca:&lt;/b&gt;
&lt;blockquote&gt;General Public License (GPL)&lt;/blockquote&gt;
&lt;br&gt;
&lt;b&gt;Homepage:&lt;/b&gt;
&lt;blockquote&gt;Možete čitati o programu novim verzijama, preuzeti izvorni kod itd., na homepageu cdcata:&lt;br&gt;&lt;tt&gt;http://cdcat.sourceforge.net&lt;/tt&gt;&lt;/blockquote&gt;</translation>
    </message>
    <message>
        <location filename="../help.ui" line="22"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:40px; margin-right:40px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>importGtktalogCsv</name>
    <message>
        <location filename="../import.cpp" line="1344"/>
        <source>Import was successful.</source>
        <translation>Uvoz je bio uspešan.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1347"/>
        <source>1 media</source>
        <translation>1 medij</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1349"/>
        <source>media</source>
        <translation>medij</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1352"/>
        <source>1 directory:,
</source>
        <translation>1 direktorijum:,
</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1354"/>
        <source>directories</source>
        <translation>direktorijumi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1357"/>
        <source>1 File</source>
        <translation>1 Fajl</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1359"/>
        <source>files</source>
        <translation>fajlovi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1361"/>
        <source>are imported.</source>
        <translation>su uvezeni.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1363"/>
        <source>Import successful</source>
        <translation>Uvoz uspešan</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1367"/>
        <source>file read error</source>
        <translation>greška pri čitanju fajla</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1367"/>
        <source>Could not read file</source>
        <translation>Ne mogu da čitam fajl</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1370"/>
        <source>Fatal error</source>
        <translation>Fatalna greška</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1370"/>
        <source>Fatal error occured.</source>
        <translation>Desila se fatalna greška.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1373"/>
        <source>file open error</source>
        <translation>greška pri otvaranju fajla</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1373"/>
        <source>Could not open file</source>
        <translation>Ne mogu da otvorim fajl</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1411"/>
        <source>importuser</source>
        <translation>uvezi korisnika</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="187"/>
        <source>Importing CSV...</source>
        <translation>Uvozim CSV...</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="187"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1274"/>
        <source>tag: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>importGtktalogXml</name>
    <message>
        <location filename="../import.cpp" line="1814"/>
        <source>Importing XML...</source>
        <translation>Uvoz XML...</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1851"/>
        <source>XML import was successful.</source>
        <translation>XML uvoz je bio uspešan.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1854"/>
        <source>1 media</source>
        <translation>1 medij</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1856"/>
        <source>media</source>
        <translation>Medij</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1859"/>
        <source>1 directory:,
</source>
        <translation>1 direktorijum:,
</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1861"/>
        <source>directories</source>
        <translation>direktorijumi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1864"/>
        <source>1 File</source>
        <translation>1 Fajl</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1866"/>
        <source>files</source>
        <translation>fajlovi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1868"/>
        <source>are imported.</source>
        <translation>su uvezeni.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1870"/>
        <source>Import successful</source>
        <translation>Uvoz uspešan</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1875"/>
        <source>parse error</source>
        <translation>greška pri obradi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1875"/>
        <source>error during parsing</source>
        <translation>greška pri obradi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1989"/>
        <source>importuser</source>
        <translation>uvezi korisnika</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="1814"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>importWhereIsItXml</name>
    <message>
        <location filename="../import.cpp" line="2658"/>
        <source>Importing XML...</source>
        <translation>Uvoz XML...</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2694"/>
        <source>Importing XML from WhereIsIt was successful.</source>
        <translation>XML uvoz je bio uspešan.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2697"/>
        <source>1 media</source>
        <translation>1 medij</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2699"/>
        <source>media</source>
        <translation>Medij</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2702"/>
        <source>1 directory:,
</source>
        <translation>1 direktorijum:,
</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2704"/>
        <source>directories</source>
        <translation>direktorijumi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2707"/>
        <source>1 File</source>
        <translation>1 Fajl</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2709"/>
        <source>files</source>
        <translation>fajlovi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2711"/>
        <source>are imported.</source>
        <translation>su uvezeni.</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2713"/>
        <source>Import successful</source>
        <translation>Uvoz uspešan</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2718"/>
        <source>parse error</source>
        <translation>greška pri obradi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2718"/>
        <source>error during parsing</source>
        <translation>greška pri obradi</translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2658"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import.cpp" line="2390"/>
        <source>tag:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>newdbdialog</name>
    <message>
        <location filename="../newdbdialog.cpp" line="133"/>
        <source>DataBase&apos;s Properties</source>
        <translation>Vrednosti Baze podataka</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="134"/>
        <source>DataBase Name:</source>
        <translation>Naziv baze:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="135"/>
        <source>DataBase Owner:</source>
        <translation>Vlasnik Baze:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="136"/>
        <source>Comment:</source>
        <translation>Komentar:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="138"/>
        <source>Catalog</source>
        <translation>Katalog</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="139"/>
        <source>hcat-user</source>
        <translation>hcat-korisnik</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="140"/>
        <source>OK</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="141"/>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="142"/>
        <source> </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="137"/>
        <source>Category:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="144"/>
        <source>encrypt catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="145"/>
        <source>password for catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="159"/>
        <source>Password length is too short, must be minimum 4 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="159"/>
        <source>Password too short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="163"/>
        <source>Password length is too big, must be maximal %1 chars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="163"/>
        <source>Password too big</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>renamedialog</name>
    <message>
        <location filename="../newdbdialog.cpp" line="227"/>
        <source>Rename node...</source>
        <translation>Promeni naziv čvora...</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="228"/>
        <source>Give the new name:</source>
        <translation>Novo ime:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="229"/>
        <source>OK</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="230"/>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="254"/>
        <source>Error:</source>
        <translation>Greška:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">U redu</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="249"/>
        <source>The new (media) name must be unique!</source>
        <translation>Nov naziv medija mora biti jedinstven!</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="254"/>
        <source>The new media name can&apos;t starts with &quot;@&quot; !</source>
        <translation>Naziv medija ne može da počinje sa  &quot;@&quot; !</translation>
    </message>
</context>
<context>
    <name>renumberdialog</name>
    <message>
        <location filename="../newdbdialog.cpp" line="372"/>
        <source>Error:</source>
        <translation>Greška:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="301"/>
        <source>The element is NOT a Media!</source>
        <translation>Element NIJE medij!</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">U redu</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="338"/>
        <source>Re-Number media...</source>
        <translation>Promeni broj medija...</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="339"/>
        <source>Give the new serial number:</source>
        <translation>Dajte mu novi serijski broj:</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="340"/>
        <source>OK</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="341"/>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="372"/>
        <source>The new media-number must be unique!</source>
        <translation>Novi broj medija mora biti jedinstven!</translation>
    </message>
    <message>
        <location filename="../newdbdialog.cpp" line="362"/>
        <source>Please enter number value!</source>
        <translation>Unestite broj!</translation>
    </message>
</context>
<context>
    <name>seekEngine</name>
    <message>
        <location filename="../find.cpp" line="1077"/>
        <source>Error in the pattern:</source>
        <translation>Greška u pretraživanju:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ok</source>
        <translation type="obsolete">U redu</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1163"/>
        <source>There is no matching.</source>
        <translation>Nema poklapanja.</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1652"/>
        <source>dir</source>
        <translation>direktorijum</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1658"/>
        <source>file</source>
        <translation>fajl</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1669"/>
        <source>error</source>
        <translation>greška</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1646"/>
        <source>media</source>
        <translation>medij</translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1041"/>
        <source>Searching, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1158"/>
        <source>Search cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1158"/>
        <source>You have cancelled searching.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1505"/>
        <source>File in archive: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1154"/>
        <source>Results:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1154"/>
        <source>searching is in progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../find.cpp" line="1160"/>
        <source>Last search results:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
